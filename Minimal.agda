{-# OPTIONS --prop #-}

module Minimal where

open import lib

-- intuitionistic logic

-- t : Pf Γ A        Γ ⊢ A        Pfs Γ (A,B,C) = Pf Γ A × Pf Γ B × Pf Γ C   Pfs Γ Γ
-- q [ p ] [ p ] : Pf (∙ ▷ A ▷ B ▷ C) A

-- simply typed CwF, thin

record Algebra (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con  : Set i
    Pfs  : Con → Con → Prop j
    For  : Set i
    Pf   : Con → For → Prop j
    id   : ∀{Γ} → Pfs Γ Γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    ∙    : Con
    ε    : ∀{Γ} → Pfs Γ ∙
    _▷_  : Con → For → Con
    _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    q    : ∀{Γ A} → Pf  (Γ ▷ A) A

    ⊤    : For
    tt   : ∀{Γ} → Pf Γ ⊤

    _⇒_  : For → For → For
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

    --   Γ,A ⊢ B
    -- --------------------- lam
    --    Γ ⊢ A ⇒ B

    -- presyntax, typing relations               CwF+extra structure        LCCC
    -- presyntax, typing relations    contex.    simply typed CwF           CCC
    -- proof theoretic notation                  this                       Heyting algebras

    -- ∧,∨,⊥
    -- Heyting: Con=For, Pfs=Pf, (Pf Γ A ↔ Pf Γ B) → A = B
    -- ▷ = ∧, Pf = ⇒
    -- 
    -- Pf ∙       B = B
    -- Pf (Γ ▷ A) B = Pf Γ (A ⇒ B) = Γ ⇒ A ⇒ B

    -- lem : ∀{Γ A} → Pf Γ (A ∨ ¬ A)

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixr 6 _⇒_

-- syntax

module I where

  data For  : Set where
       ⊤    : For
       _⇒_  : For → For → For

  data Con  : Set where
       ∙    : Con
       _▷_  : Con → For → Con

  data Pfs  : Con → Con → Prop
  data Pf   : Con → For → Prop

  data Pfs    where
       id   : ∀{Γ} → Pfs Γ Γ
       _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
       p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
       ε    : ∀{Γ} → Pfs Γ ∙
       _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)

  data Pf     where
       _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
       q    : ∀{Γ A} → Pf  (Γ ▷ A) A
       tt   : ∀{Γ} → Pf Γ ⊤
       lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
       app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

  I : Algebra _ _
  I = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = id ;
    _∘_ = _∘_ ; _[_] = _[_] ; ∙ = ∙ ; ε = ε ; _▷_ = _▷_ ; _,_ = _,_ ; p
    = p ; q = q ; ⊤ = ⊤ ; tt = tt ; _⇒_ = _⇒_ ; lam = lam ; app = app }

  module I = Algebra I

module rec {i j}(M : Algebra i j) where

  module M = Algebra M

  ⟦_⟧F : I.For → M.For
  ⟦ I.⊤ ⟧F = M.⊤
  ⟦ A I.⇒ B ⟧F = ⟦ A ⟧F M.⇒ ⟦ B ⟧F

  ⟦_⟧C : I.Con → M.Con
  ⟦ I.∙ ⟧C = M.∙
  ⟦ Γ I.▷ A ⟧C = ⟦ Γ ⟧C M.▷ ⟦ A ⟧F

  ⟦_⟧s : ∀{Γ Δ} → I.Pfs Γ Δ  → M.Pfs ⟦ Γ ⟧C ⟦ Δ ⟧C
  ⟦_⟧P : ∀{Γ A} → I.Pf  Γ A  → M.Pf  ⟦ Γ ⟧C ⟦ A ⟧F

  ⟦ I.id ⟧s = M.id
  ⟦ σ I.∘ δ ⟧s = ⟦ σ ⟧s M.∘ ⟦ δ ⟧s
  ⟦ I.p ⟧s = M.p
  ⟦ I.ε ⟧s = M.ε
  ⟦ σ I., u ⟧s = ⟦ σ ⟧s M., ⟦ u ⟧P
  
  ⟦ u I.[ σ ] ⟧P = ⟦ u ⟧P M.[ ⟦ σ ⟧s ]
  ⟦ I.q ⟧P = M.q
  ⟦ I.tt ⟧P = M.tt
  ⟦ I.lam u ⟧P = M.lam ⟦ u ⟧P
  ⟦ I.app u ⟧P = M.app ⟦ u ⟧P

-- standard model

st : Algebra _ _
st = record
  { Con = Prop
  ; Pfs = λ Γ Δ → Γ → Δ
  ; For = Prop
  ; Pf = λ Γ A → Γ → A
  ; id = λ γ → γ
  ; _∘_ = λ σ δ γ → σ (δ γ)
  ; _[_] = λ u σ γ → u (σ γ)
  ; ∙ = 𝟙p
  ; ε = λ γ → *
  ; _▷_ = λ Γ A → Γ ×p A
  ; _,_ = λ σ u γ → (σ γ ,Σ u γ)
  ; p = proj₁
  ; q = proj₂
  ; ⊤ = 𝟙p
  ; tt = λ γ → *
  ; _⇒_ = λ A B → A → B
  ; lam = λ u γ α → u (γ ,Σ α)
  ; app = λ u γ → u (proj₁ γ) (proj₂ γ)
  }

module Kripke
  (W : Set)
  (_≤_ : W → W → Prop)
  (id≤ : {w : W} → w ≤ w)
  (_∘≤_ : {w w' w'' : W} → w' ≤ w → w'' ≤ w' → w'' ≤ w)
  where
 
  record PSh : Set₁ where
    field
      ∣_∣    : W → Prop
      _∶_⟨_⟩ : ∀{w w'} → ∣_∣ w → w' ≤ w → ∣_∣ w'
  open PSh public
  
  K : Algebra _ _
  K = record
    { Con = PSh
    ; Pfs = λ Γ Δ → {w : W} → ∣ Γ ∣ w → ∣ Δ ∣ w
    ; For = PSh
    ; Pf  = λ Γ A → {w : W} → ∣ Γ ∣ w → ∣ A ∣ w
    ; id  = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = record { ∣_∣ = λ _ → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; ε = λ _ → *
    ; _▷_ = λ Γ A → record { ∣_∣ = λ w → ∣ Γ ∣ w ×p ∣ A ∣ w ; _∶_⟨_⟩ = λ { (γ ,Σ α) β → Γ ∶ γ ⟨ β ⟩ ,Σ A ∶ α ⟨ β ⟩ } }
    ; _,_ = λ σ u γ → σ γ ,Σ u γ
    ; p = proj₁
    ; q = proj₂
    ; ⊤ = record { ∣_∣ = λ w → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; tt = λ _ → *
    ; _⇒_ = λ A B → record { ∣_∣ = λ w → ∀{w'} → w' ≤ w → ∣ A ∣ w' → ∣ B ∣ w' ; _∶_⟨_⟩ = λ {w w'} f β {w''} β' α → f (β ∘≤ β') α }
    ; lam = λ {Γ}{A} u γ β α → u (Γ ∶ γ ⟨ β ⟩ ,Σ α)
    ; app = λ { {Γ}{A}{B} u (γ ,Σ α) → u γ id≤ α }
    }

  module K = Algebra K

module Completeness where

  open Kripke I.Con I.Pfs I.id I._∘_

  open rec K

  QF : (A : I.For){Γ : I.Con} → ∣ ⟦ A ⟧F ∣ Γ → I.Pf Γ A
  UF : (A : I.For){Γ : I.Con} → I.Pf Γ A → ∣ ⟦ A ⟧F ∣ Γ

  QF I.⊤       _ = I.tt
  QF (A I.⇒ B) f = I.lam (QF B (f I.p (UF A I.q)))

  UF I.⊤       _ = *
  UF (A I.⇒ B) u = λ σ α → UF B (I.app u I.[ σ I., QF A α ])

  UC : (Δ : I.Con){Γ : I.Con} → I.Pfs Γ Δ → ∣ ⟦ Δ ⟧C ∣ Γ
  UC I.∙       _ = *
  UC (Δ I.▷ A) σ = UC Δ (I.p I.∘ σ) ,Σ UF A (I.q I.[ σ ])

  compl : ∀{Γ A} → K.Pf ⟦ Γ ⟧C ⟦ A ⟧F → I.Pf Γ A
  compl {Γ}{A} f = QF A (f (UC Γ I.id))
