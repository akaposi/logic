
open import Agda.Primitive
open import Agda.Builtin.Equality
open import Agda.Builtin.Bool
open import Agda.Builtin.Unit renaming (⊤ to Unit)
open import Agda.Builtin.Sigma

data Empty : Set where

exfalso : {i : Level} {A : Set i} → Empty → A
exfalso ()

data _⊎_ {i j}(A : Set i)(B : Set j) : Set (i ⊔ j) where
  inl : A → A ⊎ B
  inr : B → A ⊎ B

case : {i j k : Level}{A : Set i}{B : Set j}{C : Set k} → A ⊎ B → (A → C) → (B → C) → C
case (inl a) f _ = f a
case (inr b) _ g = g b

_×_ : ∀{i j} → Set i → Set j → Set (i ⊔ j)
A × B = Σ A λ _ → B

postulate
  PV : Set

_&&_ : Bool → Bool → Bool
true  && b = b
false && _ = false

_||_ : Bool → Bool → Bool
true  || _ = true
false || b = b

nulll|| : ∀ a → true || a ≡ true
nulll|| false = refl
nulll|| true = refl

nullr|| : ∀ a → a || true ≡ true
nullr|| false = refl
nullr|| true  = refl

not : Bool → Bool
not true = false
not false = true

module Syntax where

  infixl 3 _▹_
  infixl 6 _∧_
  infixl 5 _∨_
  infixr 4 _⊃_
  infixl 1 _⊢_∣_

  data For : Set where -- formulak (itelet, propozicio, allitas)
    pv          : PV → For
    _∧_ _∨_ _⊃_ : For → For → For
    ⊤ ⊥         : For

  ¬_ : For → For
  ¬ A = A ⊃ ⊥

  _⇔_ : For → For → For
  A ⇔ B = (A ⊃ B) ∧ (B ⊃ A)

  postulate R U K : PV
  A1 = pv R ⊃ (pv U ∧ pv K)
  A2 = (¬ pv K) ⊃ pv U
  A3 = pv K ⊃ pv R
  -------------------------
  B  = pv U

  data Con : Set where -- kornyezet, kontextus (formulak listaja)
    ◇   : Con
    _▹_ : Con → For → Con
  -- az a kornyezet, amiben feltelezzuk A1,A2,A3-at:
  Γ : Con
  Γ = ((◇ ▹ A1) ▹ A2) ▹ A3
  -- (Haskell: Con := [For] = List For)

  {-
  data Tree : Set where
    node2 : Tree → Tree → Tree
    node1 : Tree → Tree
    leaf  : Tree

  /\        /\   ≠   /\
    |        /\     /\
   /\
  /\ |
     |
     /\

  data List(A :Set) : Set where
    nil : List A
    cons : A → List A → List A

  data Vec(A:Set) : ℕ → Set where
    nil : Vec A 0
    cons : A → {n : ℕ} → Vec A n → Vec A (suc n)

  HASKELL head : List A → Maybe A
  HASKELL head (cons a ak) = a
  HASKELL head nil = ?
  AGDA    head : {n : ℕ} → Vec A (1 + n) → A
  HASKELL tail : List A → List A
  HASKELL tail (cons a ak) = as
  HASKELL tail nil = ?
  AGDA    tail : {n : ℕ} → Vec A (1 + n) → Vec A n
  AGDA    tail (cons a as) = as
  -}

  data Pf : Con → For → Set where -- bizonyitasok (proof)
    -- Pf ⊆ Con × For binaris relacio
    -- logikai osszekotonkent vannak bevezeto es eliminacios szabalyok
    -- _∧_
    intro∧  : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    elim∧₁  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    elim∧₂  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
    -- ⊤
    intro⊤  : ∀{Γ} → Pf Γ ⊤
    -- ⊥
    elim⊥   : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A                 -- ex falso quodlibet
    -- ⊃
    elim⊃   : ∀{Γ A B} → Pf Γ (A ⊃ B) → Pf Γ A → Pf Γ B  -- modus ponens
    intro⊃  : ∀{Γ A B} → Pf (Γ ▹ A) B → Pf Γ (A ⊃ B)
    -- _∨_
    intro∨₁ : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    intro∨₂ : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    elim∨   : ∀{Γ A B C} → Pf Γ (A ∨ B) → Pf (Γ ▹ A) C → Pf (Γ ▹ B) C → Pf Γ C
    -- strukturalis szabalyok
    zero    : ∀{Γ A} → Pf (Γ ▹ A) A
    suc     : ∀{Γ A B} → Pf Γ A → Pf (Γ ▹ B) A    -- weakening, gyengites

  -- feltelek hasznalata
  v0 : ∀{Γ D} → Pf (Γ ▹ D) D
  v0 = zero
  v1 : ∀{Γ C D} → Pf (Γ ▹ C ▹ D) C
  v1 = suc zero
  v2 : ∀{Γ B C D} → Pf (Γ ▹ B ▹ C ▹ D) B
  v2 = suc (suc zero)
  v3 : ∀{Γ A B C D} → Pf (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = suc (suc (suc zero))

  p : (∀{Γ} A → Pf Γ (A ∨ ¬ A)) → Pf (◇ ▹ A1 ▹ A2 ▹ A3) B  -- <-> Pf ◇ (A1 ∧ A2 ∧ A3 ⊃ B)
  p lem = elim∨
    (lem (pv K))
    (elim∧₁
      (elim⊃
        (suc (suc (suc zero)))
        (elim⊃ (suc zero) zero)))
    (elim⊃ (suc (suc zero)) zero)

  module _ (Γ : Con)(A B : For) where
    pppp : Pf Γ (A ∧ B) → Pf Γ (A ∧ B)
    pppp (intro∧ a b) = {!!}
    pppp (elim∧₁ {.Γ}{.(A ∧ B)}{C} abc) = {!!}
    pppp (elim∧₂ {_}{C} cab) = {!!}
    pppp (elim⊥ f) = {!!}
    pppp (elim⊃ c⇒ab c) = {!!}
    pppp (elim∨ de d⇒ab e⇒ab) = {!!}
    pppp zero = {!!}
    pppp (suc ab) = {!!}


  {-
  HF1:
  A1: Ha Aladár busszal utazik, és a busz késik, akkor nem ér oda a találkozóra.
  A2: Ha nem ér oda a találkozóra és nem tud telefonálni, akkor nem kapja meg az
  állást.
  A3: Ha rossz a kocsija, akkor busszal kell mennie.
  A4: Aladárnak rossz napja van, mert a kocsija nem indul, rossz a telefonja és a busz
  késik.
  B: Tehát Aladár nem kapja meg az állást.

  HF2:
  A1: Ha elmegyünk Pécsre, akkor Hévı́zre és Keszthelyre is.
  A2: Ha nem megyünk Keszthelyre, akkor elmegyünk Hévı́zre.
  A3: Ha elmegyünk Keszthelyre, akkor Pécsre is.
  B: Tehát elmegyünk Hévı́zre.

  HF3: logikai torvenyek (nem kell lem)
  (A ∧ B) ∧ C ⇔ A ∧ (B ∧ C)
  ⊤ ∧ A ⇔ A
  A ∧ B ⇔ B ∧ A
  (A ∨ B) ∨ C ⇔ A ∨ (B ∨ C)
  ⊥ ∨ A ⇔ A
  A ∨ B ⇔ B ∨ A
  (A ∨ B) ∧ C ⇔ A ∧ C ∨ B ∧ C
  (A ∧ B) ∨ C ⇔ (A ∨ C) ∧ (B ∨ C)
  ⊤ ⊃ A ⇔ A
  A ⊃ (B ⊃ C) ⇔ (A ∧ B) ⊃ C
  (A ∨ B) ⊃ C ⇔ (A ⊃ C) ∧ (B ⊃ C)
  De Morgan szabalyok (3-bol egyhez kell lem)
  ¬ (A ∧ B) ⇔ ¬ A ∨ ¬ B
  ¬ (A ∨ B) ⇔ ¬ A ∧ ¬ B

  12 perc+
  -}


  _⊢_∣_ : ∀ Γ A → Pf Γ A → Pf Γ A
  Γ ⊢ A ∣ p = p

  module _ (A B C : For) where

    p1 : Pf (◇ ▹ A ∧ ¬ A) ⊥
    p1 = ◇ ▹ A ∧ ¬ A ⊢ ⊥ ∣ elim⊃
           (◇ ▹ A ∧ ¬ A ⊢ A ⊃ ⊥ ∣ elim∧₂
             (◇ ▹ A ∧ ¬ A ⊢ A ∧ ¬ A ∣ zero))
           (◇ ▹ A ∧ ¬ A ⊢ A ∣ elim∧₁
             (◇ ▹ A ∧ ¬ A ⊢ A ∧ ¬ A ∣ zero))

    -- ¬ (A ∧ B) ⇔ ¬ A ∨ ¬ B
    DM1 : (∀{Γ} A → Pf Γ (A ∨ ¬ A)) →
      Pf ◇ (¬ (A ∧ B) ⊃ ¬ A ∨ ¬ B)
    DM1 lem = intro⊃
            (elim∨
              (lem A)
              (intro∨₂
                (intro⊃
                  (elim⊃
                    v2
                    (intro∧
                    v1
                    v0))))
              (intro∨₁
                zero))

    DM2 : Pf ◇ (¬ A ∨ ¬ B ⊃ A ∧ B ⊃ ⊥)
    
    DM2 = intro⊃ (intro⊃ (elim∨ v1 (elim⊃ zero (elim∧₁ v1))
                                   (elim⊃ zero (elim∧₂ v1)))) 

    -- ¬ (A ∨ B) ⇔ ¬ A ∧ ¬ B
    DM3 : Pf ◇ (¬ (A ∨ B) ⊃ ¬ A ∧ ¬ B)
    DM3 = intro⊃ (intro∧ (intro⊃ (elim⊃ v1 (intro∨₁ v0)))
                         (intro⊃ (elim⊃ v1 (intro∨₂ v0))))

    -- kovetkezo ora 15 perccel rovidebb
    DM4 : Pf ◇ (¬ A ∧ ¬ B ⊃ ¬ (A ∨ B))
    DM4 = {!!}

    -- Anna
    dist1 : Pf ◇ ((A ∨ B) ∧ C ⊃ A ∧ C ∨ B ∧ C)
    dist1 = {!!}
    dist2 : Pf ◇ (A ∧ C ∨ B ∧ C ⊃ (A ∨ B) ∧ C)
    dist2 = {!!}
    dist3 : Pf ◇ ((A ∧ B) ∨ C ⊃ (A ∨ C) ∧ (B ∨ C))
    dist3 = {!!}
    dist4 : Pf ◇ ((A ∨ C) ∧ (B ∨ C) ⊃ (A ∧ B) ∨ C)
    dist4 = {!!}

  module _ (⟦PV⟧ : PV → Bool) where
    ⟦_⟧F : For → Bool
    ⟦ pv X ⟧F = ⟦PV⟧ X
    ⟦ A ∧ B ⟧F = ⟦ A ⟧F && ⟦ B ⟧F
    ⟦ A ∨ B ⟧F = ⟦ A ⟧F || ⟦ B ⟧F
    ⟦ A ⊃ B ⟧F = not ⟦ A ⟧F || ⟦ B ⟧F
    ⟦ ⊤ ⟧F = true
    ⟦ ⊥ ⟧F = false

    ⟦_⟧C : Con → Bool
    ⟦ ◇ ⟧C = true
    ⟦ Γ ▹ A ⟧C = ⟦ Γ ⟧C && ⟦ A ⟧F

    b = {!⟦ pv R ⟧F || true!}
    a = {!⟦ ⊤ ∧ (pv R ⊃ ⊤ ∨ (⊥ ∧ ⊥)) ∨ (⊥ ∨ ⊤) ⟧F!}
    -- ◇ ▹ pv R ⊃ pv U ∧ pv K ▹ (pv K ⊃ ⊥) ⊃ pv U ▹ pv K ⊃ pv R

    intro∧-helper : ∀ g a b → not g || b ≡ true → not g || a ≡ true → not g || (a && b) ≡ true
    intro∧-helper false false false e1 e2 = refl
    intro∧-helper false false true e1 e2 = refl
    intro∧-helper false true false e1 e2 = refl
    intro∧-helper false true true e1 e2 = refl
    intro∧-helper true true true e1 e2 = refl

    -- ez ugyanaz, mint a Boole modell (lasd lent), csak csunyan megadva
    ⟦_⟧p : ∀{Γ A} → Pf Γ A → not ⟦ Γ ⟧C || ⟦ A ⟧F ≡ true
    ⟦ intro∧ {Γ}{A}{B} a b ⟧p = intro∧-helper ⟦ Γ ⟧C ⟦ A ⟧F ⟦ B ⟧F ⟦ b ⟧p ⟦ a ⟧p
    {-
    not g || b = true
    not g || a = true
    ---------------------
    not g || (a && b) = true
    -}
    ⟦ elim∧₁ a ⟧p = {!!}
    ⟦ elim∧₂ b ⟧p = {!!}
    ⟦ intro⊤ ⟧p = {!!}
    ⟦ elim⊥ b ⟧p = {!!}
    ⟦ elim⊃ t a ⟧p = {!!}
    ⟦ intro⊃ b ⟧p = {!!}
    ⟦ intro∨₁ a ⟧p = {!!}
    ⟦ intro∨₂ b ⟧p = {!!}
    ⟦ elim∨ c₁ c₂ ab ⟧p = {!!}
    ⟦ zero ⟧p = {!!}
    ⟦ suc a ⟧p = {!!}

module Inconstistent where

  record Model : Set₁ where
   field
    For : Set
    pv          : PV → For
    _∧_ _∨_ _⊃_ : For → For → For
    ⊤ ⊥         : For

    Con : Set
    ◇   : Con
    _▹_ : Con → For → Con

    Pf      : Con → For → Set
    intro∧  : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    elim∧₁  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    elim∧₂  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
    intro⊤  : ∀{Γ} → Pf Γ ⊤
    elim⊥   : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A                 -- ex falso quodlibet
    elim⊃   : ∀{Γ A B} → Pf Γ (A ⊃ B) → Pf Γ A → Pf Γ B  -- modus ponens
    intro⊃  : ∀{Γ A B} → Pf (Γ ▹ A) B → Pf Γ (A ⊃ B)
    intro∨₁ : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    intro∨₂ : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    elim∨   : ∀{Γ A B C} → Pf Γ (A ∨ B) → Pf (Γ ▹ A) C → Pf (Γ ▹ A) C → Pf Γ C -- inkonzisztens
    zero    : ∀{Γ A} → Pf (Γ ▹ A) A
    suc     : ∀{Γ A B} → Pf Γ A → Pf (Γ ▹ B) A    -- weakening, gyengites
    lem     : ∀{Γ} A → Pf Γ (A ∨ (A ⊃ ⊥))

  -- Set₀ : Set₁
  -- Set₁ : Set₂ ...

   inconsistent : ∀{Γ A} → Pf Γ A
   inconsistent {Γ}{A} = elim⊥ (elim∨ {Γ}{⊥}{⊤}{⊥} (intro∨₂ intro⊤) zero zero)

  Boole : Model
  Model.For Boole = Bool
  Model.pv Boole = {!!}
  Model._∧_ Boole = _&&_
  Model._∨_ Boole = _||_
  Model._⊃_ Boole = λ a b → not a || b
  Model.⊤ Boole = true
  Model.⊥ Boole = false
  Model.Con Boole = Bool
  Model.◇ Boole = true
  Model._▹_ Boole = _&&_
  Model.Pf Boole = λ a b → not a || b ≡ true
  Model.intro∧ Boole {false} {false} {false} a b = refl
  Model.intro∧ Boole {false} {false} {true} a b = refl 
  Model.intro∧ Boole {false} {true} {false} a b = refl
  Model.intro∧ Boole {false} {true} {true} a b = refl
  Model.intro∧ Boole {true} {true} {true} a b = refl
  Model.elim∧₁ Boole {false} {false} {false} ab = refl
  Model.elim∧₁ Boole {false} {false} {true} ab = refl
  Model.elim∧₁ Boole {false} {true} {false} ab = refl
  Model.elim∧₁ Boole {false} {true} {true} ab = refl
  Model.elim∧₁ Boole {true} {true} {true} ab = refl
  Model.elim∧₂ Boole {false} {false} {false} ab = refl
  Model.elim∧₂ Boole {false} {false} {true} ab = refl
  Model.elim∧₂ Boole {false} {true} {false} ab = refl
  Model.elim∧₂ Boole {false} {true} {true} ab = refl
  Model.elim∧₂ Boole {true} {true} {true} ab = refl
  Model.intro⊤ Boole {false} = refl
  Model.intro⊤ Boole {true} = refl
  Model.elim⊥ Boole {false} {false} b = refl
  Model.elim⊥ Boole {false} {true} b = refl
  Model.elim⊃ Boole {false} {false} {false} t a = refl
  Model.elim⊃ Boole {false} {false} {true} t a = refl
  Model.elim⊃ Boole {false} {true} {false} t a = refl
  Model.elim⊃ Boole {false} {true} {true} t a = refl
  Model.elim⊃ Boole {true} {true} {true} t a = refl
  Model.intro⊃ Boole {false} {false} {false} b = refl
  Model.intro⊃ Boole {false} {false} {true} b = refl
  Model.intro⊃ Boole {false} {true} {false} b = refl
  Model.intro⊃ Boole {false} {true} {true} b = refl
  Model.intro⊃ Boole {true} {false} {false} b = refl
  Model.intro⊃ Boole {true} {false} {true} b = refl
  Model.intro⊃ Boole {true} {true} {true} b = refl
  Model.intro∨₁ Boole {false} {false} {false} a = refl
  Model.intro∨₁ Boole {false} {false} {true} a = refl
  Model.intro∨₁ Boole {false} {true} {false} a = refl
  Model.intro∨₁ Boole {false} {true} {true} a = refl
  Model.intro∨₁ Boole {true} {true} {false} a = refl
  Model.intro∨₁ Boole {true} {true} {true} a = refl
  Model.intro∨₂ Boole {false} {false} {false} b = refl
  Model.intro∨₂ Boole {false} {false} {true} b = refl
  Model.intro∨₂ Boole {false} {true} {false} b = refl
  Model.intro∨₂ Boole {false} {true} {true} b = refl
  Model.intro∨₂ Boole {true} {false} {true} b = refl
  Model.intro∨₂ Boole {true} {true} {true} b = refl
  Model.elim∨ Boole {false} {false} {false} {false} ab c₁ c₂ = refl
  Model.elim∨ Boole {false} {false} {false} {true} ab c₁ c₂ = refl
  Model.elim∨ Boole {false} {false} {true} {false} ab c₁ c₂ = refl
  Model.elim∨ Boole {false} {false} {true} {true} ab c₁ c₂ = refl
  Model.elim∨ Boole {false} {true} {false} {false} ab c₁ c₂ = refl
  Model.elim∨ Boole {false} {true} {false} {true} ab c₁ c₂ = refl
  Model.elim∨ Boole {false} {true} {true} {false} ab c₁ c₂ = refl
  Model.elim∨ Boole {false} {true} {true} {true} ab c₁ c₂ = refl
  Model.elim∨ Boole {true} {false} {true} {true} ab c₁ c₂ = refl
  Model.elim∨ Boole {true} {true} {false} {true} ab c₁ c₂ = refl
  Model.elim∨ Boole {true} {true} {true} {true} ab c₁ c₂ = refl
  Model.elim∨ Boole {true} {false} {true} {false} x x₁ x₂ = {!!}
  Model.zero Boole {false} {false} = refl
  Model.zero Boole {false} {true} = refl
  Model.zero Boole {true} {false} = refl
  Model.zero Boole {true} {true} = refl
  Model.suc Boole {false} {false} {false} a = refl
  Model.suc Boole {false} {false} {true} a = refl
  Model.suc Boole {false} {true} {false} a = refl
  Model.suc Boole {false} {true} {true} a = refl
  Model.suc Boole {true} {true} {false} a = refl
  Model.suc Boole {true} {true} {true} a = refl
  Model.lem Boole {false} false = refl
  Model.lem Boole {false} true = refl
  Model.lem Boole {true} false = refl
  Model.lem Boole {true} true = refl

  trivial : Model
  Model.For trivial = Unit
  Model.pv trivial = λ x → tt
  Model._∧_ trivial = λ x x₁ → tt
  Model._∨_ trivial = λ x x₁ → tt
  Model._⊃_ trivial = λ x x₁ → tt
  Model.⊤ trivial = tt
  Model.⊥ trivial = tt
  Model.Con trivial = Unit
  Model.◇ trivial = tt
  Model._▹_ trivial = λ x x₁ → tt
  Model.Pf trivial = λ _ _ → Unit
  Model.intro∧ trivial = λ x x₁ → tt
  Model.elim∧₁ trivial = λ x → tt
  Model.elim∧₂ trivial = λ x → tt
  Model.intro⊤ trivial = tt
  Model.elim⊥ trivial = λ x → tt
  Model.elim⊃ trivial = λ x x₁ → tt
  Model.intro⊃ trivial = λ x → tt
  Model.intro∨₁ trivial = λ x → tt
  Model.intro∨₂ trivial = λ x → tt
  Model.elim∨ trivial = λ x x₁ x₂ → tt
  Model.zero trivial = tt
  Model.suc trivial = λ x → tt
  Model.lem trivial = λ A → tt

module ProperModel where

  record Model {i}{j} : Set (lsuc (i ⊔ j)) where
   field
    For : Set i
    pv          : PV → For
    _∧_ _∨_ _⊃_ : For → For → For
    ⊤ ⊥         : For

    Con : Set i
    ◇   : Con
    _▹_ : Con → For → Con

    Pf      : Con → For → Set j
    intro∧  : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    elim∧₁  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    elim∧₂  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
    intro⊤  : ∀{Γ} → Pf Γ ⊤
    elim⊥   : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A                 -- ex falso quodlibet
    elim⊃   : ∀{Γ A B} → Pf Γ (A ⊃ B) → Pf Γ A → Pf Γ B  -- modus ponens
    intro⊃  : ∀{Γ A B} → Pf (Γ ▹ A) B → Pf Γ (A ⊃ B)
    intro∨₁ : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    intro∨₂ : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    elim∨   : ∀{Γ A B C} → Pf Γ (A ∨ B) → Pf (Γ ▹ A) C → Pf (Γ ▹ B) C → Pf Γ C -- inkonzisztens
    zero    : ∀{Γ A} → Pf (Γ ▹ A) A
    suc     : ∀{Γ A B} → Pf Γ A → Pf (Γ ▹ B) A    -- weakening, gyengites
    
   ¬ : For → For
   ¬ A = A ⊃ ⊥

   ¬¬lem : ∀{A} → Pf ◇ (¬ (¬ (A ∨ ¬ A)))
   ¬¬lem {A} = intro⊃ (elim⊃ zero (intro∨₂ (intro⊃ (elim⊃ (suc zero) (intro∨₁ zero)))))

   ¬lem→⊥ : ∀{A} → Pf ◇ (¬ (A ∨ ¬ A)) → Pf ◇ ⊥
   ¬lem→⊥ p = elim⊃ ¬¬lem p

   suczero : ∀{Γ A B} → Pf ((Γ ▹ A) ▹ B) A
   suczero = suc zero

  module _ {i}{j}(M : Model {i}{j}) where
    module M = Model M
    ⟦_⟧F : Syntax.For → M.For
    ⟦ Syntax.pv x ⟧F = M.pv x
    ⟦ x Syntax.∧ y ⟧F = ⟦ x ⟧F M.∧ ⟦ y ⟧F
    ⟦ x Syntax.∨ y ⟧F = ⟦ x ⟧F M.∨ ⟦ y ⟧F
    ⟦ x Syntax.⊃ y ⟧F = ⟦ x ⟧F M.⊃ ⟦ y ⟧F
    ⟦ Syntax.⊤ ⟧F = M.⊤
    ⟦ Syntax.⊥ ⟧F = M.⊥

    -- Balint1
    ⟦_⟧C : Syntax.Con → M.Con
    ⟦ Syntax.◇ ⟧C = M.◇
    ⟦ x Syntax.▹ y ⟧C = ⟦ x ⟧C M.▹ ⟦ y ⟧F

    ⟦_⟧ : ∀{Γ A} → Syntax.Pf Γ A → M.Pf ⟦ Γ ⟧C ⟦ A ⟧F
    ⟦ Syntax.intro∧ x y ⟧ =  M.intro∧ ⟦ x ⟧ ⟦ y ⟧
    ⟦ Syntax.elim∧₁ x ⟧ = M.elim∧₁ ⟦ x ⟧
    ⟦ Syntax.elim∧₂ x ⟧ = M.elim∧₂ ⟦ x ⟧
    ⟦ Syntax.intro⊤ ⟧ = M.intro⊤
    ⟦ Syntax.elim⊥ x ⟧ = M.elim⊥ ⟦ x ⟧
    ⟦ Syntax.elim⊃ x y ⟧ = M.elim⊃ ⟦ x ⟧ ⟦ y ⟧
    ⟦ Syntax.intro⊃ x ⟧ = M.intro⊃ ⟦ x ⟧
    ⟦ Syntax.intro∨₁ x ⟧ = M.intro∨₁ ⟦ x ⟧
    ⟦ Syntax.intro∨₂ x ⟧ = M.intro∨₂ ⟦ x ⟧
    ⟦ Syntax.elim∨ x y z ⟧ = M.elim∨ ⟦ x ⟧ ⟦ y ⟧ ⟦ z ⟧
    ⟦ Syntax.zero ⟧ = M.zero
    ⟦ Syntax.suc x ⟧ = M.suc ⟦ x ⟧

  module BOOLE (pv : PV → Bool) where
    Boole :  Model
    Model.For Boole = Bool
    Model.pv Boole = pv
    Model._∧_ Boole = _&&_
    Model._∨_ Boole = _||_
    Model._⊃_ Boole = λ a b → not a || b
    Model.⊤ Boole = true
    Model.⊥ Boole = false
    Model.Con Boole = Bool
    Model.◇ Boole = true
    Model._▹_ Boole = _&&_
    Model.Pf Boole = λ Γ A → (not Γ || A) ≡ true
    Model.intro∧ Boole {false} {false} {false} a b = refl
    Model.intro∧ Boole {false} {false} {true} a b = refl 
    Model.intro∧ Boole {false} {true} {false} a b = refl
    Model.intro∧ Boole {false} {true} {true} a b = refl
    Model.intro∧ Boole {true} {true} {true} a b = refl
    Model.elim∧₁ Boole {false} {false} {false} ab = refl
    Model.elim∧₁ Boole {false} {false} {true} ab = refl
    Model.elim∧₁ Boole {false} {true} {false} ab = refl
    Model.elim∧₁ Boole {false} {true} {true} ab = refl
    Model.elim∧₁ Boole {true} {true} {true} ab = refl
    Model.elim∧₂ Boole {false} {false} {false} ab = refl
    Model.elim∧₂ Boole {false} {false} {true} ab = refl
    Model.elim∧₂ Boole {false} {true} {false} ab = refl
    Model.elim∧₂ Boole {false} {true} {true} ab = refl
    Model.elim∧₂ Boole {true} {true} {true} ab = refl
    Model.intro⊤ Boole {false} = refl
    Model.intro⊤ Boole {true} = refl
    Model.elim⊥ Boole {false} {false} b = refl
    Model.elim⊥ Boole {false} {true} b = refl
    Model.elim⊃ Boole {false} {false} {false} t a = refl
    Model.elim⊃ Boole {false} {false} {true} t a = refl
    Model.elim⊃ Boole {false} {true} {false} t a = refl
    Model.elim⊃ Boole {false} {true} {true} t a = refl
    Model.elim⊃ Boole {true} {true} {true} t a = refl
    Model.intro⊃ Boole {false} {false} {false} b = refl
    Model.intro⊃ Boole {false} {false} {true} b = refl
    Model.intro⊃ Boole {false} {true} {false} b = refl
    Model.intro⊃ Boole {false} {true} {true} b = refl
    Model.intro⊃ Boole {true} {false} {false} b = refl
    Model.intro⊃ Boole {true} {false} {true} b = refl
    Model.intro⊃ Boole {true} {true} {true} b = refl
    Model.intro∨₁ Boole {false} {false} {false} a = refl
    Model.intro∨₁ Boole {false} {false} {true} a = refl
    Model.intro∨₁ Boole {false} {true} {false} a = refl
    Model.intro∨₁ Boole {false} {true} {true} a = refl
    Model.intro∨₁ Boole {true} {true} {false} a = refl
    Model.intro∨₁ Boole {true} {true} {true} a = refl
    Model.intro∨₂ Boole {false} {false} {false} b = refl
    Model.intro∨₂ Boole {false} {false} {true} b = refl
    Model.intro∨₂ Boole {false} {true} {false} b = refl
    Model.intro∨₂ Boole {false} {true} {true} b = refl
    Model.intro∨₂ Boole {true} {false} {true} b = refl
    Model.intro∨₂ Boole {true} {true} {true} b = refl
    Model.elim∨ Boole {false} {false} {false} {false} ab c₁ c₂ = refl
    Model.elim∨ Boole {false} {false} {false} {true} ab c₁ c₂ = refl
    Model.elim∨ Boole {false} {false} {true} {false} ab c₁ c₂ = refl
    Model.elim∨ Boole {false} {false} {true} {true} ab c₁ c₂ = refl
    Model.elim∨ Boole {false} {true} {false} {false} ab c₁ c₂ = refl
    Model.elim∨ Boole {false} {true} {false} {true} ab c₁ c₂ = refl
    Model.elim∨ Boole {false} {true} {true} {false} ab c₁ c₂ = refl
    Model.elim∨ Boole {false} {true} {true} {true} ab c₁ c₂ = refl
    Model.elim∨ Boole {true} {false} {true} {true} ab c₁ c₂ = refl
    Model.elim∨ Boole {true} {true} {false} {true} ab c₁ c₂ = refl
    Model.elim∨ Boole {true} {true} {true} {true} ab c₁ c₂ = refl
    Model.zero Boole {false} {false} = refl
    Model.zero Boole {false} {true} = refl
    Model.zero Boole {true} {false} = refl
    Model.zero Boole {true} {true} = refl
    Model.suc Boole {false} {false} {false} a = refl
    Model.suc Boole {false} {false} {true} a = refl
    Model.suc Boole {false} {true} {false} a = refl
    Model.suc Boole {false} {true} {true} a = refl
    Model.suc Boole {true} {true} {false} a = refl
    Model.suc Boole {true} {true} {true} a = refl

    open Model Boole
    lem     : ∀{Γ} A → Pf Γ (A ∨ (A ⊃ ⊥))
    lem {false} false = refl
    lem {false} true = refl
    lem {true} false = refl
    lem {true} true = refl

  trivial : Model
  Model.For trivial = Unit
  Model.pv trivial = λ x → tt
  Model._∧_ trivial = λ x x₁ → tt
  Model._∨_ trivial = λ x x₁ → tt
  Model._⊃_ trivial = λ x x₁ → tt
  Model.⊤ trivial = tt
  Model.⊥ trivial = tt
  Model.Con trivial = Unit
  Model.◇ trivial = tt
  Model._▹_ trivial = λ x x₁ → tt
  Model.Pf trivial = λ _ _ → Unit
  Model.intro∧ trivial = λ x x₁ → tt
  Model.elim∧₁ trivial = λ x → tt
  Model.elim∧₂ trivial = λ x → tt
  Model.intro⊤ trivial = tt
  Model.elim⊥ trivial = λ x → tt
  Model.elim⊃ trivial = λ x x₁ → tt
  Model.intro⊃ trivial = λ x → tt
  Model.intro∨₁ trivial = λ x → tt
  Model.intro∨₂ trivial = λ x → tt
  Model.elim∨ trivial = λ x x₁ x₂ → tt
  Model.zero trivial = tt
  Model.suc trivial = λ x → tt

  module TRIV where
    open Model trivial
    lem : ∀{Γ} A → Pf Γ (A ∨ (A ⊃ ⊥))
    lem = λ A → tt

  -- Brouwer, Heyting, Kolmogorov, BHK, metacirkularis modell
  -- Balint2
  Tarski : (pv : PV → Set) → Model
  Tarski pv = record
    { For = Set
    ; pv = pv
    ; _∧_ = _×_
    ; _∨_ = _⊎_
    ; _⊃_ = λ A B → (A → B)
    ; ⊤ = Unit
    ; ⊥ = Empty
    ; Con = Set
    ; ◇ = Unit
    ; _▹_ = _×_
    ; Pf = λ Γ A → Γ → A
    ; intro∧ = λ f g → λ h → f h , g h
    ; elim∧₁ = λ a b → fst (a b)
    ; elim∧₂ = λ a b → snd (a b)
    ; intro⊤ = λ a → tt
    ; elim⊥ = λ a b → exfalso (a b)
    ; elim⊃ = λ f a γ → f γ (a γ)
    ; intro⊃ = λ f γ a → f (γ , a)
    ; intro∨₁ = λ a b → inl (a b)
    ; intro∨₂ = λ a b → inr (a b)
    ; elim∨ = λ a b c d → case (a d) (λ e → b (d , e)) λ f → c (d , f)
    ; zero = snd
    ; suc = λ γ→a γb → γ→a (fst γb)
    }

  -- Saul Kripke
  -- 30 perc +

  -- presheaf (előkéve)
  PSh : (W : Set) → (_≤_ : W → W → Set) → Set₁
  PSh W _≤_ = Σ (W → Set) (λ A → ∀ {w w' : W} → A w → w' ≤ w → A w')

  -- Kripke modell parametere: preorder (=thin category) (reszleges rendezes)

  {-
  category:

  graf + mindenutt hurokel (reflexivitas, identitas) + tranzitivitas (kompozicio) + 3 egyenlosed

                  _
                 / \id
     f       g   v |              f        print                id(b) := b
  V1---->V2---->V3_/         Bool----->Int------>String         f(b) := if b then 3 else 4
   \____________^                                               (f∘g)(x) := f(g(x))
        g∘f                                                     (id∘f)(x) = id(f(x)) = f(x)
                                                                ((f∘g)∘h)(x) = (f∘g)(h(x)) = f(g(h(x))) = f((g∘h)(x)) = (f∘(g∘h))(x)
  id∘f = f
  f∘id = f
  (f∘g)∘h = f∘(g∘h)


  Ob := For
  Mor A B := Pf ◇ (A ⊃ B)

  id : Pf ◇ (A ⊃ A)
  _∘_ : Pf ◇ (B ⊃ C) → Pf ◇ (A ⊃ B) → Pf ◇ (A ⊃ C)
  
  (A∧B) ---->  A  <------ (B∧A)
  -} 

  Kripke : (W : Set) (_≤_ : W → W → Set) → (∀ (w : W) → w ≤ w) → (∀ {w w' w'' : W} → w ≤ w' → w' ≤ w'' → w ≤ w'') → (PV → PSh W _≤_) → Model
  Kripke W _≤_ ≤-refl ≤-trans pv = record
    { For = PSh W _≤_
    ; pv = pv
    ; _∧_ = _And_
    ; _∨_ = λ Anp Bnp → let A = fst Anp; B = fst Bnp in (λ w → A w ⊎ B w) , λ {w} {w'} (Aw⊎Bw) w'≤w → case Aw⊎Bw (λ Aw → inl (snd Anp Aw w'≤w)) λ Bw → inr (snd Bnp Bw w'≤w)
    ; _⊃_ = λ Anp Bnp → let A = fst Anp; B = fst Bnp in (λ w → ∀ (w' : W) → w' ≤ w → A w' → B w') , λ {w} {w'} alllater w'≤w w'' w''≤w' Aw'' → alllater w'' (≤-trans w''≤w' w'≤w) Aw''
    ; ⊤ = Top
    ; ⊥ = (λ _ → Empty) , (λ bot _ → bot)
    ; Con = PSh W _≤_
    ; ◇ = Top
    ; _▹_ = _And_
    ; Pf = λ Γ Anp → ∀ {w' : W} → fst Γ w' → fst Anp w'
    ; intro∧ = λ {Γnp Anp Bnp} Γ→A Γ→B {w} Γw → Γ→A Γw , Γ→B Γw 
    ; elim∧₁ = λ {Γnp Anp Bnp} Γ→A∧B {w} Γw → fst (Γ→A∧B Γw)
    ; elim∧₂ = λ {Γnp Anp Bnp} Γ→A∧B {w} Γw → snd (Γ→A∧B Γw)
    ; intro⊤ = λ {_} {_} _ → tt
    ; elim⊥ = λ {Γnp} {Anp} ¬Γ {w'} Γw' → exfalso (¬Γ Γw')
    ; elim⊃ = λ {Γ}{A}{B} ab a → λ {w} γw → ab {w} γw w (≤-refl w) (a γw)
    ; intro⊃ = λ {Γ}{A}{B} b {w} γw w' w'≤w aw' → b (snd Γ γw w'≤w , aw')
    ; intro∨₁ = λ {Γ}{A}{B} a {w} γw → inl (a γw)
    ; intro∨₂ = λ {Γ}{A}{B} b {w} γw → inr (b γw)
    ; elim∨ = λ {Γ}{A}{B}{C} ab ac bc {w} γw → case (ab γw) (λ aw → ac (γw , aw)) (λ bw → bc (γw , bw))
    ; zero = snd
    ; suc = λ a γbw → a (fst γbw)        -- _∘ fst
    }
    where
      Top = (λ _ → Unit) , (λ {w} {w'} tt _ → tt)
      _And_ = λ Anp Bnp → let A = fst Anp; B = fst Bnp in (λ w → A w × B w) , (λ {w} {w'} Aw×Bw w'≤w → ((snd Anp) (fst Aw×Bw) w'≤w) , (snd Bnp (snd Aw×Bw) w'≤w))

  data W : Set where
    c d e : W
  _≤_ : W → W → Set
  x ≤ c = Unit
  d ≤ d = Unit
  e ≤ e = Unit
  _ ≤ _ = Empty

{-
   d
   ^
  /
 c  
  \
   v
   e
-}

  ≤-refl : ∀ (w : W) → w ≤ w
  ≤-refl c = tt
  ≤-refl d = tt
  ≤-refl e = tt

  ≤-trans : ∀ {w w' w'' : W} → w ≤ w' → w' ≤ w'' → w ≤ w''
  ≤-trans {c} {c} {c} p q = tt
  ≤-trans {d} {c} {c} p q = tt
  ≤-trans {d} {d} {c} p q = tt
  ≤-trans {d} {d} {d} p q = tt
  ≤-trans {e} {c} {c} p q = tt
  ≤-trans {e} {e} {c} p q = tt
  ≤-trans {e} {e} {e} p q = tt

  A : PSh W _≤_
  A = (λ { d → Unit ; _ → Empty }) , λ { {d}{d} → _ }

  postulate pv : PV → PSh W _≤_
  open Model (Kripke W _≤_ ≤-refl ≤-trans pv)
  ¬lem : Pf ◇ (A ∨ ¬ A) → Empty  -- nincs A∨¬A-nak bizonyitasa
  ¬lem a∨¬a = case (a∨¬a {c} tt) (λ e → e) (λ f → f d tt tt)
