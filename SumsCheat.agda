{-# OPTIONS --prop #-}

module SumsCheat where

open import lib

-- intuitionistic logic

record Model (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con  : Set i
    Pfs  : Con → Con → Prop j
    For  : Set i
    Pf   : Con → For → Prop j
    id   : ∀{Γ} → Pfs Γ Γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    ∙    : Con
    ε    : ∀{Γ} → Pfs Γ ∙
    _▷_  : Con → For → Con
    _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    q    : ∀{Γ A} → Pf  (Γ ▷ A) A

    _⇒_  : For → For → For
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

    ⊤    : For
    tt   : ∀{Γ} → Pf Γ ⊤

    _∧_  : For → For → For
    pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B

    _∨_  : For → For → For
    left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

  _↑ : ∀{Γ Δ A} → Pfs Γ Δ → Pfs (Γ ▷ A) (Δ ▷ A)
  σ ↑ = σ ∘ p , q
  _$_ : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf Γ A → Pf Γ B
  t $ u = app t [ id , u ]
  v⁰ : ∀{Γ A} → Pf (Γ ▷ A) A
  v⁰ = q
  v¹ : ∀{Γ A B} → Pf (Γ ▷ A ▷ B) A
  v¹ = q [ p ]
  v² : ∀{Γ A B C} → Pf (Γ ▷ A ▷ B ▷ C) A
  v² = q [ p ] [ p ]

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixr 6 _⇒_
  infixr 7 _∨_

-- syntax

module Initial where

  data For  : Set where
       ⊤    : For
       _⇒_  : For → For → For
       _∧_  : For → For → For
       _∨_  : For → For → For

  data Con  : Set where
       ∙    : Con
       _▷_  : Con → For → Con

  data Pfs  : Con → Con → Prop
  data Pf   : Con → For → Prop

  data Pfs    where
       id   : ∀{Γ} → Pfs Γ Γ
       _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
       p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
       ε    : ∀{Γ} → Pfs Γ ∙
       _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)

  data Pf     where
       _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
       q    : ∀{Γ A} → Pf  (Γ ▷ A) A
       tt   : ∀{Γ} → Pf Γ ⊤
       lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
       app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
       pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
       fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
       snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
       left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
       right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
       case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

  I : Model _ _
  I = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = id ;
    _∘_ = _∘_ ; _[_] = _[_] ; ∙ = ∙ ; ε = ε ; _▷_ = _▷_ ; _,_ = _,_ ;
    p = p ; q = q ; _⇒_ = _⇒_ ; lam = lam ; app = app ; ⊤ = ⊤ ; _∧_ =
    _∧_ ; pair = pair ; fst = fst ; snd = snd ; tt = tt ; _∨_ = _∨_ ;
    left = left ; right = right ; case = case }

  module I = Model I

module rec {i j}(M : Model i j) where

  module I = Initial
  module M = Model M

  ⟦_⟧F : I.For → M.For
  ⟦ A I.⇒ B ⟧F = ⟦ A ⟧F M.⇒ ⟦ B ⟧F
  ⟦ I.⊤ ⟧F = M.⊤
  ⟦ A I.∧ B ⟧F = ⟦ A ⟧F M.∧ ⟦ B ⟧F
  ⟦ A I.∨ B ⟧F = ⟦ A ⟧F M.∨ ⟦ B ⟧F

  ⟦_⟧C : I.Con → M.Con
  ⟦ I.∙ ⟧C = M.∙
  ⟦ Γ I.▷ A ⟧C = ⟦ Γ ⟧C M.▷ ⟦ A ⟧F

  ⟦_⟧s : ∀{Γ Δ} → I.Pfs Γ Δ  → M.Pfs ⟦ Γ ⟧C ⟦ Δ ⟧C
  ⟦_⟧P : ∀{Γ A} → I.Pf  Γ A  → M.Pf  ⟦ Γ ⟧C ⟦ A ⟧F

  ⟦ I.id ⟧s = M.id
  ⟦ σ I.∘ δ ⟧s = ⟦ σ ⟧s M.∘ ⟦ δ ⟧s
  ⟦ I.p ⟧s = M.p
  ⟦ I.ε ⟧s = M.ε
  ⟦ σ I., u ⟧s = ⟦ σ ⟧s M., ⟦ u ⟧P
  
  ⟦ u I.[ σ ] ⟧P = ⟦ u ⟧P M.[ ⟦ σ ⟧s ]
  ⟦ I.q ⟧P = M.q
  ⟦ I.lam u ⟧P = M.lam ⟦ u ⟧P
  ⟦ I.app u ⟧P = M.app ⟦ u ⟧P
  ⟦ I.tt ⟧P = M.tt
  ⟦ I.pair u v ⟧P = M.pair ⟦ u ⟧P ⟦ v ⟧P
  ⟦ I.fst u ⟧P = M.fst ⟦ u ⟧P
  ⟦ I.snd u ⟧P = M.snd ⟦ u ⟧P
  ⟦ I.left u ⟧P = M.left ⟦ u ⟧P
  ⟦ I.right u ⟧P = M.right ⟦ u ⟧P
  ⟦ I.case u v w ⟧P = M.case ⟦ u ⟧P ⟦ v ⟧P ⟦ w ⟧P

-- standard model

st : Model _ _
st = record
  { Con = Prop
  ; Pfs = λ Γ Δ → Γ → Δ
  ; For = Prop
  ; Pf = λ Γ A → Γ → A
  ; id = λ γ → γ
  ; _∘_ = λ σ δ γ → σ (δ γ)
  ; _[_] = λ u σ γ → u (σ γ)
  ; ∙ = 𝟙p
  ; ε = λ γ → *
  ; _▷_ = λ Γ A → Γ ×p A
  ; _,_ = λ σ u γ → (σ γ ,Σ u γ)
  ; p = proj₁
  ; q = proj₂
  ; _⇒_ = λ A B → A → B
  ; lam = λ u γ α → u (γ ,Σ α)
  ; app = λ u γ → u (proj₁ γ) (proj₂ γ)
  ; ⊤ = 𝟙p
  ; tt = λ γ → *
  ; _∧_ = λ A B → A ×p B
  ; pair = λ u v γ → u γ ,Σ v γ
  ; fst = λ u γ → proj₁ (u γ)
  ; snd = λ u γ → proj₂ (u γ)
  ; _∨_ = λ A B → A +p B
  ; left = λ u γ → inj₁ (u γ)
  ; right = λ u γ → inj₂ (u γ)
  ; case = λ u v w γ → ind+p _ (λ α → u (γ ,Σ α)) (λ α → v (γ ,Σ α)) (w γ)
  }

module Kripke
  (W : Set)
  (_≤_ : W → W → Prop)
  (id≤ : {w : W} → w ≤ w)
  (_∘≤_ : {w w' w'' : W} → w' ≤ w → w'' ≤ w' → w'' ≤ w)
  where
 
  record PSh : Set₁ where
    field
      ∣_∣    : W → Prop
      _∶_⟨_⟩ : ∀{w w'} → ∣_∣ w → w' ≤ w → ∣_∣ w'
  open PSh public
  
  K : Model _ _
  K = record
    { Con = PSh
    ; Pfs = λ Γ Δ → {w : W} → ∣ Γ ∣ w → ∣ Δ ∣ w
    ; For = PSh
    ; Pf  = λ Γ A → {w : W} → ∣ Γ ∣ w → ∣ A ∣ w
    ; id  = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = record { ∣_∣ = λ _ → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; ε = λ _ → *
    ; _▷_ = λ Γ A → record { ∣_∣ = λ w → ∣ Γ ∣ w ×p ∣ A ∣ w ; _∶_⟨_⟩ = λ { (γ ,Σ α) β → Γ ∶ γ ⟨ β ⟩ ,Σ A ∶ α ⟨ β ⟩ } }
    ; _,_ = λ σ u γ → σ γ ,Σ u γ
    ; p = proj₁
    ; q = proj₂
    ; _⇒_ = λ A B → record { ∣_∣ = λ w → ∀{w'} → w' ≤ w → ∣ A ∣ w' → ∣ B ∣ w' ; _∶_⟨_⟩ = λ {w w'} f β {w''} β' α → f (β ∘≤ β') α }
    ; lam = λ {Γ}{A} u γ β α → u (Γ ∶ γ ⟨ β ⟩ ,Σ α)
    ; app = λ { {Γ}{A}{B} u (γ ,Σ α) → u γ id≤ α }
    ; ⊤ = record { ∣_∣ = λ w → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; tt = λ _ → *
    ; _∧_ = λ A B → record { ∣_∣ = λ w → ∣ A ∣ w ×p ∣ B ∣ w ; _∶_⟨_⟩ = λ { (α ,Σ α') β → A ∶ α ⟨ β ⟩ ,Σ B ∶ α' ⟨ β ⟩ } }
    ; pair = λ u v γ → u γ ,Σ v γ
    ; fst = λ u γ → proj₁ (u γ)
    ; snd = λ u γ → proj₂ (u γ)
    ; _∨_ = λ A B → record { ∣_∣ = λ w → ∣ A ∣ w +p ∣ B ∣ w ; _∶_⟨_⟩ = λ α β → ind+p _ (λ x → inj₁ (A ∶ x ⟨ β ⟩)) (λ y → inj₂ (B ∶ y ⟨ β ⟩)) α }
    ; left = λ u γ → inj₁ (u γ)
    ; right = λ u γ → inj₂ (u γ)
    ; case = λ u u' v γ → ind+p _ (λ α → u (γ ,Σ α)) (λ α → u' (γ ,Σ α)) (v γ)
    }

  module K = Model K

record Pseudomor {i j i' j' : Level}(M : Model i j)(N : Model i' j') : Set (i ⊔ j ⊔ i' ⊔ j') where
  module M = Model M
  module N = Model N
  field
    Con : M.Con → N.Con
    Pfs : ∀{Γ Δ} → M.Pfs Γ Δ → N.Pfs (Con Γ) (Con Δ)
    For : M.For → N.For
    Pf  : ∀{Γ A} → M.Pf Γ A → N.Pf (Con Γ) (For A)
    ∙   : N.Pfs (Con M.∙) N.∙
    ∙⁻¹ : N.Pfs N.∙ (Con M.∙)
    ▷   : ∀{Γ A} → N.Pfs (Con (Γ M.▷ A)) (Con Γ N.▷ For A)
    ▷⁻¹ : ∀{Γ A} → N.Pfs (Con Γ N.▷ For A) (Con (Γ M.▷ A))

  ⇒ : ∀{A B} → N.Pf (N.∙ N.▷ For (A M.⇒ B)) (For A N.⇒ For B)
  ⇒ {A}{B} = N.lam (Pf (M.app {Γ = M.∙ M.▷ A M.⇒ B}{A = A} M.q) N.[ ▷⁻¹ N.∘ ((▷⁻¹ N.∘ (∙⁻¹ N.↑)) N.↑) ])

  ∧ : ∀{A B} → N.Pf (N.∙ N.▷ For (A M.∧ B)) (For A N.∧ For B)
  ∧ = N.pair (Pf (M.fst M.q)) (Pf (M.snd M.q)) N.[ ▷⁻¹ N.∘ (∙⁻¹ N.↑) ]
  ∧⁻¹ : ∀{A B} → N.Pf (N.∙ N.▷ For A N.∧ For B) (For (A M.∧ B))
  ∧⁻¹ = Pf (M.pair M.v¹ M.q) N.[ ▷⁻¹ N.∘ ((▷⁻¹ N.∘ ((∙⁻¹ N.∘ N.p) N., N.fst N.q)) N., N.snd N.q) ]

  ∨⁻¹ : ∀{A B} → N.Pf (N.∙ N.▷ For A N.∨ For B) (For (A M.∨ B))
  ∨⁻¹ = N.case
    (Pf (M.left  M.v⁰) N.[ ▷⁻¹ N.∘ ((∙⁻¹ N.∘ N.ε) N., N.q) ])
    (Pf (M.right M.v⁰) N.[ ▷⁻¹ N.∘ ((∙⁻¹ N.∘ N.ε) N., N.q) ])
    N.q

module Gluing {i j i' j' : Level}(M : Model i j)(N : Model i' j')(F : Pseudomor M N) where

  -- we can only define gluing like this because Pf is in Prop. for
  -- STT, the target of F needs to know more than just a model of STT
  -- (to be able to define the predicate for function space)

  module M = Model M
  module N = Model N
  module F = Pseudomor F

  Con = Σ M.Con λ Γ → Σsp N.Con λ PΓ → N.Pfs PΓ (F.Con Γ)
  Pfs : Con → Con → Prop _
  Pfs (Γ ,Σ PΓ ,Σ πΓ) (Δ ,Σ PΔ ,Σ πΔ) = M.Pfs Γ Δ ×p N.Pfs PΓ PΔ
  For = Σ M.For λ A → Σsp N.For λ PA → N.Pf (N.∙ N.▷ PA) (F.For A)
  Pf : Con → For → Prop _
  Pf (Γ ,Σ PΓ ,Σ πΓ) (A ,Σ PA ,Σ πA) = M.Pf Γ A ×p N.Pf PΓ PA
  
  id : ∀{Γ} → Pfs Γ Γ
  id = M.id ,Σ N.id
  _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
  ((σ ,Σ Pσ) ∘ (δ ,Σ Pδ)) = (σ M.∘ δ) ,Σ (Pσ N.∘ Pδ)
  _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
  ((u ,Σ Pu) [ σ ,Σ Pσ ]) = u M.[ σ ] ,Σ (Pu N.[ Pσ ])
  ∙ : Con
  ∙ = M.∙ ,Σ N.∙ ,Σ F.∙⁻¹
  ε : ∀{Γ} → Pfs Γ ∙
  ε = M.ε ,Σ N.ε
  _▷_  : Con → For → Con
  (Γ ,Σ PΓ ,Σ πΓ) ▷ (A ,Σ PA ,Σ πA) = (Γ M.▷ A) ,Σ (PΓ N.▷ PA) ,Σ (F.▷⁻¹ N.∘ (πΓ N.∘ N.p N., (πA N.[ N.ε N., N.q ])))
  _,_ : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
  ((σ ,Σ Pσ) , (t ,Σ Pt)) = (σ M., t) ,Σ (Pσ N., Pt)
  p : ∀{Γ A} → Pfs (Γ ▷ A) Γ
  p = M.p ,Σ N.p
  q : ∀{Γ A} → Pf  (Γ ▷ A) A
  q = M.q ,Σ N.q
  
  _⇒_ : For → For → For
  (A ,Σ PA ,Σ πA) ⇒ (B ,Σ PB ,Σ πB) = (A M.⇒ B) ,Σ (F.For (A M.⇒ B) N.∧ (PA N.⇒ PB)) ,Σ N.fst N.q
  lam : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
  lam {Γ ,Σ PΓ ,Σ πΓ}(t ,Σ Pt) = M.lam t ,Σ N.pair (F.Pf (M.lam t) N.[ πΓ ]) (N.lam Pt)
  app : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
  app (t ,Σ Pt) = M.app t ,Σ N.app (N.snd Pt)
  
  ⊤ : For
  ⊤ = M.⊤ ,Σ N.⊤ ,Σ (F.Pf M.tt N.[ F.∙⁻¹ N.∘ N.ε ])
  tt : ∀{Γ} → Pf Γ ⊤
  tt {Γ} = M.tt ,Σ N.tt

  _∧_  : For → For → For
  (A ,Σ PA ,Σ πA) ∧ (B ,Σ PB ,Σ πB) =
    (A M.∧ B) ,Σ
    (PA N.∧ PB) ,Σ
    (F.∧⁻¹ N.[ N.ε N., N.pair (πA N.[ N.ε N., N.fst N.q ]) (πB N.[ N.ε N., N.snd N.q ]) ])
  pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
  pair (u ,Σ Pu) (v ,Σ Pv) = M.pair u v ,Σ N.pair Pu Pv
  fst : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
  fst (u ,Σ Pu) = M.fst u ,Σ N.fst Pu
  snd : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
  snd (u ,Σ Pu) = M.snd u ,Σ N.snd Pu
  
  _∨_ : For → For → For
  (A ,Σ PA ,Σ πA) ∨ (B ,Σ PB ,Σ πB) =
    (A M.∨ B) ,Σ
    (PA N.∨ PB) ,Σ
    (F.∨⁻¹ N.[ N.ε N., N.case (N.left (πA N.[ N.ε N., N.q ])) (N.right (πB N.[ N.ε N., N.q ])) N.q ])
  left  : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
  left  (u ,Σ Pu) = M.left  u ,Σ N.left  Pu
  right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
  right (u ,Σ Pu) = M.right u ,Σ N.right Pu
  case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C
  case {Γ ,Σ PΓ ,Σ πΓ}{A ,Σ PA ,Σ πA}{B ,Σ PB ,Σ πB}{C ,Σ PC ,Σ πC}(u ,Σ Pu)(v ,Σ Pv)(w ,Σ Pw) = M.case u v w ,Σ N.case Pu Pv Pw

  G : Model _ _
  
  G = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = λ
    {Γ} → id {Γ} ; _∘_ = λ {Γ}{Θ}{Δ} → _∘_ {Γ}{Θ}{Δ} ; _[_] = λ
    {Γ}{Δ}{A} → _[_] {Γ}{Δ}{A} ; ∙ = ∙ ; ε = λ {Γ} → ε {Γ} ; _▷_ = _▷_
    ; _,_ = λ {Γ}{Δ}{A} → _,_ {Γ}{Δ}{A} ; p = λ {Γ}{A} → p {Γ}{A} ; q
    = λ {Γ}{A} → q {Γ}{A} ; _⇒_ = _⇒_ ; lam = λ {Γ}{A}{B} → lam
    {Γ}{A}{B} ; app = λ {Γ}{A}{B} → app {Γ}{A}{B} ; ⊤ = ⊤ ; _∧_ = _∧_
    ; pair = λ {Γ}{A}{B} → pair {Γ}{A}{B} ; fst = λ {Γ}{A}{B} → fst
    {Γ}{A}{B} ; snd = λ {Γ}{A}{B} → snd {Γ}{A}{B} ; tt = λ {Γ} → tt
    {Γ} ; _∨_ = _∨_ ; left = λ {Γ}{A}{B} → left {Γ}{A}{B} ; right = λ
    {Γ}{A}{B} → right {Γ}{A}{B} ; case = λ {Γ}{A}{B}{C} → case
    {Γ}{A}{B}{C} }

module Yoneda where

  I = Initial.I
  module I = Model I
  open Kripke
  Kr = Kripke.K I.Con I.Pfs I.id I._∘_

  Y : Pseudomor I Kr
  ∣ Pseudomor.Con Y Γ ∣ w = I.Pfs w Γ
  Pseudomor.Con Y Γ ∶ σ ⟨ f ⟩ = σ I.∘ f
  Pseudomor.Pfs Y σ f = σ I.∘ f
  ∣ Pseudomor.For Y A ∣ w = I.Pf w A
  Pseudomor.For Y A ∶ t ⟨ f ⟩ = t I.[ f ]
  Pseudomor.Pf Y t f = t I.[ f ]
  Pseudomor.∙ Y _ = *
  Pseudomor.∙⁻¹ Y _ = I.ε
  Pseudomor.▷ Y σ = (I.p I.∘ σ) ,Σ I.q I.[ σ ]
  Pseudomor.▷⁻¹ Y (σ ,Σ t) = σ I., t

module CutFree {i}{j}(M : Model i j) where

  open Model M

  data Tel : Con → Prop i where
    ∙tel : Tel ∙
    _▷tel_ : ∀{Γ} → Tel Γ → ∀ A → Tel (Γ ▷ A)
  
  data Var : ∀ Γ A → Pf Γ A → Prop (i ⊔ j) where
    vz : ∀{Γ A} → Var (Γ ▷ A) A q
    vs : ∀{Γ A B u} → Var Γ A u → Var (Γ ▷ B) A (u [ p ])

  data Ne : ∀ Γ A → Pf Γ A → Prop (i ⊔ j)
  data Nf : ∀ Γ A → Pf Γ A → Prop (i ⊔ j)

  data Ne where
    var : ∀{Γ A u} → Var Γ A u → Ne Γ A u
    appne : ∀{Γ A B u v} → Ne Γ (A ⇒ B) u → Nf Γ A v → Ne Γ B (u $ v)
    fstne : ∀{Γ A B u} → Ne Γ (A ∧ B) u → Ne Γ A (fst u)
    sndne : ∀{Γ A B u} → Ne Γ (A ∧ B) u → Ne Γ B (snd u)
    casene : ∀{Γ A B C u v w} → Nf (Γ ▷ A) C u → Nf (Γ ▷ B) C v → Ne Γ (A ∨ B) w → Ne Γ C (case u v w)

  data Nf where
    neu : ∀{Γ A u} → Ne Γ A u → Nf Γ A u
    ttnf : ∀{Γ} → Nf Γ ⊤ tt
    lamnf : ∀{Γ A B u} → Nf (Γ ▷ A) B u → Nf Γ (A ⇒ B) (lam u)
    pairnf : ∀{Γ A B u v} → Nf Γ A u → Nf Γ B v → Nf Γ (A ∧ B) (pair u v)
    leftnf : ∀{Γ A B u} → Nf Γ A u → Nf Γ (A ∨ B) (left u)
    rightnf : ∀{Γ A B u} → Nf Γ B u → Nf Γ (A ∨ B) (right u)

  data Ren Γ : ∀ Δ → Pfs Γ Δ → Prop (i ⊔ j) where
    εren : Ren Γ ∙ ε
    _,ren_ : ∀{Δ A σ u} → Ren Γ Δ σ → Var Γ A u → Ren Γ (Δ ▷ A) (σ , u)

  -- do we have decidable equality for Nf? not yet. we need normal
  -- formulas and probably telescopic contexts as well

  {-
  -- this only works if we have telescopes:
  _[_]var : ∀{Δ A u Γ σ} → Var Δ A u → Ren Γ Δ σ → Var Γ A (u [ σ ])
  vz [ β ]var = {!β!}
  vs x [ β ]var = {!!}
  -}
