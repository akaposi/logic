{-# OPTIONS --prop --rewriting #-}

open import lib

module FirstOrderFinitary2
  (funar : ℕ → Set) -- adott aritasu fuggvenyszimbolumbol hany van pl. Peano (0,1,suc,_+_,_*_): funar 0 = 2, funar 1 = 1, funar 2 = 2, funar n = 0 (n>2)
  (relar : ℕ → Set) -- adott aritasu relacioszimbolumbol hany van
  where

-- finitary presentation, one-sorted, one context, except in I we separate them

-- TODO: can we present this using HOAS? in a way that the context dependencies are explicit (e.g. For cannot depend on proof-variables)

-- TODO: add ∨, ∃
-- TODO: add uniqueness or dependent eliminator

-- TODO: use the merged context version in Model, but split contexts in I

record Model (i j k l : Level) : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  field
    -- context of term variables (in the propositional case this is always empty)
    Con   : Set i
    Sub   : Con → Con → Set j
    _∘_   : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
    ass   : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
    id    : ∀{Γ} → Sub Γ Γ
    idl   : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
    idr   : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
    ◆     : Con
    ε     : ∀{Γ} → Sub Γ ◆
    ◆η    : ∀{Γ}(σ : Sub Γ ◆) → σ ≡ ε

    -- formulas (For : Set)
    For   : Con → Set k
    _[_]F : ∀{Γ Δ} → For Γ → Sub Δ Γ → For Δ
    [∘]F  : ∀{Γ}{K : For Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → K [ γ ∘ δ ]F ≡ K [ γ ]F [ δ ]F
    [id]F : ∀{Γ}{K : For Γ} → K [ id ]F ≡ K

    -- proofs (Pf : For → Prop+)
    Pf    : (Γ : Con) → For Γ → Prop l
    _[_]p : ∀{Γ K} → Pf Γ K → ∀{Δ} → (γ : Sub Δ Γ) → Pf Δ (K [ γ ]F)
    _▸p_  : (Γ : Con) → For Γ → Con
    _,p_  : ∀{Γ Δ} → (γ : Sub Δ Γ) → ∀{K} → Pf Δ (K [ γ ]F) → Sub Δ (Γ ▸p K)
    pp    : ∀{Γ K} → Sub (Γ ▸p K) Γ
    qp    : ∀{Γ K} → Pf  (Γ ▸p K) (K [ pp ]F)
    ▸pβ₁  : ∀{Γ Δ}{γ : Sub Δ Γ}{K}{k : Pf Δ (K [ γ ]F)} → pp ∘ (γ ,p k) ≡ γ

    -- propositional connectives (they don't depend on the term context)

    -- ⊥ : For, exfalso : Pf ⊥ → Pf K
    ⊥    : ∀{Γ} → For Γ
    ⊥[]  : ∀{Γ Δ}{γ : Sub Δ Γ} → ⊥ [ γ ]F ≡ ⊥
    exfalso : ∀{Γ K} → Pf Γ ⊥ → Pf Γ K

    -- ⊤ : For, tt : Pf ⊤
    ⊤    : ∀{Γ} → For Γ
    ⊤[]  : ∀{Γ Δ}{γ : Sub Δ Γ} → ⊤ [ γ ]F ≡ ⊤
    tt   : ∀{Γ} → Pf Γ ⊤

    -- ⊃ : For → For → For, (Pf K → Pf L) ↔ Pf (K ⊃ L)
    _⊃_  : ∀{Γ} → For Γ → For Γ → For Γ
    ⊃[]  : ∀{Γ K L Δ}{γ : Sub Δ Γ} → (K ⊃ L) [ γ ]F ≡ K [ γ ]F ⊃ L [ γ ]F
    lam  : ∀{Γ K L} → Pf (Γ ▸p K) (L [ pp ]F) → Pf Γ (K ⊃ L)
    app  : ∀{Γ K L} → Pf Γ (K ⊃ L) → Pf (Γ ▸p K) (L [ pp ]F)

    -- ∧ : For → For → For, Pf K × Pf L ↔ Pf (K × L)
    _∧_  : ∀{Γ} → For Γ → For Γ → For Γ
    ∧[]  : ∀{Γ K L Δ}{γ : Sub Δ Γ} → (K ∧ L) [ γ ]F ≡ K [ γ ]F ∧ L [ γ ]F
    pair : ∀{Γ K L} → Pf Γ K → Pf Γ L → Pf Γ (K ∧ L)
    fst  : ∀{Γ K L} → Pf Γ (K ∧ L) → Pf Γ K
    snd  : ∀{Γ K L} → Pf Γ (K ∧ L) → Pf Γ L

    -- terms (Tm : Set+)
    Tm    : Con → Set j
    _[_]t : ∀{Γ} → Tm Γ → ∀{Δ} → Sub Δ Γ → Tm Δ
    [∘]t  : ∀{Γ}{t : Tm Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → t [ γ ∘ δ ]t ≡ t [ γ ]t [ δ ]t
    [id]t : ∀{Γ}{t : Tm Γ} → t [ id ]t ≡ t
    _▸t   : Con → Con
    _,t_  : ∀{Γ Δ} → Sub Δ Γ → Tm Δ → Sub Δ (Γ ▸t)
    pt    : ∀{Γ} → Sub (Γ ▸t) Γ
    qt    : ∀{Γ} → Tm (Γ ▸t)
    ▸tβ₁  : ∀{Γ Δ}{γ : Sub Δ Γ}{t : Tm Δ} → (pt ∘ (γ ,t t)) ≡ γ
    ▸tβ₂  : ∀{Γ Δ}{γ : Sub Δ Γ}{t : Tm Δ} → (qt [ γ ,t t ]t) ≡ t
    ▸tη   : ∀{Γ Δ}{γt : Sub Δ (Γ ▸t)} → ((pt ∘ γt) ,t (qt [ γt ]t)) ≡ γt

    -- telescopes of terms (Tms : ℕ → Set, Tms 0 ≅ ⊤, Tms (1+n) ≅ Tms n × Tm)
    Tms    : Con → ℕ → Set j
    _[_]ts : ∀{Γ n} → Tms Γ n → ∀{Δ} → Sub Δ Γ → Tms Δ n
    [∘]ts  : ∀{Γ n}{ts : Tms Γ n}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → ts [ γ ∘ δ ]ts ≡ ts [ γ ]ts [ δ ]ts
    [id]ts : ∀{Γ n}{ts : Tms Γ n} → ts [ id ]ts ≡ ts
    εs     : ∀{Γ} → Tms Γ zero
    ◆sη    : ∀{Γ}(ts : Tms Γ zero) → ts ≡ εs
    _,s_   : ∀{Γ n} → Tms Γ n → Tm Γ → Tms Γ (suc n)
    π₁     : ∀{Γ n} → Tms Γ (suc n) → Tms Γ n
    π₂     : ∀{Γ n} → Tms Γ (suc n) → Tm Γ
    ▸sβ₁   : ∀{Γ n}{ts : Tms Γ n}{t : Tm Γ} → π₁ (ts ,s t) ≡ ts
    ▸sβ₂   : ∀{Γ n}{ts : Tms Γ n}{t : Tm Γ} → π₂ (ts ,s t) ≡ t
    ▸sη    : ∀{Γ n}{ts : Tms Γ (suc n)} → π₁ ts ,s π₂ ts ≡ ts
    ,[]    : ∀{Γ n}{ts : Tms Γ n}{t : Tm Γ}{Δ}{γ : Sub Δ Γ} → (ts ,s t) [ γ ]ts ≡ ts [ γ ]ts ,s t [ γ ]t

    -- function and relation symbols (fun : (n : ℕ) → funar n → Tms n → Tm, rel : (n : ℕ) → relar n → Tms n → For)
    fun  : ∀{Γ}(n : ℕ) → funar n → Tms Γ n → Tm Γ
    fun[] : ∀{Γ n a ts Δ}{γ : Sub Δ Γ} → fun n a ts [ γ ]t ≡ fun n a (ts [ γ ]ts)
    rel  : ∀{Γ}(n : ℕ) → relar n → Tms Γ n → For Γ
    rel[] : ∀{Γ n a ts Δ}{γ : Sub Δ Γ} → rel n a ts [ γ ]F ≡ rel n a (ts [ γ ]ts)

    -- first order connectives

    -- ∀ : (Tm → For) → For, ((t : Tm) → Pf (K t)) ↔ Pf (∀ K)
    ∀'    : ∀{Γ} → For (Γ ▸t) → For Γ
    ∀[]   : ∀{Γ K Δ}{γ : Sub Δ Γ} → (∀' K) [ γ ]F ≡ ∀' (K [ (γ ∘ pt) ,t qt ]F)
    mk∀   : ∀{Γ K} → Pf (Γ ▸t) K → Pf Γ (∀' K)
    un∀   : ∀{Γ K} → Pf Γ (∀' K) → Pf (Γ ▸t) K

    -- Eq : Tm → Tm → For, ref : (t : Tm) → Eq t t, subst : (K : Tm → For) → Pf (Eq t t') → Pf (K t) → Pf (K t')
    Eq    : ∀{Γ} → Tm Γ → Tm Γ → For Γ
    Eq[]  : ∀{Γ Δ}{γ : Sub Δ Γ}{t t' : Tm Γ} → (Eq t t') [ γ ]F ≡ Eq (t [ γ ]t) (t' [ γ ]t)
    ref   : ∀{Γ}{t : Tm Γ} → Pf Γ (Eq t t)
    subst' : ∀{Γ}(K : For (Γ ▸t)){t t' : Tm Γ} → Pf Γ (Eq t t') → Pf Γ (K [ id ,t t ]F) → Pf Γ (K [ id ,t t' ]F)

    -- Lindenbaum-Tarski : ∀{Γ}(A B : For Γ) → Pf Γ (A ⊃ B ∧ B ⊃ A) → A ≡ B

  infixl 5 _▸t _▸p_
  infixl 5 _,t_ _,p_ _,s_
  infixr 7 _∘_ _∘p_
  infixl 8 _[_]t _[_]F _[_]p
  infixr 6 _⊃_
  infixr 7 _∧_
  infixl 6 _$_

  ,∘ : ∀{Γ Δ}{γ : Sub Δ Γ}{t : Tm Δ}{Θ}{δ : Sub Θ Δ} → (γ ,t t) ∘ δ ≡ γ ∘ δ ,t t [ δ ]t
  ,∘ {Γ}{Δ}{γ}{t}{Θ}{δ} = trans (sym ▸tη) (cong (λ z → proj₁ z ,t proj₂ z) (mk,= (trans (sym ass) (cong (_∘ δ) ▸tβ₁)) (trans [∘]t (cong (_[ δ ]t) ▸tβ₂))))
  
  ▸tη' : ∀{Γ} → id {Γ ▸t} ≡ pt ,t qt
  ▸tη' {Γ} = trans (sym ▸tη) (cong (λ z → proj₁ z ,t proj₂ z) (mk,= idr [id]t))

  _$_ : ∀{Γ K L} → Pf Γ (K ⊃ L) → Pf Γ K → Pf Γ L
  _$_ {Γ}{K}{L} m k = substp (Pf Γ) (trans (sym [∘]F) (trans (cong (L [_]F) ▸pβ₁) [id]F)) (app m [ id ,p substp (Pf Γ) (sym [id]F) k ]p)

  un∀' : ∀{Γ K} → Pf Γ (∀' K) → (t : Tm Γ) → Pf Γ (K [ id ,t t ]F)
  un∀' {Γ}{K} k t = (un∀ k) [ id ,t t ]p

{-
record Morphism {i j k l}(I : Model i j k l){i' j' k' l'}(J : Model i' j' k' l') : Set (i ⊔ j ⊔ k ⊔ l ⊔ i' ⊔ j' ⊔ k' ⊔ l') where
  module I = Model I
  module J = Model J
  field
    Con    : I.Con → J.Con
    Sub    : ∀{Γ Δ} → I.Sub Δ Γ → J.Sub (Con Δ) (Con Γ)
    _∘_    : ∀{Γ Δ}(γ : I.Sub Δ Γ){Θ}(δ : I.Sub Θ Δ) → Sub (γ I.∘ δ) ≡ Sub γ J.∘ Sub δ
    id     : ∀{Γ} → Sub (I.id {Γ}) ≡ J.id
    ◆      : Con I.◆ ≡ J.◆
    ε      : ∀{Γ} → transport (J.Sub (Con Γ)) ◆  (Sub (I.ε {Γ})) ≡ J.ε {Con Γ}
    Tm     : ∀{Γ} → I.Tm Γ → J.Tm (Con Γ)
    _[_]t  : ∀{Γ}(t : I.Tm Γ){Δ}(γ : I.Sub Δ Γ) → Tm (t I.[ γ ]t) ≡ Tm t J.[ Sub γ ]t
    _▸t    : (Γ : I.Con) → Con (Γ I.▸t) ≡ Con Γ J.▸t
    _,t_   : ∀{Γ Δ}(γ : I.Sub Δ Γ)(t : I.Tm Δ) → transport (J.Sub (Con Δ)) (Γ ▸t) (Sub (γ I.,t t)) ≡ Sub γ J.,t Tm t
    pt     : ∀{Γ} → transport (λ z → J.Sub z (Con Γ)) (Γ ▸t) (Sub (I.pt {Γ})) ≡ J.pt
    qt     : ∀{Γ} → transport J.Tm (Γ ▸t) (Tm (I.qt {Γ})) ≡ J.qt
    Tms    : ∀{Γ n} → I.Tms Γ n → J.Tms (Con Γ) n
    _[_]ts : ∀{Γ n}(ts : I.Tms Γ n){Δ}(γ : I.Sub Δ Γ) → Tms (ts I.[ γ ]ts) ≡ Tms ts J.[ Sub γ ]ts
    εs     : ∀{Γ} → Tms (I.εs {Γ}) ≡ J.εs
    _,s_   : ∀{Γ n}(ts : I.Tms Γ n)(t : I.Tm Γ) → Tms (ts I.,s t) ≡ Tms ts J.,s Tm t
    π₁     : ∀{Γ n}(ts : I.Tms Γ (suc n)) → Tms (I.π₁ ts) ≡ J.π₁ (Tms ts)
    π₂     : ∀{Γ n}(ts : I.Tms Γ (suc n)) → Tm (I.π₂ ts) ≡ J.π₂ (Tms ts)
    For    : ∀{Γ} → I.For Γ → J.For (Con Γ)
    _[_]F  : ∀{Γ}(K : I.For Γ){Δ}(γ : I.Sub Δ Γ) → For (K I.[ γ ]F) ≡ For K J.[ Sub γ ]F
    Conp   : ∀{Γ} → I.Conp Γ → J.Conp (Con Γ)
    _[_]C  : ∀{Γ}(Γp : I.Conp Γ){Δ}(γ : I.Sub Δ Γ) → Conp (Γp I.[ γ ]C) ≡ (Conp Γp J.[ Sub γ ]C) 
    Subp   : ∀{Γ}{Γp' Γp : I.Conp Γ} → I.Subp Γp' Γp → J.Subp (Conp Γp') (Conp Γp)
    ◆p     : ∀{Γ} → Conp (I.◆p {Γ}) ≡ J.◆p
    Pf     : ∀{Γ}{Γp : I.Conp Γ}{K : I.For Γ} → I.Pf Γp K → J.Pf (Conp Γp) (For K)
    _▸p_   : ∀{Γ}(Γp : I.Conp Γ)(K : I.For Γ) → Conp (Γp I.▸p K) ≡ Conp Γp J.▸p For K
    ⊥      : ∀{Γ} → For (I.⊥ {Γ}) ≡ J.⊥
    ⊤      : ∀{Γ} → For (I.⊤ {Γ}) ≡ J.⊤
    _⊃_    : ∀{Γ}(K L : I.For Γ) → For (K I.⊃ L) ≡ For K J.⊃ For L
    _∧_    : ∀{Γ}(K L : I.For Γ) → For (K I.∧ L) ≡ For K J.∧ For L
    fun    : ∀{Γ}(n : ℕ)(a : funar n)(ts : I.Tms Γ n) → Tm (I.fun n a ts) ≡ J.fun n a (Tms ts)
    rel    : ∀{Γ}(n : ℕ)(a : relar n)(ts : I.Tms Γ n) → For (I.rel n a ts) ≡ J.rel n a (Tms ts)
    ∀'     : ∀{Γ}(K : I.For (Γ I.▸t)) → For (I.∀' K) ≡ J.∀' (transport J.For (Γ ▸t) (For K))
    Eq     : ∀{Γ}(t t' : I.Tm Γ) → For (I.Eq t t') ≡ J.Eq (Tm t) (Tm t')

  infixl 5 _▸t _▸p_
  infixl 5 _,t_
  infixr 7 _∘_
  infixl 8 _[_]t _[_]F _[_]C
  infixr 6 _⊃_
  infixr 7 _∧_
-}

-- syntax (this is a definable quotient)
module I where
  infixl 5 _▸t _▸p_
  infixl 5 _,t_ _,p_
  infixr 7 _∘_ _∘p_ _∘pv_
  infixl 8 _[_]t _[_]F _[_]C _[_]P _[_]p _[_]v _[_]s _[_]ts _[_]PV _[_]pv
  infixr 6 _⊃_
  infixr 7 _∧_
  infixl 6 _$_

  data Cont  : Set where
    ◆t  : Cont 
    _▸t : Cont  → Cont 

  -- maybe instead of V we could use a point-weakening operation
  module V where

    data Tm : Cont → Set where
      vz : ∀{Γ} → Tm (Γ ▸t)
      vs : ∀{Γ} → Tm Γ → Tm (Γ ▸t)

    Sub : Cont → Cont → Set
    Sub Δt ◆t      = 𝟙
    Sub Δt (Γt ▸t) = Sub Δt Γt × Tm Δt

    _[_] : ∀{Γ} → Tm Γ → ∀{Δ} → Sub Δ Γ → Tm Δ
    vz [ γ ,Σ t ] = t
    vs t [ γ ,Σ _ ] = t [ γ ]

    _∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
    _∘_ {Γ = ◆t}   γ δ = *
    _∘_ {Γ = Γ ▸t} (γ ,Σ t) δ = γ ∘ δ ,Σ t [ δ ]

    [∘] : ∀{Γ}{t : Tm Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → t [ γ ∘ δ ] ≡ t [ γ ] [ δ ]
    [∘] {t = vz} = refl
    [∘] {t = vs t} = [∘] {t = t}

    ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
    ass {Γ = ◆t} = refl
    ass {Γ = Γ ▸t}{γ = γ ,Σ t} = mk,= (ass {Γ = Γ}) (sym ([∘] {t = t}))

    wk : ∀{Γ Δ} → Sub Δ Γ → Sub (Δ ▸t) Γ
    wk {◆t}    _        = *
    wk {Γ ▸t} (γ ,Σ t) = wk γ ,Σ vs t

    id : ∀{Γ} → Sub Γ Γ
    id {◆t}    = *
    id {Γ ▸t} = wk id ,Σ vz

    wk∘ : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{t : Tm Θ} → (wk γ ∘ (δ ,Σ t)) ≡ γ ∘ δ
    wk∘ {◆t}                  = refl
    wk∘ {Γ ▸t}{γ = γ ,Σ t}{δ = δ} = cong (_,Σ (t [ δ ])) (wk∘ {γ = γ}) 

    idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
    idl {Γ = ◆t}    = refl
    idl {Γ = Γ ▸t}{γ = γ ,Σ t} = cong (_,Σ t) (trans (wk∘ {γ = id}) (idl {γ = γ}))

    vs[] : ∀{Γ}{t : Tm Γ}{Δ}{γ : Sub Δ Γ} → t [ wk γ ] ≡ vs (t [ γ ])
    vs[] {t = vz}                = refl
    vs[] {t = vs t}{γ = γ ,Σ t'} = vs[] {t = t}

    [id] : ∀{Γ}{t : Tm Γ} → t [ id ] ≡ t
    [id] {t = vz}   = refl
    [id] {t = vs t} = trans (vs[] {t = t}) (cong vs ([id] {t = t}))

    idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
    idr {◆t}                 = refl
    idr {Γ ▸t} {γ = γ ,Σ t} = mk,= (idr {γ = γ}) [id]
  open V using (vz; vs)

  data Tm (Γt : Cont) : Set
  Tms : Cont → ℕ → Set

  data Tm Γt where
    var  : V.Tm Γt → Tm Γt
    fun  : (n : ℕ) → funar n → Tms Γt n → Tm Γt
  Tms Γt zero = 𝟙
  Tms Γt (suc n) = Tms Γt n × Tm Γt

  Subt : Cont → Cont → Set
  Subt Δt ◆t       = 𝟙
  Subt Δt (Γt ▸t) = Subt Δt Γt × Tm Δt

  _[_]v : ∀{Γt Δt} → V.Tm Γt → Subt Δt Γt → Tm Δt
  vz [ γ ,Σ t ]v = t
  (vs x) [ γ ,Σ t ]v = x [ γ ]v

  _[_]t  : ∀{Γt} → Tm Γt → ∀{Δt} → Subt Δt Γt → Tm Δt
  _[_]ts : ∀{Γt n} → Tms Γt n → ∀{Δt} → Subt Δt Γt → Tms Δt n
  var x [ γ ]t = x [ γ ]v
  fun n a ts [ γ ]t = fun n a (ts [ γ ]ts)
  _[_]ts {n = zero}  _         _ = *
  _[_]ts {n = suc n} (ts ,Σ t) γ = ts [ γ ]ts ,Σ t [ γ ]t

  εt : ∀{Γt} → Subt Γt ◆t
  εt = *

  _∘t_ : ∀{Γt Δt} → Subt Δt Γt → ∀{Θt} → Subt Θt Δt → Subt Θt Γt
  _∘t_ {Γt = ◆t}    _        _ = *
  _∘t_ {Γt = Γt ▸t} (γ ,Σ t) δ = γ ∘t δ ,Σ t [ δ ]t

  [∘]v : ∀{Γt}{x : V.Tm Γt}{Δt}{γ : Subt Δt Γt}{Θt}{δ : Subt Θt Δt} → x [ γ ∘t δ ]v ≡ x [ γ ]v [ δ ]t
  [∘]v {x = vz}               = refl
  [∘]v {x = vs x}{γ = γ ,Σ t} = [∘]v {x = x}

  [∘]t : ∀{Γt}{t : Tm Γt}{Δt}{γ : Subt Δt Γt}{Θt}{δ : Subt Θt Δt} → t [ γ ∘t δ ]t ≡ t [ γ ]t [ δ ]t
  [∘]ts : ∀{Γt n}{ts : Tms Γt n}{Δt}{γ : Subt Δt Γt}{Θt}{δ : Subt Θt Δt} → ts [ γ ∘t δ ]ts ≡ ts [ γ ]ts [ δ ]ts
  [∘]t {t = var x} = [∘]v {x = x}
  [∘]t {t = fun n a ts} = cong (fun n a) ([∘]ts {ts = ts})
  [∘]ts {n = zero}                = refl
  [∘]ts {n = suc n}{ts = ts ,Σ t} = mk,= [∘]ts ([∘]t {t = t})

  ass : ∀{Γt Δt}{γ : Subt Δt Γt}{Θt}{δ : Subt Θt Δt}{Ξt}{θ : Subt Ξt Θt} → (γ ∘t δ) ∘t θ ≡ γ ∘t (δ ∘t θ)
  ass {◆t}                  = refl
  ass {Γt ▸t} {γ = γ ,Σ t} = mk,= (ass {Γt}) (sym ([∘]t {t = t}))

  ⌜_⌝ : ∀{Γt Δt} → V.Sub Δt Γt → Subt Δt Γt
  ⌜_⌝ {Γt = ◆t}    _        = *
  ⌜_⌝ {Γt = Γt ▸t} (γ ,Σ x) = ⌜ γ ⌝ ,Σ var x

  idt : ∀{Γt} → Subt Γt Γt
  idt = ⌜ V.id ⌝

  ⌜wk⌝∘ : ∀{Γt Δt}{γv : V.Sub Δt Γt}{Θt}{δ : Subt Θt Δt}{t : Tm Θt} → ⌜ V.wk γv ⌝ ∘t (δ ,Σ t) ≡ ⌜ γv ⌝ ∘t δ
  ⌜wk⌝∘ {Γt = ◆t}                          = refl
  ⌜wk⌝∘ {Γt = Γt ▸t} {γv = γv ,Σ x}{δ = δ} = cong (_,Σ x [ δ ]v) (⌜wk⌝∘ {Γt = Γt}{γv = γv})

  idl : ∀{Γt Δt}{γ : Subt Δt Γt} → idt ∘t γ ≡ γ
  idl {◆t} = refl
  idl {Γt ▸t}{_}{γ ,Σ t} = cong (_,Σ t) (trans (⌜wk⌝∘ {γv = V.id}) (idl {Γt}))

  [⌜⌝] : ∀{Γt}{x : V.Tm Γt}{Δt}{γv : V.Sub Δt Γt} → x [ ⌜ γv ⌝ ]v ≡ var (x V.[ γv ])
  [⌜⌝] {x = vz}                 = refl
  [⌜⌝] {x = vs x}{γv = γv ,Σ y} = [⌜⌝] {x = x}{γv = γv}

  [id]v : ∀{Γt}{x : V.Tm Γt} → x [ idt ]v ≡ var x
  [id]v {x = vz} = refl
  [id]v {x = vs x} = trans (trans ([⌜⌝] {x = x}) (cong var (V.vs[] {t = x}{γ = V.id}))) (cong (λ z → var (vs z)) V.[id])

  [id]t  : ∀{Γt}{t : Tm Γt} → t [ idt ]t ≡ t
  [id]ts : ∀{Γt n}{ts : Tms Γt n} → ts [ idt ]ts ≡ ts
  [id]t {t = var x} = [id]v {x = x}
  [id]t {t = fun n a ts} = cong (fun n a) ([id]ts {ts = ts})
  [id]ts {n = zero}                = refl
  [id]ts {n = suc n}{ts = ts ,Σ t} = mk,= ([id]ts {ts = ts}) ([id]t {t = t})

  idr : ∀{Γt Δt}{γ : Subt Δt Γt} → γ ∘t idt ≡ γ
  idr {Γt = ◆t} = refl
  idr {Γt = Γt ▸t}{γ = γ ,Σ t} = mk,= (idr {γ = γ}) ([id]t {t = t})

  _,t_  : ∀{Γt Δt} → Subt Δt Γt → Tm Δt → Subt Δt (Γt ▸t)
  _,t_ = _,Σ_

  pt : ∀{Γt} → Subt (Γt ▸t) Γt
  pt {Γt} = ⌜ V.wk V.id ⌝

  qt : ∀{Γt} → Tm (Γt ▸t)
  qt = var V.vz

  ▸tβ₁ : ∀{Γt Δt}{γ : Subt Δt Γt}{t : Tm Δt} → (pt ∘t (γ ,t t)) ≡ γ
  ▸tβ₁ = trans ⌜wk⌝∘ idl

  ▸tη : ∀{Γt Δt}{γt : Subt Δt (Γt ▸t)} → ((pt ∘t γt) ,t (qt [ γt ]t)) ≡ γt
  ▸tη {γt = γ ,Σ t} = cong (_,Σ t) (trans (⌜wk⌝∘ {γv = V.id}) idl)

  data For (Γt : Cont) : Set where
    ⊥    : For Γt
    ⊤    : For Γt
    _⊃_  : For Γt → For Γt → For Γt
    _∧_  : For Γt → For Γt → For Γt
    ∀'   : For (Γt ▸t) → For Γt
    Eq   : Tm Γt → Tm Γt → For Γt
    rel  : (n : ℕ) → relar n → Tms Γt n → For Γt

  _[_]F : ∀{Γt Δt} → For Γt → Subt Δt Γt → For Δt
  ⊥ [ γ ]F = ⊥
  ⊤ [ γ ]F = ⊤
  (K ⊃ L) [ γ ]F = K [ γ ]F ⊃ L [ γ ]F
  (K ∧ L) [ γ ]F = K [ γ ]F ∧ L [ γ ]F
  ∀' K [ γ ]F = ∀' (K [ γ ∘t pt ,t qt ]F)
  Eq t t' [ γ ]F = Eq (t [ γ ]t) (t' [ γ ]t)
  rel n a ts [ γ ]F = rel n a (ts [ γ ]ts)

  [∘]F : ∀{Γt}{K : For Γt}{Δt}{γ : Subt Δt Γt}{Θt}{δ : Subt Θt Δt} → K [ γ ∘t δ ]F ≡ K [ γ ]F [ δ ]F
  [∘]F {K = ⊥} = refl
  [∘]F {K = ⊤} = refl
  [∘]F {K = K ⊃ L} = cong (λ z → proj₁ z ⊃ proj₂ z) (mk,= ([∘]F {K = K}) ([∘]F {K = L}))
  [∘]F {K = K ∧ L} = cong (λ z → proj₁ z ∧ proj₂ z) (mk,= ([∘]F {K = K}) ([∘]F {K = L}))
  [∘]F {K = ∀' K}{γ = γ}{δ = δ} = cong ∀' (trans (cong (K [_]F) (cong (_,Σ var vz) (trans (trans ass (cong (γ ∘t_) (sym (▸tβ₁ {γ = δ ∘t pt})))) (sym ass)))) ([∘]F {K = K}))
  [∘]F {K = Eq t t'} = cong (λ z → Eq (proj₁ z) (proj₂ z)) (mk,= ([∘]t {t = t}) ([∘]t {t = t'}))
  [∘]F {K = rel n a ts} = cong (rel n a) ([∘]ts {ts = ts})

  [id]F : ∀{Γt}{K : For Γt} → K [ idt ]F ≡ K
  [id]F {K = ⊥} = refl
  [id]F {K = ⊤} = refl
  [id]F {K = K ⊃ L} = cong (λ z → proj₁ z ⊃ proj₂ z) (mk,= ([id]F {K = K}) ([id]F {K = L}))
  [id]F {K = K ∧ L} = cong (λ z → proj₁ z ∧ proj₂ z) (mk,= ([id]F {K = K}) ([id]F {K = L}))
  [id]F {K = ∀' K} = cong ∀' (trans (cong (K [_]F) (cong (_,Σ var vz) idl)) ([id]F {K = K}))
  [id]F {K = Eq t t'} = cong (λ z → Eq (proj₁ z) (proj₂ z)) (mk,= ([id]t {t = t}) ([id]t {t = t'}))
  [id]F {K = rel n a ts} = cong (rel n a) ([id]ts {ts = ts})

  data Conp (Γt : Cont) : Set where
    ◆p   : Conp Γt
    _▸p_ : Conp Γt → For Γt → Conp Γt
    
  _[_]C : ∀{Γt} → Conp Γt → ∀{Δt} → Subt Δt Γt → Conp Δt
  ◆p [ γ ]C = ◆p
  (Γp ▸p K) [ γ ]C = Γp [ γ ]C ▸p K [ γ ]F

  [∘]C  : ∀{Γt}{Γp : Conp Γt}{Δt}{γ : Subt Δt Γt}{Θt}{δ : Subt Θt Δt} → Γp [ γ ∘t δ ]C ≡ Γp [ γ ]C [ δ ]C
  [∘]C {Γp = ◆p}      = refl
  [∘]C {Γp = Γp ▸p K} = cong (λ z → proj₁ z ▸p proj₂ z) (mk,= ([∘]C {Γp = Γp}) ([∘]F {K = K}))

  [id]C : ∀{Γt}{Γp : Conp Γt} → Γp [ idt ]C ≡ Γp
  [id]C {Γp = ◆p}      = refl
  [id]C {Γp = Γp ▸p K} = cong (λ z → proj₁ z ▸p proj₂ z) (mk,= ([id]C {Γp = Γp}) ([id]F {K = K}))

  module PV where

    data Pf {Γt : Cont} : Conp Γt → For Γt → Prop where
      vz : ∀{Γp K} → Pf (Γp ▸p K) K
      vs : ∀{Γp K} → Pf Γp K → ∀{L} → Pf (Γp ▸p L) K

    _[_]P : ∀{Γt}{Γp : Conp Γt}{K} → Pf Γp K → ∀{Δt} → (γ : Subt Δt Γt) → Pf (Γp [ γ ]C) (K [ γ ]F)
    vz [ γ ]P = vz
    vs k [ γ ]P = vs (k [ γ ]P)

    Subp : {Γt : Cont} → Conp Γt → Conp Γt → Prop
    Subp Γp' ◆p        = 𝟙p
    Subp Γp' (Γp ▸p K) = Subp Γp' Γp ×p Pf Γp' K

    _[_]s : ∀{Γt}{Γp Γp' : Conp Γt} → Subp Γp' Γp → ∀{Δt}(γ : Subt Δt Γt) → Subp (Γp' [ γ ]C) (Γp [ γ ]C)
    _[_]s {Γp = ◆p}      _         _ = *
    _[_]s {Γp = Γp ▸p K} (γp ,Σ k) γ = γp [ γ ]s ,Σ k [ γ ]P

    _[_]p : ∀{Γt}{Γp : Conp Γt}{K : For Γt} → Pf Γp K → ∀{Γp'} → Subp Γp' Γp → Pf Γp' K
    vz [ γp ,Σ k ]p = k
    vs px [ γp ,Σ k ]p = px [ γp ]p

    _∘p_  : ∀{Γt}{Γp Γp' : Conp Γt} → Subp Γp' Γp → ∀{Γp''} → Subp Γp'' Γp' → Subp Γp'' Γp
    _∘p_ {Γp = ◆p}      _         _   = *
    _∘p_ {Γp = Γp ▸p K} (γp ,Σ k) γp' = γp ∘p γp' ,Σ k [ γp' ]p

    wkp : ∀{Γt}{Γp Γp' : Conp Γt} → Subp Γp' Γp → ∀{K} → Subp (Γp' ▸p K) Γp
    wkp {Γp = ◆p}      _         = *
    wkp {Γp = Γp ▸p K} (γp ,Σ k) = wkp γp ,Σ vs k

    idp : ∀{Γt}{Γp : Conp Γt} → Subp Γp Γp
    idp {Γp = ◆p}      = *
    idp {Γp = Γp ▸p K} = wkp (idp {Γp = Γp}) ,Σ vz

    pp : ∀{Γt}{Γp : Conp Γt}{K} → Subp (Γp ▸p K) Γp
    pp = wkp idp

  open PV using (vz; vs)

  data Pf {Γt : Cont} : Conp Γt → For Γt → Prop where
    var  : ∀{Γp K} → PV.Pf Γp K → Pf Γp K
    exfalso : ∀{Γp : Conp Γt}{K} → Pf Γp ⊥ → Pf Γp K
    tt   : {Γp : Conp Γt} → Pf Γp ⊤
    lam  : ∀{K L}{Γp : Conp Γt} → Pf (Γp ▸p K) L → Pf Γp (K ⊃ L)
    _$_  : ∀{K L}{Γp : Conp Γt} → Pf Γp (K ⊃ L) → Pf Γp K → Pf Γp L
    pair : ∀{K L}{Γp : Conp Γt} → Pf Γp K → Pf Γp L → Pf Γp (K ∧ L)
    fst  : ∀{K L}{Γp : Conp Γt} → Pf Γp (K ∧ L) → Pf Γp K
    snd  : ∀{K L}{Γp : Conp Γt} → Pf Γp (K ∧ L) → Pf Γp L
    mk∀  : ∀{K Γp} → Pf {Γt ▸t} (Γp [ pt ]C) K → Pf {Γt} Γp (∀' K)
    un∀' : ∀{K Γp} → Pf Γp (∀' K) → (t : Tm Γt) → Pf Γp (K [ idt ,t t ]F)
    ref  : ∀{a Γp} → Pf Γp (Eq a a)
    subst' : ∀(K : For (Γt ▸t)){t t' : Tm Γt}{Γp} → Pf Γp (Eq t t') → Pf Γp (K [ idt ,t t ]F) → Pf Γp (K [ idt ,t t' ]F)

  -- TOOD: this should be a constructor because it is in Prop anyway, so it's not worth the effort
  _[_]P : ∀{Γ}{Γp : Conp Γ}{K} → Pf Γp K → ∀{Δt} → (γ : Subt Δt Γ) → Pf (Γp [ γ ]C) (K [ γ ]F)
  var px [ γ ]P = var (px PV.[ γ ]P)
  exfalso p [ γ ]P = exfalso (p [ γ ]P)
  tt [ γ ]P = tt
  lam k [ γ ]P = lam (k [ γ ]P)
  (l $ k) [ γ ]P = l [ γ ]P $ k [ γ ]P
  pair k l [ γ ]P = pair (k [ γ ]P) (l [ γ ]P)
  fst k [ γ ]P = fst (k [ γ ]P)
  snd k [ γ ]P = snd (k [ γ ]P)
  _[_]P {Γ}{Γp} (mk∀ {K} k) γ  = mk∀ (substp (λ Γp → Pf Γp (K [ γ ∘t pt ,t qt ]F))
    (trans (sym [∘]C) (trans (cong (Γp [_]C) ▸tβ₁) [∘]C))
    (k [ γ ∘t pt ,t qt ]P))
  un∀' {K}{Γp} k t [ γ ]P = substp (Pf (Γp [ γ ]C))
    (trans (sym [∘]F) (trans (cong (λ z → K [ z ,Σ t [ γ ]t ]F) (trans ass (trans (trans (cong (γ ∘t_) ▸tβ₁) idr) (sym idl)))) [∘]F))
    (un∀' (k [ γ ]P) (t [ γ ]t))
  ref [ γ ]P = ref
  subst' K {t}{t'}{Γp} e k [ γ ]P = substp (Pf (Γp [ γ ]C))
    (trans (sym [∘]F) (trans (cong (λ z → K [ z ,Σ t' [ γ ]t ]F) (trans ass (trans (trans (cong (γ ∘t_) ▸tβ₁) idr) (sym idl)))) [∘]F))
    (subst' (K [ γ ∘t pt ,Σ qt ]F) (e [ γ ]P) (substp (Pf (Γp [ γ ]C)) (trans (sym [∘]F) (trans (cong (λ z → K [ z ,Σ t [ γ ]t ]F) (trans idl (trans (trans (sym idr) (cong (γ ∘t_) (sym ▸tβ₁))) (sym ass)))) [∘]F)) (k [ γ ]P)))

  Subp : {Γ : Cont} → Conp Γ → Conp Γ → Prop
  Subp Γp' ◆p        = 𝟙p
  Subp Γp' (Γp ▸p K) = Subp Γp' Γp ×p Pf Γp' K

  _[_]s : ∀{Γ}{Γp Γp' : Conp Γ} → Subp Γp' Γp → ∀{Δt}(γ : Subt Δt Γ) → Subp (Γp' [ γ ]C) (Γp [ γ ]C)
  _[_]s {Γp = ◆p}      _         _ = *
  _[_]s {Γp = Γp ▸p K} (γp ,Σ k) γ = γp [ γ ]s ,Σ k [ γ ]P

  εp : ∀{Γ}{Γp : Conp Γ} → Subp Γp ◆p
  εp = *

  _,p_ : ∀{Γ}{Γp Γp' : Conp Γ} → Subp Γp' Γp → ∀{K} → Pf Γp' K → Subp Γp' (Γp ▸p K)
  γp ,p k = γp ,Σ k

  _[_]PV : ∀{Γ}{Γp : Conp Γ}{K : For Γ} → Pf Γp K → ∀{Γp'} → PV.Subp Γp' Γp → Pf Γp' K
  var x [ γp ]PV = var (x PV.[ γp ]p)
  exfalso p [ γp ]PV = exfalso (p [ γp ]PV)
  tt [ γp ]PV = tt
  lam k [ γp ]PV = lam (k [ PV.wkp γp ,Σ vz ]PV)
  (m $ k) [ γp ]PV = m [ γp ]PV $ k [ γp ]PV
  pair k l [ γp ]PV = pair (k [ γp ]PV) (l [ γp ]PV)
  fst kl [ γp ]PV = fst (kl [ γp ]PV)
  snd kl [ γp ]PV = snd (kl [ γp ]PV)
  mk∀ k [ γp ]PV = mk∀ (k [ γp PV.[ pt ]s ]PV)
  un∀' k t [ γp ]PV = un∀' (k [ γp ]PV) t
  ref [ γp ]PV = ref
  subst' K {t}{t'}{Γp} e k [ γp ]PV = subst' K (e [ γp ]PV) (k [ γp ]PV)

  _∘pv_  : ∀{Γ}{Γp Γp' : Conp Γ} → Subp Γp' Γp → ∀{Γp''} → PV.Subp Γp'' Γp' → Subp Γp'' Γp
  _∘pv_ {Γp = ◆p}      _         _   = *
  _∘pv_ {Γp = Γp ▸p K} (γp ,Σ k) γpv = γp ∘pv γpv ,Σ k [ γpv ]PV

  _[_]pv : ∀{Γ}{Γp : Conp Γ}{K : For Γ} → PV.Pf Γp K → ∀{Γp'} → Subp Γp' Γp → Pf Γp' K
  vz [ γp ,Σ k ]pv = k
  vs px [ γp ,Σ k ]pv = px [ γp ]pv

  ⌜_⌝P : ∀{Γ}{Γp Γp' : Conp Γ} → PV.Subp Γp' Γp → Subp Γp' Γp
  ⌜_⌝P {Γp = ◆p}      _         = *
  ⌜_⌝P {Γp = Γp ▸p K} (γp ,Σ x) = ⌜ γp ⌝P ,Σ var x

  _[_]p : ∀{Γ}{Γp : Conp Γ}{K : For Γ} → Pf Γp K → ∀{Γp'} → Subp Γp' Γp → Pf Γp' K
  var px [ γp ]p = px [ γp ]pv
  exfalso p [ γp ]p = exfalso (p [ γp ]p)
  tt [ γp ]p = tt
  lam k [ γp ]p = lam (k [ (γp ∘pv PV.pp) ,Σ var vz ]p)
  (l $ k) [ γp ]p = l [ γp ]p $ k [ γp ]p
  pair k l [ γp ]p = pair (k [ γp ]p) (l [ γp ]p)
  fst kl [ γp ]p = fst (kl [ γp ]p)
  snd kl [ γp ]p = snd (kl [ γp ]p)
  mk∀ k [ γp ]p = mk∀ (k [ γp [ pt ]s ]p)
  un∀' k t [ γp ]p = un∀' (k [ γp ]p) t
  ref [ γp ]p = ref
  subst' K {Γp = Γp} e k [ γp ]p = subst' K (e [ γp ]p) (k [ γp ]p)

  _∘p_  : ∀{Γ}{Γp Γp' : Conp Γ} → Subp Γp' Γp → ∀{Γp''} → Subp Γp'' Γp' → Subp Γp'' Γp
  _∘p_ {Γp = ◆p}      _         _   = *
  _∘p_ {Γp = Γp ▸p K} (γp ,Σ k) γp' = γp ∘p γp' ,Σ k [ γp' ]p

  pp : ∀{Γ}{Γp : Conp Γ}{K} → Subp (Γp ▸p K) Γp
  pp = ⌜ PV.pp ⌝P

  qp : ∀{Γ}{Γp : Conp Γ}{K} → Pf (Γp ▸p K) K
  qp = var vz

  idp : ∀{Γ}{Γp : Conp Γ} → Subp Γp Γp
  idp = ⌜ PV.idp ⌝P

  app : ∀{Γ K L}{Γp : Conp Γ} → Pf Γp (K ⊃ L) → Pf (Γp ▸p K) L
  app m = m [ PV.pp ]PV $ qp

  ids : ∀{n} → Tms (iteℕ ◆t _▸t n) n
  ids {zero} = *
  ids {suc n} = ids {n} [ pt ]ts ,Σ var vz

  un∀ : ∀{Γ K Γp} → Pf {Γ} Γp (∀' K) → Pf {Γ ▸t} (Γp [ pt ]C) K
  un∀ {K = K}{Γp} k = substp (Pf (Γp [ pt ]C))
    (trans (trans (sym [∘]F) (cong (λ z → K [ z ,Σ var vz ]F) (trans ass (trans (cong (pt ∘t_) ▸tβ₁) idr)))) [id]F)
    (un∀' (k [ pt ]P) (var vz))

  -- putting together the merged context
  
  Con : Set
  Con = Σ Cont Conp

  Sub : Con → Con → Set
  Sub (Δt ,Σ Δp) (Γt ,Σ Γp) = Σsp (Subt Δt Γt) λ γt → Subp Δp (Γp [ γt ]C)

  _∘_ : {Γ Δ : Con} → Sub Δ Γ → {Θ : Con} → Sub Θ Δ → Sub Θ Γ
  (γt ,Σ γp) ∘ (δt ,Σ δp) = (γt ∘t δt) ,Σ (substp (Subp _) (sym [∘]C) (γp [ δt ]s ∘p δp))

  id : {Γ : Con} → Sub Γ Γ
  id {Γt ,Σ Γp} = idt {Γt} ,Σ substp (Subp Γp) (sym [id]C) (idp {Γt}{Γp})

  ◆ : Con
  ◆ = ◆t ,Σ ◆p

  ε : {Γ : Con} → Sub Γ ◆
  ε {Γt ,Σ Γp} = εt {Γt} ,Σ εp {Γt}{Γp}

  -- For : Con → Set
  -- For Γ = ?

I : Model lzero lzero lzero lzero
I = record
  { Con = Con
  ; Sub = Sub
  ; _∘_ = _∘_
  ; ass = mk,sp= ass
  ; id = id
  ; idl = mk,sp= idl
  ; idr = mk,sp= idr
  ; ◆ = ◆
  ; ε = λ {Γ} → ε {Γ}
  ; ◆η = λ _ → refl
  ; For = {!!}
  ; _[_]F = {!!}
  ; [∘]F = {!!}
  ; [id]F = {!!}
  ; Pf = {!!}
  ; _[_]p = {!!}
  ; _▸p_ = {!!}
  ; _,p_ = {!!}
  ; pp = {!!}
  ; qp = {!!}
  ; ▸pβ₁ = {!!}
  ; ⊥ = {!!}
  ; ⊥[] = {!!}
  ; exfalso = {!!}
  ; ⊤ = {!!}
  ; ⊤[] = {!!}
  ; tt = {!!}
  ; _⊃_ = {!!}
  ; ⊃[] = {!!}
  ; lam = {!!}
  ; app = {!!}
  ; _∧_ = {!!}
  ; ∧[] = {!!}
  ; pair = {!!}
  ; fst = {!!}
  ; snd = {!!}
  ; Tm = {!!}
  ; _[_]t = {!!}
  ; [∘]t = {!!}
  ; [id]t = {!!}
  ; _▸t = {!!}
  ; _,t_ = {!!}
  ; pt = {!!}
  ; qt = {!!}
  ; ▸tβ₁ = {!!}
  ; ▸tβ₂ = {!!}
  ; ▸tη = {!!}
  ; Tms = {!!}
  ; _[_]ts = {!!}
  ; [∘]ts = {!!}
  ; [id]ts = {!!}
  ; εs = {!!}
  ; ◆sη = {!!}
  ; _,s_ = {!!}
  ; π₁ = {!!}
  ; π₂ = {!!}
  ; ▸sβ₁ = {!!}
  ; ▸sβ₂ = {!!}
  ; ▸sη = {!!}
  ; ,[] = {!!}
  ; fun = {!!}
  ; fun[] = {!!}
  ; rel = {!!}
  ; rel[] = {!!}
  ; ∀' = {!!}
  ; ∀[] = {!!}
  ; mk∀ = {!!}
  ; un∀ = {!!}
  ; Eq = {!!}
  ; Eq[] = {!!}
  ; ref = {!!}
  ; subst' = {!!}
  }
  where
    open I
{-
record
  { Con = Con
  ; Sub = Sub
  ; _∘_ = _∘_
  ; ass = ass
  ; id = id
  ; idl = idl
  ; idr = idr
  ; ◆ = ◆
  ; ε = λ {Γ} → ε {Γ}
  ; ◆η = λ _ → refl
  ; Tm = Tm
  ; _[_]t = _[_]t
  ; [∘]t = λ {Γ}{t}{Δ}{γ}{Θ}{δ} → [∘]t {Γ}{t}{Δ}{γ}{Θ}{δ}
  ; [id]t = [id]t
  ; _▸t = _▸t
  ; _,t_ = _,t_
  ; pt = pt
  ; qt = qt
  ; ▸tβ₁ = ▸tβ₁
  ; ▸tβ₂ = refl
  ; ▸tη = ▸tη
  ; Tms = Tms
  ; _[_]ts = _[_]ts
  ; [∘]ts = [∘]ts
  ; [id]ts = [id]ts
  ; εs = *
  ; ◆sη = λ _ → refl
  ; _,s_ = _,Σ_
  ; π₁ = proj₁
  ; π₂ = proj₂
  ; ▸sβ₁ = refl
  ; ▸sβ₂ = refl
  ; ▸sη = refl
  ; ,[] = refl
  ; For = For
  ; _[_]F = _[_]F
  ; [∘]F = [∘]F
  ; [id]F = [id]F
  ; Conp = Conp
  ; _[_]C = _[_]C
  ; [∘]C = [∘]C
  ; [id]C = [id]C
  ; Subp = Subp
  ; _[_]s = _[_]s
  ; _∘p_ = _∘p_
  ; idp = idp
  ; ◆p = ◆p
  ; ◆p[] = refl
  ; εp = λ {Γ}{Γp} → εp {Γ}{Γp}
  ; Pf = Pf
  ; _[_]P = _[_]P
  ; _[_]p = _[_]p
  ; _▸p_ = _▸p_
  ; ▸p[] = refl
  ; _,p_ = _,p_
  ; pp = pp
  ; qp = qp
  ; ⊥ = ⊥
  ; ⊥[] = refl
  ; exfalso = exfalso
  ; ⊤ = ⊤
  ; ⊤[] = refl
  ; tt = tt
  ; _⊃_ = _⊃_
  ; ⊃[] = refl
  ; lam = lam
  ; app = app
  ; _∧_ = _∧_
  ; ∧[] = refl
  ; pair = pair
  ; fst = fst
  ; snd = snd
  ; fun = fun
  ; fun[] = refl
  ; rel = rel
  ; rel[] = refl
  ; ∀' = ∀'
  ; ∀[] = refl
  ; mk∀ = mk∀
  ; un∀ = un∀
  ; Eq = Eq
  ; Eq[] = refl
  ; ref = ref
  ; subst' = subst'
  }
  where
    open I

-- interpretation into any model
module Ite {i j k l}(J : Model i j k l) where
  module J = Model J
  Con : I.Con → J.Con
  Con I.◆ = J.◆
  Con (Γ I.▸t) = Con Γ J.▸t

  Var : ∀{Γ} → I.V.Tm Γ → J.Tm (Con Γ)
  Var I.V.vz     = J.qt
  Var (I.V.vs x) = Var x J.[ J.pt ]t
  
  Tm : ∀{Γ} → I.Tm Γ → J.Tm (Con Γ)
  Tms : ∀{Γ n} → I.Tms Γ n → J.Tms (Con Γ) n
  Tm (I.var x) = Var x
  Tm (I.fun n a ts) = J.fun n a (Tms ts)
  Tms {n = zero}  _         = J.εs
  Tms {n = suc n} (ts ,Σ t) = Tms ts J.,s Tm t

  Sub : ∀{Γ Δ} → I.Sub Δ Γ → J.Sub (Con Δ) (Con Γ)
  Sub {I.◆}    _        = J.ε
  Sub {Γ I.▸t} (γ ,Σ t) = Sub γ J.,t Tm t

  _[_]v : ∀{Γ Δ}(x : I.V.Tm Γ)(γ : I.Sub Δ Γ) → Tm (x I.[ γ ]v) ≡ Var x J.[ Sub γ ]t
  I.V.vz   [ γ ]v = sym J.▸tβ₂
  I.V.vs x [ γ ,Σ t ]v = trans (x [ γ ]v) (trans (cong (λ z → Var x J.[ z ]t) (sym J.▸tβ₁)) J.[∘]t)

  _[_]t : ∀{Γ}(t : I.Tm Γ){Δ}(γ : I.Sub Δ Γ) → Tm (t I.[ γ ]t) ≡ Tm t J.[ Sub γ ]t
  _[_]ts : ∀{Γ n}(ts : I.Tms Γ n){Δ}(γ : I.Sub Δ Γ) → Tms (ts I.[ γ ]ts) ≡ Tms ts J.[ Sub γ ]ts
  I.var x [ γ ]t = x [ γ ]v
  I.fun n a ts [ γ ]t = trans (cong (J.fun n a) (ts [ γ ]ts)) (sym J.fun[])
  _[_]ts {Γ} {zero}  ts        γ = sym (J.◆sη (J.εs J.[ Sub γ ]ts))
  _[_]ts {Γ} {suc n} (ts ,Σ t) γ = trans (cong (λ z → proj₁ z J.,s proj₂ z) (mk,= (ts [ γ ]ts) (t [ γ ]t))) (sym J.,[])

  _∘_ : ∀{Γ Δ}(γ : I.Sub Δ Γ){Θ}(δ : I.Sub Θ Δ) → Sub (γ I.∘ δ) ≡ Sub γ J.∘ Sub δ
  _∘_ {Γ = I.◆}    γ        δ = sym (J.◆η (J.ε J.∘ Sub δ))
  _∘_ {Γ = Γ I.▸t} (γ ,Σ t) δ = trans (cong (λ z → proj₁ z J.,t proj₂ z) (mk,= (γ ∘ δ) (t [ δ ]t))) (sym J.,∘)

  Subv : ∀{Γ Δ} → I.V.Sub Δ Γ → J.Sub (Con Δ) (Con Γ)
  Subv {I.◆}    γ = J.ε
  Subv {Γ I.▸t} (γ ,Σ x) = Subv γ J.,t Tm (I.var x)

  wk : ∀{Γ Δ}{γv : I.V.Sub Δ Γ} → Sub I.⌜ I.V.wk γv ⌝ ≡ Sub I.⌜ γv ⌝ J.∘ J.pt
  wk {I.◆}   {Δ}{γv}      = sym (J.◆η (J.ε J.∘ J.pt))
  wk {Γ I.▸t}{Δ}{γv ,Σ x} = trans (cong (λ z → z J.,t Var x J.[ J.pt ]t) (wk {Γ}{Δ}{γv})) (sym (J.,∘ {γ = Sub I.⌜ γv ⌝}{t = Var x}{δ = J.pt}))

  id : ∀{Γ} → Sub (I.id {Γ}) ≡ J.id
  id {I.◆}    = sym (J.◆η J.id)
  id {Γ I.▸t} = trans (cong (J._,t J.qt) (trans (wk {Γ}{Γ}{I.V.id}) (trans (cong (J._∘ J.pt) (id {Γ})) J.idl))) (sym (J.▸tη' {Con Γ}))

  pt : ∀{Γ} → Sub (I.pt {Γ}) ≡ J.pt
  pt {Γ} = trans (wk {Γ}{Γ}{I.V.id}) (trans (cong (J._∘ J.pt) (id {Γ})) J.idl)

  For : ∀{Γ} → I.For Γ → J.For (Con Γ)
  For I.⊥ = J.⊥
  For I.⊤ = J.⊤
  For (K I.⊃ L) = For K J.⊃ For L
  For (K I.∧ L) = For K J.∧ For L
  For (I.∀' K) = J.∀' (For K)
  For (I.Eq t t') = J.Eq (Tm t) (Tm t')
  For (I.rel n a ts) = J.rel n a (Tms ts)

  _[_]F : ∀{Γ}(A : I.For Γ){Δ}(γ : I.Sub Δ Γ)→ For (A I.[ γ ]F) ≡ For A J.[ Sub γ ]F
  I.⊥ [ γ ]F = sym J.⊥[]
  I.⊤ [ γ ]F = sym J.⊤[]
  (K I.⊃ L) [ γ ]F = trans (cong (λ z → proj₁ z J.⊃ proj₂ z) (mk,= (K [ γ ]F) (L [ γ ]F))) (sym J.⊃[])
  (K I.∧ L) [ γ ]F = trans (cong (λ z → proj₁ z J.∧ proj₂ z) (mk,= (K [ γ ]F) (L [ γ ]F))) (sym J.∧[])
  _[_]F {Γ} (I.∀' K) {Δ} γ = trans (cong J.∀' (trans (K [ γ I.∘ I.pt I.,t I.qt ]F) (cong (λ z → For K J.[ z J.,t J.qt ]F) (trans (γ ∘ I.pt {Δ}) (cong (Sub γ J.∘_) (pt {Γ = Δ})))))) (sym J.∀[])
  I.Eq t t' [ γ ]F = trans (cong (λ z → J.Eq (proj₁ z) (proj₂ z)) (mk,= (t [ γ ]t) (t' [ γ ]t))) (sym J.Eq[])
  I.rel n a ts [ γ ]F = trans (cong (J.rel n a) (ts [ γ ]ts)) (sym J.rel[])

  Conp : ∀{Γ} → I.Conp Γ → J.Conp (Con Γ)
  Conp I.◆p        = J.◆p
  Conp (Γp I.▸p K) = Conp Γp J.▸p For K

  _[_]C : ∀{Γ}(Γp : I.Conp Γ){Δ}(γ : I.Sub Δ Γ) → Conp (Γp I.[ γ ]C) ≡ Conp Γp J.[ Sub γ ]C
  I.◆p        [ γ ]C = sym (J.◆p[] {γ = Sub γ})
  (Γp I.▸p K) [ γ ]C = trans (cong (λ z → proj₁ z J.▸p proj₂ z) (mk,= (Γp [ γ ]C) (K [ γ ]F))) (sym (J.▸p[] {Γp = Conp Γp}{K = For K}))

  PVar : ∀{Γ}{Γp : I.Conp Γ}{K : I.For Γ} → I.PV.Pf Γp K → J.Pf (Conp Γp) (For K)
  PVar I.PV.vz = J.qp
  PVar (I.PV.vs x) = PVar x J.[ J.pp ]p

  Pf : ∀{Γ}{Γp : I.Conp Γ}{K : I.For Γ} → I.Pf Γp K → J.Pf (Conp Γp) (For K)
  Pf (I.var x) = PVar x
  Pf (I.exfalso p) = J.exfalso (Pf p)
  Pf I.tt = J.tt
  Pf (I.lam k) = J.lam (Pf k)
  Pf (m I.$ k) = Pf m J.$ Pf k
  Pf (I.pair k l) = J.pair (Pf k) (Pf l)
  Pf (I.fst kl) = J.fst (Pf kl)
  Pf (I.snd kl) = J.snd (Pf kl)
  Pf {Γ} (I.mk∀ {K}{Γp} k) = J.mk∀ (substp (λ z → J.Pf z (For K)) (trans (Γp [ I.pt ]C) (cong (λ z → Conp Γp J.[ z ]C) (pt {Γ = Γ}))) (Pf k))
  Pf {Γ} (I.un∀' {K}{Γp} k t) = substp (J.Pf (Conp Γp)) (trans (cong (λ z → For K J.[ z J.,t Tm t ]F) (sym (id {Γ}))) (sym (K [ I.id I.,t t ]F))) (J.un∀' (Pf k) (Tm t))
  Pf I.ref = J.ref
  Pf {Γ} (I.subst' K {t}{t'}{Γp} e k) = substp (J.Pf (Conp Γp)) (trans (cong (λ z → For K J.[ z J.,t Tm t' ]F) (sym (id {Γ}))) (sym (K [ I.id I.,t t' ]F))) (J.subst' (For K) (Pf e) (substp (J.Pf (Conp Γp)) (trans (K [ I.id I.,t t ]F) (cong (λ z → For K J.[ z J.,t Tm t ]F) (id {Γ = Γ}))) (Pf k)))

  Subp : ∀{Γ}{Γp Γp' : I.Conp Γ} → I.Subp Γp' Γp → J.Subp (Conp Γp') (Conp Γp)
  Subp {Γ} {I.◆p}      _         = J.εp
  Subp {Γ} {Γp I.▸p K} (γp ,Σ k) = Subp γp J.,p Pf k

  ite : Morphism I J
  ite = record
    { Con = Con
    ; Sub = Sub
    ; _∘_ = _∘_
    ; id = λ {Γ} → id {Γ}
    ; ◆ = refl
    ; ε = refl
    ; Tm = Tm
    ; _[_]t = _[_]t
    ; _▸t = λ _ → refl
    ; _,t_ = λ _ _ → refl
    ; pt = λ {Γ} → pt {Γ}
    ; qt = refl
    ; Tms = Tms
    ; _[_]ts = _[_]ts
    ; εs = refl
    ; _,s_ = λ _ _ → refl 
    ; π₁ = λ _ → sym J.▸sβ₁
    ; π₂ = λ _ → sym J.▸sβ₂ 
    ; For = For
    ; _[_]F = _[_]F
    ; Conp = Conp
    ; _[_]C = _[_]C
    ; Subp = Subp
    ; ◆p = refl
    ; Pf = Pf
    ; _▸p_ = λ _ _ → refl
    ; ⊥ = refl
    ; ⊤ = refl
    ; _⊃_ = λ _ _ → refl
    ; _∧_ = λ _ _ → refl
    ; fun = λ _ _ _ → refl
    ; rel = λ _ _ _ → refl
    ; ∀' = λ _ → refl
    ; Eq = λ _ _ → refl
    }

-- standard model
module Tarski
  (D : Set)
  (fun : (n : ℕ) → funar n → D ^ n → D)
  (rel : (n : ℕ) → relar n → D ^ n → Prop)
  where
  St : Model _ _ _ _
  St = record
    { Con = Set
    ; Sub = λ Δ Γ → Δ → Γ
    ; _∘_ = λ γ δ θ* → γ (δ θ*)
    ; ass = refl
    ; id = λ γ* → γ*
    ; idl = refl
    ; idr = refl
    ; ◆ = 𝟙
    ; ε = λ _ → *
    ; ◆η = λ _ → refl
    ; Tm = λ Γ → Γ → D
    ; _[_]t = λ t γ δ* → t (γ δ*)
    ; [∘]t = refl
    ; [id]t = refl
    ; _▸t = λ Γ → Γ × D
    ; _,t_ = λ γ t δ* → (γ δ* ,Σ t δ*)
    ; pt = proj₁
    ; qt = proj₂
    ; ▸tβ₁ = refl
    ; ▸tβ₂ = refl
    ; ▸tη = refl
    ; Tms = λ Γ n → Γ → D ^ n
    ; _[_]ts = λ ts γ δ* → ts (γ δ*)
    ; [∘]ts = refl
    ; [id]ts = refl
    ; εs = λ _ → *
    ; ◆sη = λ _ → refl
    ; _,s_ = λ ts t γ* → ts γ* ,Σ t γ*
    ; π₁ = λ ts γ* → proj₁ (ts γ*)
    ; π₂ = λ ts γ* → proj₂ (ts γ*)
    ; ▸sβ₁ = refl
    ; ▸sβ₂ = refl
    ; ▸sη = refl
    ; ,[] = refl
    ; For = λ Γ → Γ → Prop
    ; _[_]F = λ K γ δ* → K (γ δ*)
    ; [∘]F = refl
    ; [id]F = refl
    ; Conp = λ Γ → Γ → Prop
    ; _[_]C = λ Γp γ δ* → Γp (γ δ*)
    ; [∘]C = refl
    ; [id]C = refl
    ; Subp = λ Γp' Γp → ∀{γ} → Γp' γ → Γp γ 
    ; _[_]s = λ γp γ {δ*} → γp {γ δ*}
    ; _∘p_ = λ γp γp' γp''* → γp (γp' γp''*)
    ; idp = λ γp* → γp*
    ; ◆p = λ _ → 𝟙p
    ; ◆p[] = refl
    ; εp = λ _ → *
    ; Pf = λ {Γ} Γp K → ∀{γ} → Γp γ → K γ
    ; _[_]P = λ k γ γp* → k γp*
    ; _[_]p = λ k γp γp* → k (γp γp*)
    ; _▸p_ = λ Γp K γ* → Γp γ* ×p K γ*
    ; ▸p[] = refl
    ; _,p_ = λ γp k γp* → γp γp* ,Σ k γp*
    ; pp = proj₁
    ; qp = proj₂
    ; ⊥ = λ _ → 𝟘p
    ; ⊥[] = refl
    ; exfalso = λ p γ* → ind𝟘p (p γ*)
    ; ⊤ = λ _ → 𝟙p
    ; ⊤[] = refl
    ; tt = λ _ → *
    ; _⊃_ = λ K L γ* → K γ* → L γ*
    ; ⊃[] = refl
    ; lam = λ l γp* k* → l (γp* ,Σ k*)
    ; app = λ m x* → m (proj₁ x*) (proj₂ x*)
    ; _∧_ = λ K L γ* → K γ* ×p L γ*
    ; ∧[] = refl
    ; pair = λ k l γp* → k γp* ,Σ l γp*
    ; fst = λ kl γp* → proj₁ (kl γp*)
    ; snd = λ kl γp* → proj₂ (kl γp*)
    ; fun = λ n a ts γ* → fun n a (ts γ*)
    ; fun[] = refl
    ; rel = λ n a ts γ* → rel n a (ts γ*)
    ; rel[] = refl
    ; ∀' = λ K γ* → (d : D) → K (γ* ,Σ d)
    ; ∀[] = refl
    ; mk∀ = λ k γp* d → k γp*
    ; un∀ = λ k {γd*} γp* → k γp* (proj₂ γd*)
    ; Eq = λ t t' γ* → t γ* ≡ t' γ*
    ; Eq[] = refl
    ; ref = λ _ → refl
    ; subst' = λ K e k γ* → substp K (cong (_ ,Σ_) (e γ*)) (k γ*)
    }

-- TODO: spell out the ite called on this model and show some examples

-- TODO: Kripke completeness proof

-- TODO: use metavariable names nicer than γp*, γp...
-}
