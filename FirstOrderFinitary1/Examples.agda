{-# OPTIONS --prop --rewriting #-}

open import lib

module FirstOrderFinitary1.Examples where

module TarskiSemantics
  (funar : ℕ → Set)
  (relar : ℕ → Set)
  where
  open import FirstOrderFinitary1 funar relar
  open I

  postulate
    P Q : For ◆
    
  Peirce : For ◆
  Peirce = ((P ⊃ Q) ⊃ P) ⊃ P

  postulate
    D   : Set
    funD : (n : ℕ) → funar n → D ^ n → D
    relD : (n : ℕ) → relar n → D ^ n → Prop

  module F = Morphism (Ite.ite (Tarski.St D funD relD))

  ⟦P⟧ ⟦Q⟧ ⟦Peirce⟧ : Prop
  ⟦P⟧      = F.For P *
  ⟦Q⟧      = F.For Q *
  ⟦Peirce⟧ = F.For Peirce *

  ⟦Peirce⟧= : ⟦Peirce⟧ ≡ (((⟦P⟧ → ⟦Q⟧) → ⟦P⟧) → ⟦P⟧)
  ⟦Peirce⟧= = refl

  postulate
    R : For (◆ ▸t)
    S : For (◆ ▸t ▸t)
    
  ⟦R⟧ : 𝟙 × D → Prop
  ⟦R⟧ = F.For R
  ⟦S⟧ : 𝟙 × D × D → Prop
  ⟦S⟧ = F.For S

  R' : For ◆
  R' = ∀' (R ⊃ R)

  Γ : Con
  Γ = ◆ ▸t ▸t
  ⟦Γ⟧ : Set
  ⟦Γ⟧ = F.Con Γ
  ⟦Γ⟧= : ⟦Γ⟧ ≡ (𝟙 × D × D)
  ⟦Γ⟧= = refl

  postulate
    p : Pf {Γ} ◆p S
  ⟦p⟧ : {γ : ⟦Γ⟧} → 𝟙p → ⟦S⟧ γ
  ⟦p⟧ = F.Pf p

  p' : Pf {◆} ◆p (∀' (∀' S))
  p' = mk∀ (mk∀ p)

  ⟦p'⟧ : 𝟙p → ∀(x y : D) → ⟦S⟧ ((* ,Σ x) ,Σ y)
  ⟦p'⟧ = F.Pf p'

module Peano where
  funar : ℕ → Set
  funar zero = {!!}
  funar (suc zero) = {!!}
  funar (suc (suc zero)) = {!!}
  funar (suc (suc (suc n))) = {!!}
