{-# OPTIONS --prop --type-in-type #-}

module GilbertHermant (PV : Set) where

-- needs impredicativity

open import lib

-- intuitionistic logic

record Model (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con  : Set i
    Pfs  : Con → Con → Prop j
    For  : Set i
    Pf   : Con → For → Prop j
    id   : ∀{Γ} → Pfs Γ Γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    ∙    : Con
    ε    : ∀{Γ} → Pfs Γ ∙
    _▷_  : Con → For → Con
    _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    q    : ∀{Γ A} → Pf  (Γ ▷ A) A

    pv   : PV → For

    ⊤    : For
    tt   : ∀{Γ} → Pf Γ ⊤

    _⇒_  : For → For → For
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

    ⊥    : For
    exfalso : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A

    _∨_  : For → For → For
    left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixr 6 _⇒_
  infixr 7 _∨_

-- syntax

module I where

  data For  : Set where
       pv   : PV → For
       ⊤    : For
       _⇒_  : For → For → For
       ⊥    : For
       _∨_  : For → For → For

  data Con  : Set where
       ∙    : Con
       _▷_  : Con → For → Con

  data Pfs  : Con → Con → Prop
  data Pf   : Con → For → Prop

  data Pfs    where
       id   : ∀{Γ} → Pfs Γ Γ
       _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
       p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
       ε    : ∀{Γ} → Pfs Γ ∙
       _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)

  data Pf     where
       _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
       q    : ∀{Γ A} → Pf  (Γ ▷ A) A
       tt   : ∀{Γ} → Pf Γ ⊤
       lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
       app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
       exfalso : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A
       left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
       right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
       case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

  I : Model _ _
  I = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = id
    ; _∘_ = _∘_ ; _[_] = _[_] ; ∙ = ∙ ; ε = ε ; _▷_ = _▷_ ; _,_ =
    _,_ ; p = p ; q = q ; pv = pv ; ⊤ = ⊤ ; tt = tt ; _⇒_ = _⇒_ ;
    lam = lam ; app = app ; ⊥ = ⊥ ; exfalso = exfalso ;_∨_ = _∨_ ;
    left = left ; right = right ; case = case }

  module I = Model I

module rec {i j}(M : Model i j) where

  module M = Model M

  ⟦_⟧F : I.For → M.For
  ⟦ I.pv v ⟧F = M.pv v
  ⟦ I.⊤ ⟧F = M.⊤
  ⟦ A I.⇒ B ⟧F = ⟦ A ⟧F M.⇒ ⟦ B ⟧F
  ⟦ I.⊥ ⟧F = M.⊥
  ⟦ A I.∨ B ⟧F = ⟦ A ⟧F M.∨ ⟦ B ⟧F

  ⟦_⟧C : I.Con → M.Con
  ⟦ I.∙ ⟧C = M.∙
  ⟦ Γ I.▷ A ⟧C = ⟦ Γ ⟧C M.▷ ⟦ A ⟧F

  ⟦_⟧s : ∀{Γ Δ} → I.Pfs Γ Δ  → M.Pfs ⟦ Γ ⟧C ⟦ Δ ⟧C
  ⟦_⟧P : ∀{Γ A} → I.Pf  Γ A  → M.Pf  ⟦ Γ ⟧C ⟦ A ⟧F

  ⟦ I.id ⟧s = M.id
  ⟦ σ I.∘ δ ⟧s = ⟦ σ ⟧s M.∘ ⟦ δ ⟧s
  ⟦ I.p ⟧s = M.p
  ⟦ I.ε ⟧s = M.ε
  ⟦ σ I., u ⟧s = ⟦ σ ⟧s M., ⟦ u ⟧P
  
  ⟦ u I.[ σ ] ⟧P = ⟦ u ⟧P M.[ ⟦ σ ⟧s ]
  ⟦ I.q ⟧P = M.q
  ⟦ I.tt ⟧P = M.tt
  ⟦ I.lam u ⟧P = M.lam ⟦ u ⟧P
  ⟦ I.app u ⟧P = M.app ⟦ u ⟧P
  ⟦ I.exfalso u ⟧P = M.exfalso ⟦ u ⟧P
  ⟦ I.left u ⟧P = M.left ⟦ u ⟧P
  ⟦ I.right u ⟧P = M.right ⟦ u ⟧P
  ⟦ I.case u v w ⟧P = M.case ⟦ u ⟧P ⟦ v ⟧P ⟦ w ⟧P

module _
  (W : Set)
  (_≤_ : W → W → Prop)
  (id≤ : {w : W} → w ≤ w)
  (_∘≤_ : {w w' w'' : W} → w' ≤ w → w'' ≤ w' → w'' ≤ w)
  (pv : PV → W → Prop)
   where

  -- lower bound
  _≤s_ : W → (W → Prop) → Prop
  w ≤s A = (w' : W) → A w' → w ≤ w'

  M : Model _ _
  M = record
    { Con   = W → Prop
    ; Pfs   = λ Γ Δ → ∀ w → w ≤s Γ → w ≤s Δ
    ; For   = W → Prop
    ; Pf    = λ Γ Δ → ∀ w → w ≤s Γ → w ≤s Δ
    ; id    = λ w w≤Γ → w≤Γ
    ; _∘_   = λ σ δ w w≤Γ → σ w (δ w w≤Γ)
    ; _[_]  = λ t σ w w≤Γ → t w (σ w w≤Γ)
    ; ∙     = λ _ → 𝟘p
    ; ε     = λ ΓI f ΔI ()
    ; _▷_   = λ Γ A w → Γ w +p A w
    ; _,_   = λ σ t w w≤Γ w' → ind+p (λ _ → w ≤ w') (σ w w≤Γ w') (t w w≤Γ w')
    ; p     = λ w w≤Γ+A w' Γ∋w' → w≤Γ+A w' (inj₁ Γ∋w')
    ; q     = λ w w≤Γ+A w' Γ∋w' → w≤Γ+A w' (inj₂ Γ∋w')
    ; pv    = pv
    ; ⊤     = λ _ → 𝟘p
    ; tt    = λ w w≤Γ w' → ind𝟘p
    ; _⇒_   = λ A B w → ∃ (W → Prop) λ C → C w ×p ∀ (D : W → Prop) → {!∀ w → (w ≤s A +p ) !} -- ∃ (I.Con → Prop) λ C → C ΓI ×p ((D : I.Con → Prop) → Pf (A ▷ D) B → Pf D C)
    ; lam   = λ u ΓI f ΔI → {!!}
    ; app   = {!!}
    ; ⊥     = λ ΓI → 𝟙p
    ; exfalso = λ t ΓI f ΔI A∋ΔI → t ΓI f ΔI *
    ; _∨_   = {!!} -- λ A B ΓI → ∃ (I.Con → Prop) λ C → Pf A C ×p Pf B C
    ; left  = λ {Γ}{A}{B} u ΓI f ΔI g → {!!}
    ; right = {!!}
    ; case  = {!!}
    }
