{-# OPTIONS --prop #-}

module Classical (PV : Set) where

open import lib

record Algebra (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con  : Set i
    Pfs  : Con → Con → Prop j
    For  : Set i
    Pf   : Con → For → Prop j
    id   : ∀{Γ} → Pfs Γ Γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    ∙    : Con
    ε    : ∀{Γ} → Pfs Γ ∙
    _▷_  : Con → For → Con
    _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    q    : ∀{Γ A} → Pf  (Γ ▷ A) A

    pv   : PV → For
    
    ⊤    : For
    tt   : ∀{Γ} → Pf Γ ⊤

    _⇒_  : For → For → For
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

    _∧_  : For → For → For
    pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B

    ⊥    : For
    exfalso : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A
    
    _∨_  : For → For → For
    left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

    lem : ∀{Γ A} → Pf Γ (A ∨ (A ⇒ ⊥))

    -- LT : ∀{Γ A B} → Pf Γ ((A ⇒ B) ∧ (B ⇒ A)) → A ≡ B

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixr 6 _⇒_
  infixr 8 _∧_
  infixr 7 _∨_

-- syntax

module I where

  data For  : Set where
       pv   : PV → For
       ⊤    : For
       _⇒_  : For → For → For
       _∧_  : For → For → For
       ⊥    : For
       _∨_  : For → For → For

  data Con  : Set where
       ∙    : Con
       _▷_  : Con → For → Con

  data Pfs  : Con → Con → Prop
  data Pf   : Con → For → Prop

  data Pfs    where
       id   : ∀{Γ} → Pfs Γ Γ
       _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
       p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
       ε    : ∀{Γ} → Pfs Γ ∙
       _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)

  data Pf     where
       _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
       q    : ∀{Γ A} → Pf  (Γ ▷ A) A
       tt   : ∀{Γ} → Pf Γ ⊤
       lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
       app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
       pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
       fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
       snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
       exfalso : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A
       left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
       right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
       case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C
       lem : ∀{Γ A} → Pf Γ (A ∨ (A ⇒ ⊥))

  I : Algebra _ _
  I = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = id ;
    _∘_ = _∘_ ; _[_] = _[_] ; ∙ = ∙ ; ε = ε ; _▷_ = _▷_ ; _,_ = _,_ ;
    p = p ; q = q ; pv = pv ; ⊤ = ⊤ ; tt = tt ; _⇒_ = _⇒_ ; lam = lam
    ; app = app ; _∧_ = _∧_ ; pair = pair ; fst = fst ; snd = snd ; ⊥
    = ⊥ ; exfalso = exfalso ; _∨_ = _∨_ ; left = left ; right = right
    ; case = case ; lem = lem }

  module I = Algebra I

module rec {i j}(M : Algebra i j) where

  module M = Algebra M

  ⟦_⟧F : I.For → M.For
  ⟦ I.pv v ⟧F = M.pv v
  ⟦ I.⊤ ⟧F = M.⊤
  ⟦ A I.⇒ B ⟧F = ⟦ A ⟧F M.⇒ ⟦ B ⟧F
  ⟦ A I.∧ B ⟧F = ⟦ A ⟧F M.∧ ⟦ B ⟧F
  ⟦ I.⊥ ⟧F = M.⊥
  ⟦ A I.∨ B ⟧F = ⟦ A ⟧F M.∨ ⟦ B ⟧F

  ⟦_⟧C : I.Con → M.Con
  ⟦ I.∙ ⟧C = M.∙
  ⟦ Γ I.▷ A ⟧C = ⟦ Γ ⟧C M.▷ ⟦ A ⟧F

  ⟦_⟧s : ∀{Γ Δ} → I.Pfs Γ Δ  → M.Pfs ⟦ Γ ⟧C ⟦ Δ ⟧C
  ⟦_⟧P : ∀{Γ A} → I.Pf  Γ A  → M.Pf  ⟦ Γ ⟧C ⟦ A ⟧F

  ⟦ I.id ⟧s = M.id
  ⟦ σ I.∘ δ ⟧s = ⟦ σ ⟧s M.∘ ⟦ δ ⟧s
  ⟦ I.p ⟧s = M.p
  ⟦ I.ε ⟧s = M.ε
  ⟦ σ I., u ⟧s = ⟦ σ ⟧s M., ⟦ u ⟧P
  
  ⟦ u I.[ σ ] ⟧P = ⟦ u ⟧P M.[ ⟦ σ ⟧s ]
  ⟦ I.q ⟧P = M.q
  ⟦ I.tt ⟧P = M.tt
  ⟦ I.lam u ⟧P = M.lam ⟦ u ⟧P
  ⟦ I.app u ⟧P = M.app ⟦ u ⟧P
  ⟦ I.pair u v ⟧P = M.pair ⟦ u ⟧P ⟦ v ⟧P
  ⟦ I.fst u ⟧P = M.fst ⟦ u ⟧P
  ⟦ I.snd u ⟧P = M.snd ⟦ u ⟧P
  ⟦ I.exfalso u ⟧P = M.exfalso ⟦ u ⟧P
  ⟦ I.left u ⟧P = M.left ⟦ u ⟧P
  ⟦ I.right u ⟧P = M.right ⟦ u ⟧P
  ⟦ I.case u v w ⟧P = M.case ⟦ u ⟧P ⟦ v ⟧P ⟦ w ⟧P
  ⟦ I.lem ⟧P = M.lem

postulate
  lem+p : ∀{A : Prop} → A +p (A → 𝟘p)

-- standard model

module _ (pv : PV → Prop) where

  st : Algebra _ _
  st = record
    { Con = Prop
    ; Pfs = λ Γ Δ → Γ → Δ
    ; For = Prop
    ; Pf = λ Γ A → Γ → A
    ; id = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = 𝟙p
    ; ε = λ γ → *
    ; _▷_ = λ Γ A → Γ ×p A
    ; _,_ = λ σ u γ → (σ γ ,Σ u γ)
    ; p = proj₁
    ; q = proj₂
    ; pv = pv
    ; ⊤ = 𝟙p
    ; tt = λ γ → *
    ; _⇒_ = λ A B → A → B
    ; lam = λ u γ α → u (γ ,Σ α)
    ; app = λ u γ → u (proj₁ γ) (proj₂ γ)
    ; _∧_ = λ A B → A ×p B
    ; pair = λ u v γ → u γ ,Σ v γ
    ; fst = λ u γ → proj₁ (u γ)
    ; snd = λ u γ → proj₂ (u γ)
    ; ⊥ = 𝟘p
    ; exfalso = λ f γ → ind𝟘p (f γ)
    ; _∨_ = λ A B → A +p B
    ; left = λ f γ → inj₁ (f γ)
    ; right = λ f γ → inj₂ (f γ)
    ; case = λ f g h γ → ind+p _ (λ α → f (γ ,Σ α)) (λ α → g (γ ,Σ α)) (h γ)
    ; lem = λ γ → lem+p
    }

  open rec st

  cons : I.Pf I.∙ I.⊥ → 𝟘p
  cons f = ⟦ f ⟧P *

  exfalso' : I.Pf I.∙ I.⊥ → ∀{A : I.For} → I.Pf I.∙ A
  exfalso' t = I.exfalso t

  cons' : I.Pf I.∙ (I.⊤ I.⇒ I.⊥) → 𝟘p
  cons' t = ⟦ I.app t ⟧P (* ,Σ *)

module Kripke
  (W : Set)
  (_≤_ : W → W → Prop)
  (id≤ : {w : W} → w ≤ w)
  (_∘≤_ : {w w' w'' : W} → w' ≤ w → w'' ≤ w' → w'' ≤ w)
  (∣_∣pv : PV → W → Prop)
  (_pv∶_⟨_⟩ : (v : PV) → ∀{w w'} → ∣ v ∣pv w → w' ≤ w → ∣ v ∣pv w')
  where
 
  record PSh : Set₁ where
    field
      ∣_∣    : W → Prop
      _∶_⟨_⟩ : ∀{w w'} → ∣_∣ w → w' ≤ w → ∣_∣ w'
  open PSh public
  
  K : Algebra _ _
  K = record
    { Con = PSh
    ; Pfs = λ Γ Δ → {w : W} → ∣ Γ ∣ w → ∣ Δ ∣ w
    ; For = PSh
    ; Pf  = λ Γ A → {w : W} → ∣ Γ ∣ w → ∣ A ∣ w
    ; id  = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = record { ∣_∣ = λ _ → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; ε = λ _ → *
    ; _▷_ = λ Γ A → record { ∣_∣ = λ w → ∣ Γ ∣ w ×p ∣ A ∣ w ; _∶_⟨_⟩ = λ { (γ ,Σ α) β → Γ ∶ γ ⟨ β ⟩ ,Σ A ∶ α ⟨ β ⟩ } }
    ; _,_ = λ σ u γ → σ γ ,Σ u γ
    ; p = proj₁
    ; q = proj₂
    ; pv = λ v → record { ∣_∣ = ∣ v ∣pv ; _∶_⟨_⟩ = v pv∶_⟨_⟩ }
    ; ⊤ = record { ∣_∣ = λ w → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; tt = λ _ → *
    ; _⇒_ = λ A B → record { ∣_∣ = λ w → ∀{w'} → w' ≤ w → ∣ A ∣ w' → ∣ B ∣ w' ; _∶_⟨_⟩ = λ {w w'} f β {w''} β' α → f (β ∘≤ β') α }
    ; lam = λ {Γ}{A} u γ β α → u (Γ ∶ γ ⟨ β ⟩ ,Σ α)
    ; app = λ { {Γ}{A}{B} u (γ ,Σ α) → u γ id≤ α }
    ; _∧_ = λ A B → record { ∣_∣ = λ w → ∣ A ∣ w ×p ∣ B ∣ w ; _∶_⟨_⟩ = λ { (α ,Σ α') β → A ∶ α ⟨ β ⟩ ,Σ B ∶ α' ⟨ β ⟩ } }
    ; pair = λ u v γ → u γ ,Σ v γ
    ; fst = λ u γ → proj₁ (u γ)
    ; snd = λ u γ → proj₂ (u γ)
    ; ⊥ = record { ∣_∣ = λ _ → 𝟘p ; _∶_⟨_⟩ = λ x _ → x }
    ; exfalso = λ t γ → ind𝟘p (t γ)
    ; _∨_ = λ A B → record { ∣_∣ = λ w → ∣ A ∣ w +p ∣ B ∣ w ; _∶_⟨_⟩ = λ α β → ind+p _ (λ x → inj₁ (A ∶ x ⟨ β ⟩)) (λ y → inj₂ (B ∶ y ⟨ β ⟩)) α }
    ; left = λ u γ → inj₁ (u γ)
    ; right = λ u γ → inj₂ (u γ)
    ; case = λ u u' v γ → ind+p _ (λ α → u (γ ,Σ α)) (λ α → u' (γ ,Σ α)) (v γ)
    ; lem = λ {Γ}{A}{w} γ → ind+p _ inj₁ {!!} (lem+p {∣ A ∣ w})
    }

  module K = Algebra K
