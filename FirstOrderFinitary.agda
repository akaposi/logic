{-# OPTIONS --prop --rewriting #-}

open import lib

module FirstOrderFinitary
  (funar : ℕ → Set) -- adott aritasu fuggvenyszimbolumbol hany van pl. Peano (0,1,suc,_+_,_*_): funar 0 = 2, funar 1 = 1, funar 2 = 2, funar n = 0 (n>2)
  (relar : ℕ → Set) -- adott aritasu relacioszimbolumbol hany van
  where

-- finitary presentation, one-sorted
postulate
  funExt : ∀{i j} {A : Set i} {B : A → Set j} {f g : (x : A) → B x} → (∀ x → f x ≡ g x) → f ≡ g
  funExtp : ∀{i j} {A : Prop i} {B : A → Set j} {f g : (x : A) → B x} → (∀ x → f x ≡ g x) → f ≡ g

-- metavaltozok: t : Tm Γ, a,b,c:Pf Γ A

record Algebra (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con   : Set i
    Sub   : Con → Con → Set j
    _∘_   : ∀{Γ Θ Δ} → Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
    ass   : ∀{Γ Θ Δ Φ}{p : Sub Δ Φ}{q : Sub Θ Δ}{r : Sub Γ Θ} → (p ∘ q) ∘ r ≡ p ∘ (q ∘ r)
    id    : ∀{Γ} → Sub Γ Γ
    idl   : ∀{Γ}{p : Sub Γ Γ} → id ∘ p ≡ p
    idr   : ∀{Γ}{p : Sub Γ Γ} → p ∘ id ≡ p

    ∙     : Con
    ε     : ∀{Γ} → Sub Γ ∙

    For   : Con → Set i
    _[_]F : ∀{Γ Δ} → For Δ → Sub Γ Δ → For Γ
    [∘]   : ∀{Γ Θ Δ}{A : For Δ}{σ : Sub Θ Δ}{δ : Sub Γ Θ} → A [ σ ∘ δ ]F ≡ A [ σ ]F [ δ ]F
    [id]  : ∀{Γ}{A : For Γ} → A [ id ]F ≡ A
    Pf    : (Γ : Con) → For Γ → Prop j
    _[_]  : ∀{Γ Δ A} → Pf Δ A → (σ : Sub Γ Δ) → Pf Γ (A [ σ ]F)
    _▷_   : (Γ : Con) → For Γ → Con
    _,_   : ∀{Γ Δ A} → (σ : Sub Γ Δ) → Pf Γ (A [ σ ]F) → Sub Γ (Δ ▷ A)
    p     : ∀{Γ A} → Sub (Γ ▷ A) Γ
    q     : ∀{Γ A} → Pf  (Γ ▷ A) (A [ p ]F)
    ▷β₁   : ∀{Γ Δ A}{σ : Sub Γ Δ}{a : Pf Γ (A [ σ ]F)} → (p ∘ (σ , a)) ≡ σ
    ▷η    : ∀{Γ Δ A}{σ : Sub Γ (Δ ▷ A)} → ((p ∘ σ) , substp (Pf Γ) (sym [∘]) (q [ σ ])) ≡ σ

    ⊤    : For ∙
    tt   : Pf ∙ ⊤

    _⇒_  : ∀{Γ} → For Γ → For Γ → For Γ
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) (B [ p ]F) → Pf Γ (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) (B [ p ]F)
    ⇒[]  : ∀{Γ Δ A B}{σ : Sub Γ Δ} → (A ⇒ B) [ σ ]F ≡ A [ σ ]F ⇒ B [ σ ]F

    _∧_  : ∀{Γ} → For Γ → For Γ → For Γ
    pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
    ∧[]  : ∀{Γ Δ A B}{σ : Sub Γ Δ} → (A ∧ B) [ σ ]F ≡ A [ σ ]F ∧ B [ σ ]F

    Tm    : Con → Set j
    _[_]t : ∀{Γ Δ} → Tm Δ → Sub Γ Δ → Tm Γ
    [∘]t  : ∀{Γ Δ Θ}{t : Tm Γ}{γ : Sub Δ Γ}{δ : Sub Θ Δ} → t [ γ ∘ δ ]t ≡ t [ γ ]t [ δ ]t
    [id]t : ∀{Γ}{t : Tm Γ} → t [ id ]t ≡ t
    _▷t   : Con → Con
    _,t_  : ∀{Γ Δ} → Sub Γ Δ → Tm Γ → Sub Γ (Δ ▷t)
    pt    : ∀{Γ} → Sub (Γ ▷t) Γ
    qt    : ∀{Γ} → Tm (Γ ▷t)
    ▷tβ₁  : ∀{Γ Δ}{σ : Sub Γ Δ}{t : Tm Γ} → (pt ∘ (σ ,t t)) ≡ σ
    ▷tβ₂  : ∀{Γ Δ}{σ : Sub Γ Δ}{t : Tm Γ} → (qt [ σ ,t t ]t) ≡ t
    ▷tη   : ∀{Γ Δ}{σ : Sub Γ (Δ ▷t)} → ((pt ∘ σ) ,t (qt [ σ ]t)) ≡ σ

    fun  : (n : ℕ) → funar n → Tm  (rec ∙ _▷t n)
    rel  : (n : ℕ) → relar n → For (rec ∙ _▷t n)
    
    ∀'   : ∀{Γ} → For (Γ ▷t) → For Γ
    ∀[]  : ∀{Γ Δ A}{σ : Sub Γ Δ} → (∀' A) [ σ ]F ≡ ∀' (A [ (σ ∘ pt) ,t qt ]F)
    mk∀  : ∀{Γ A} → Pf (Γ ▷t) A → Pf Γ (∀' A)
    un∀  : ∀{Γ A} → Pf Γ (∀' A) → Pf (Γ ▷t) A

    Eq   : ∀{Γ} → Tm Γ → Tm Γ → For Γ
    ref  : ∀{Γ}{a : Tm Γ} → Pf Γ (Eq a a)
    reflect : ∀{Γ}{a b : Tm Γ} → Pf Γ (Eq a b) → a ≡ b
    Eq[] : ∀{Δ Γ}{γ : Sub Δ Γ}{a b : Tm Γ} → (Eq a b) [ γ ]F ≡ Eq (a [ γ ]t) (b [ γ ]t)

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixl 8 _[_]F
  infixr 6 _⇒_
  infixr 7 _∧_

module I where
  mutual
    data Con : Set where
      ∙ : Con
      _▷_ : (Γ : Con) → For Γ → Con

    data For : Con → Set where
      ⊤ : {Γ : Con} → For Γ


module Standard
  -- matematikai struktura
  (D : Set)
  (d : D) -- D nemures
  (fun  : (n : ℕ) → funar n → D ^ n → D)
  (rel  : (n : ℕ) → relar n → D ^ n → Prop)
  where

  St : Algebra _ _
  St = record
    { Con = Set
    ; Sub = λ Γ Δ → Γ → Δ
    ; _∘_ = λ f g x → f (g x)
    ; ass = refl
    ; id = λ x → x
    ; idl = refl
    ; idr = refl

    ; ∙ = 𝟙
    ; ε = λ _ → *
    
    ; For = λ Γ → Γ → Prop
    ; _[_]F = λ A σ γ → A (σ γ)
    ; [∘] = refl
    ; [id] = refl
    ; Pf = λ Γ A → (γ : Γ) → A γ
    ; _[_] = λ z σ γ → z (σ γ)
    ; _▷_ = λ Γ A → Σsp Γ A
    ; _,_ = λ σ f y → σ y ,Σ f y
    ; p = proj₁
    ; q = proj₂
    ; ▷β₁ = refl
    ; ▷η = refl
    ; Tm = λ Γ → Γ → D
    ; _[_]t = λ f g x → f (g x)
    ; _▷t = _× D
    ; _,t_ = λ f g y → f y ,Σ g y
    ; [∘]t = refl
    ; [id]t = refl
    ; pt = proj₁
    ; qt = proj₂
    ; ▷tβ₁ = refl
    ; ▷tβ₂ = refl
    ; ▷tη = refl
    ; fun = fun
    ; rel = rel
    ; ⊤ = λ _ → 𝟙p
    ; tt = λ _ → *
    ; _⇒_ = λ f g x → f x → g x
    ; lam = λ f γ α → f (γ ,Σ α)
    ; app = λ f γ → f (proj₁ γ) (proj₂ γ)
    ; ⇒[] = refl
    ; _∧_ = λ f g x → f x ×p g x
    ; pair = λ f g γ → f γ ,Σ g γ
    ; fst = λ f γ → proj₁ (f γ)
    ; snd = λ f γ → proj₂ (f γ)
    ; ∧[] = refl
    ; ∀' = λ A γ → (x : D) → A (γ ,Σ x)
    ; ∀[] = refl
    ; mk∀ = λ f γ d₁ → f (γ ,Σ d₁)
    ; un∀ = λ f γ → f (proj₁ γ) (proj₂ γ)
    ; Eq = λ a b γ → a γ ≡ b γ
    ; ref = λ _ → refl
    ; reflect = funExt -- funext kell
    ; Eq[] = refl
    }
-- TODO: Kell az iniciális modell: I.Algebra
-- ez valszeg tul egyszeru
module Kripke
  (D : Set)
  (fun  : (n : ℕ) → funar n → D ^ n → D)
  (W : Set)
  (_≥_ : W → W → Prop)
  (id≥ : {w : W} → w ≥ w)
  (_∘≥_ : {w w' w'' : W} → w' ≥ w → w'' ≥ w' → w'' ≥ w)
  (rel  : (n : ℕ) → relar n → D ^ n → W → Prop)
  (⟨rel⟩ : ∀{n i ds w w'} → rel n i ds w → w' ≥ w → rel n i ds w')
  where
    record PSh : Set₁ where
      constructor mk
      field
        ∣Γ∣    : W → Set
        Γ∶_⟨_⟩ : ∀{w w'} → ∣Γ∣ w → w' ≥ w → ∣Γ∣ w'
        ⟨id⟩   : ∀{w}{γ* : ∣Γ∣ w} → Γ∶ γ* ⟨ id≥ ⟩ ≡ γ*
        ⟨∘⟩    : ∀{w}{γ* : ∣Γ∣ w}{w'}{f : w' ≥ w}{w''}{g : w'' ≥ w'} → Γ∶ γ* ⟨ f ∘≥ g ⟩ ≡ Γ∶ Γ∶ γ* ⟨ f ⟩ ⟨ g ⟩
    open PSh public renaming (∣Γ∣ to ∣_∣ ; Γ∶_⟨_⟩ to _∶_⟨_⟩)

    record PShMor (Γ Δ : PSh) : Set where
      constructor mk
      field
        ∣δ∣ : ∀ w → ∣ Γ ∣ w → ∣ Δ ∣ w
        ⟨δ⟩ : ∀{w}{γ* : ∣ Γ ∣ w}{w'}{f : w' ≥ w} → Δ ∶ ∣δ∣ w γ* ⟨ f ⟩ ≡ ∣δ∣ w' (Γ ∶ γ* ⟨ f ⟩)
        {-
                 δ w
           ∣Γ∣w ----->∣Δ∣w
            |          |
      Γ∶_⟨f⟩|          |Δ∶_⟨f⟩
            v    δ w'  v
           ∣Γ∣w'----->∣Δ∣w'
        -}
    open PShMor renaming (∣δ∣ to ∣_∣ ; ⟨δ⟩ to ⟨_⟩)

    mkPShMor= : ∀{Γ Δ}{∣δ∣₀ ∣δ∣₁ : ∀ w → ∣ Γ ∣ w → ∣ Δ ∣ w}(∣δ∣₂ : ∣δ∣₀ ≡ ∣δ∣₁)
      {⟨δ⟩₀ : ∀{w}{γ* : ∣ Γ ∣ w}{w'}{f : w' ≥ w} → Δ ∶ ∣δ∣₀ w γ* ⟨ f ⟩ ≡ ∣δ∣₀ w' (Γ ∶ γ* ⟨ f ⟩)}
      {⟨δ⟩₁ : ∀{w}{γ* : ∣ Γ ∣ w}{w'}{f : w' ≥ w} → Δ ∶ ∣δ∣₁ w γ* ⟨ f ⟩ ≡ ∣δ∣₁ w' (Γ ∶ γ* ⟨ f ⟩)} →
      _≡_ {A = PShMor Γ Δ} (mk ∣δ∣₀ ⟨δ⟩₀) (mk ∣δ∣₁ ⟨δ⟩₁)
    mkPShMor= refl = refl

    record FamPSh (Γ : PSh) : Set₁ where
      constructor mk
      field
        ∣A∣    : (w : W) → ∣ Γ ∣ w → Prop
        A∶_⟨_⟩ : ∀{w γ*} → ∣A∣ w γ* → ∀{w'}(f : w' ≥ w) → ∣A∣ w' (Γ ∶ γ* ⟨ f ⟩)
    open FamPSh public renaming (∣A∣ to ∣_∣ ; A∶_⟨_⟩ to _∶_⟨_⟩)

    mkFamPSh= : ∀{Γ}{∣A∣₀ ∣A∣₁ : (w : W) → ∣ Γ ∣ w → Prop}(∣A∣₂ : ∣A∣₀ ≡ ∣A∣₁)
      {⟨A⟩₀ : ∀{w γ*} → ∣A∣₀ w γ* → ∀{w'}(f : w' ≥ w) → ∣A∣₀ w' (Γ ∶ γ* ⟨ f ⟩)}
      {⟨A⟩₁ : ∀{w γ*} → ∣A∣₁ w γ* → ∀{w'}(f : w' ≥ w) → ∣A∣₁ w' (Γ ∶ γ* ⟨ f ⟩)} →
      _≡_ {A = FamPSh Γ} (mk ∣A∣₀ ⟨A⟩₀) (mk ∣A∣₁ ⟨A⟩₁)
    mkFamPSh= refl = refl

    record Lift {i}(A : Prop i) : Set i where
      constructor mk
      field un : A
    open Lift public

    ∙ : PSh
    ∙ = mk (λ _ → 𝟙) _ refl refl

    _▷t : PSh → PSh
    _▷t Γ = mk (λ w → ∣ Γ ∣ w × D) (λ {w} {w'} x f -> (Γ ∶ proj₁ x ⟨ f ⟩) ,Σ (proj₂ x)) (mk,= (⟨id⟩ Γ) refl) (mk,= (⟨∘⟩ Γ) refl)

    ∣rec∣ : ∀{w} → (n : ℕ) → ∣ rec ∙ _▷t n ∣ w → D ^ n
    ∣rec∣ zero    _           = *
    ∣rec∣ (suc n) (ts* ,Σ t*) = ∣rec∣ n ts* ,Σ t*

    ∣rec∣worldIndependent : ∀{w}{w'} 
                         → (n : ℕ) → (f : w' ≥ w) → (ts* : ∣ rec ∙ _▷t n ∣ w) 
                         → ∣rec∣ {w} n ts* ≡ ∣rec∣ {w'} n (rec ∙ _▷t n ∶ ts* ⟨ f ⟩)
    ∣rec∣worldIndependent {w} {w'} zero f ts* = refl
    ∣rec∣worldIndependent {w} {w'} (suc n) f (ts ,Σ t) = mk,= (∣rec∣worldIndependent n f ts) refl
     
    m : Algebra _ _
    m = record
      { Con     = PSh
      ; Sub     = PShMor
      ; _∘_     = λ γ δ → mk (λ w γ* → ∣ γ ∣ w (∣ δ ∣ w γ*)) λ {w}{γ*}{w'}{f} → trans ⟨ γ ⟩ (cong (∣ γ ∣ w') ⟨ δ ⟩)
      ; ass     = refl
      ; id      = mk (λ w γ* → γ*) refl
      ; idl     = refl
      ; idr     = refl
      ; ∙       = ∙
      ; ε       = mk _ refl
      ; For     = FamPSh
      ; _[_]F   = λ A δ → mk (λ w γ* → ∣ A ∣ w (∣ δ ∣ w γ*)) λ {w}{γ*} a* {w'} f → substp (∣ A ∣ w') ⟨ δ ⟩ (A ∶ a* ⟨ f ⟩)
      ; [∘]     = refl 
      ; [id]    = refl
      ; Pf      = λ Γ A → ∀ w → (γ* : ∣ Γ ∣ w) → ∣ A ∣ w γ*
      ; _[_]    = λ {Δ}{Γ}{A} a γ w δ* → a w (∣ γ ∣ w δ*)
      ; _▷_     = λ Γ A → mk (λ w → Σsp (∣ Γ ∣ w) (∣ A ∣ w)) (λ { {w}{w'}(γ* ,Σ a*) f → Γ ∶ γ* ⟨ f ⟩ ,Σ A ∶ a* ⟨ f ⟩ }) (mk,sp= (⟨id⟩ Γ)) (mk,sp= (⟨∘⟩ Γ))
      ; _,_     = λ {Δ}{Γ}{A} γ a → mk (λ w δ* → ∣ γ ∣ w δ* ,Σ a w δ*) λ {w} {γ*} {w'} {f} -> mk,sp= ⟨ γ ⟩
      ; p       = mk (λ w → proj₁) refl
      ; q       = λ w → proj₂
      ; ▷β₁     = refl
      ; ▷η      = refl
      ; ⊤       = mk (λ w _ → w ≥ w) λ _ f → id≥
      ; tt      = λ w γ* → id≥
      ; _⇒_     = λ {Γ} A B → mk (λ w γ* → ∀ w' (f : w' ≥ w) → ∣ A ∣ w' (Γ ∶ γ* ⟨ f ⟩) → ∣ B ∣ w' (Γ ∶ γ* ⟨ f ⟩)) λ {w} {γ*} -> λ x f w' g a* → substp (λ y -> y) (cong ( ∣ B ∣ w') (⟨∘⟩ Γ)) (x w' (f ∘≥ g) (substp (λ y -> y) (sym (cong (∣ A ∣ w') (⟨∘⟩ Γ))) a*))
      ; lam     = λ {Γ} {A} {B} g w γ* w' f x → g w' ((Γ ∶ γ* ⟨ f ⟩) ,Σ x)
      ; app     = λ {Γ} {A} {B} → λ f w γ* → substp (∣ B ∣ w) (⟨id⟩ Γ) (f w (proj₁ γ*) w id≥ (substp (∣ A ∣ w) (sym (⟨id⟩ Γ)) (proj₂ γ*)))
-- ∀{w γ*} → ∣A∣₀ w γ* → ∀{w'}(f : w' ≥ w) → ∣A∣₀ w' (Γ ∶ γ* ⟨ f ⟩)}
-- ∀{w γ*} → ∣A∣₁ w γ* → ∀{w'}(f : w' ≥ w) → ∣A∣₁ w' (Γ ∶ γ* ⟨ f ⟩)}
      ; ⇒[]     = λ {Γ} {Δ} {A} {B} {σ} → mkFamPSh= (funExt λ w → funExt λ γ* → cong (λ F → (w' : W)(f : w' ≥ w) → F w' f) (funExt λ w' → funExtp λ f → cong (λ x → ∣ A ∣ w' x → ∣ B ∣ w' x) ⟨ σ ⟩)) {(λ {w} {γ*} a* {w'} f → substp (λ γ*₁ → (w'' : W) (f₁ : w'' ≥ w') → ∣ A ∣ w'' (Δ ∶ γ*₁ ⟨ f₁ ⟩) → ∣ B ∣ w'' (Δ ∶ γ*₁ ⟨ f₁ ⟩)) ⟨ σ ⟩ (λ w'' g a*₁ → substp (λ y → y) (cong (∣ B ∣ w'') (⟨∘⟩ Δ)) (a* w'' (f ∘≥ g) (substp (λ y → y) (sym (cong (∣ A ∣ w'') (⟨∘⟩ Δ))) a*₁))))}{(λ {w} {γ*} x f w' g a* → substp (λ y → y) (cong (λ γ*₁ → ∣ B ∣ w' (∣ σ ∣ w' γ*₁)) (⟨∘⟩ Γ)) (x w' (f ∘≥ g) (substp (λ y → y) (sym (cong (λ γ*₁ → ∣ A ∣ w' (∣ σ ∣ w' γ*₁)) (⟨∘⟩ Γ {w} {γ*} {_} {f} {w'} {g}))) a*)))}
      
      ; _∧_     = λ {Γ} A B → mk (λ w γ* → ∣ A ∣ w γ* ×p ∣ B ∣ w γ*) λ {w} {γ*} -> λ { (a* ,Σ b*) {w'} f → (A ∶ a* ⟨ f ⟩) ,Σ (B ∶ b* ⟨ f ⟩) } 
      ; pair    = λ {Γ} {A} {B} -> λ f g w γ* → (f w γ*) ,Σ (g w γ*)
      ; fst     = λ {Γ} {A} {B} x w γ* -> proj₁ (x w γ*)
      ; snd     = λ {Γ} {A} {B} x w γ* -> proj₂ (x w γ*)
      ; ∧[]     = refl
      ; Tm      = λ Γ → PShMor Γ (mk (λ _ → D) (λ d _ → d) refl refl)
      ; _[_]t   = λ a δ → mk (λ w γ* → ∣ a ∣ w (∣ δ ∣ w γ*)) (trans ⟨ a ⟩ (cong (∣ a ∣ _) ⟨ δ ⟩))
      ; [∘]t    = refl   
      ; [id]t   = refl 
      ; _▷t     = _▷t
      ; _,t_    = λ {Γ} {Δ} γ t -> mk (λ w γ* -> (∣ γ ∣ w γ*) ,Σ ∣ t ∣ w γ*) (mk,= ⟨ γ ⟩ ⟨ t ⟩)
      ; pt      = mk (λ _ → proj₁) refl
      ; qt      = mk (λ _ → proj₂) refl
      ; ▷tβ₁    = refl
      ; ▷tβ₂    = refl
      ; ▷tη     = refl
{-
    fun  : (n : ℕ) → funar n → Tm  (rec ∙ _▷t n)
    rel  : (n : ℕ) → relar n → For (rec ∙ _▷t n)
  (fun  : (n : ℕ) → funar n → D ^ n → D)
  (W : Set)
  (_≥_ : W → W → Prop)
  (id≥ : {w : W} → w ≥ w)
  (_∘≥_ : {w w' w'' : W} → w' ≥ w → w'' ≥ w' → w'' ≥ w)
  (rel  : (n : ℕ) → relar n → D ^ n → W → Prop)
  (⟨rel⟩ : ∀{n i ds w w'} → rel n i ds w → w' ≥ w → rel n i ds w')

-}
      ; fun     = λ n i' → mk (λ w ts* → fun n i' (∣rec∣ n ts*)) λ {w}{ts*}{w'}{f} → cong (fun n i') (∣rec∣worldIndependent n f ts*)
      ; rel     = λ n i → mk (λ w ts* → rel n i (∣rec∣ n ts*) w) λ {w} {γ*} x {w'} f -> substp (λ y -> rel n i y w')  (∣rec∣worldIndependent n f γ*) (⟨rel⟩ x f)
      ; ∀'      = λ {Γ} A → mk (λ w γ* → (d : D) → ∣ A ∣ w (γ* ,Σ d)) (λ {w}{γ*} a* {w'} f d → A ∶ a* d ⟨ f ⟩)
      ; ∀[]     = refl
      ; mk∀     = λ a w γ* t* → a w (γ* ,Σ t*)
      ; un∀     = λ a w γt* → a w (proj₁ γt*) (proj₂ γt*)
      ; Eq      = λ a a' → mk (λ w γ* → ∣ a ∣ w γ* ≡ ∣ a' ∣ w γ*) (λ e f → trans (sym ⟨ a ⟩) (trans e ⟨ a' ⟩))
      ; ref     = λ w γ* → refl
      ; reflect = λ {_}{a}{b} e → mkPShMor= (funExt λ w → funExt λ γ* → e w γ*) {⟨ a ⟩}{⟨ b ⟩}
      ; Eq[]    = refl
      }
{-
module Completeness
  open Kripke 

  where
-}
{-
module Kripke'
  (W : Set)
  (_≥_ : W → W → Prop)
  (id≥ : {w : W} → w ≥ w)
  (_∘≥_ : {w w' w'' : W} → w' ≥ w → w'' ≥ w' → w'' ≥ w)

  -- ezekre a Tm-ek ertelmezese miatt van szukseg:
  (D : W → Set)
  (_[_]D : ∀{w w'} → (w' ≥ w) → D w → D w')
  (d : (w : W) → D w) -- D w nemures
  (fun  : (n : ℕ) → funar n → (w : W) → D w ^ n → D w)
  -- (⟨fun⟩ : fun n i w xs [ f ]D ≡ fun n i w (pontonkent xs [ f ]D))
  (rel  : (n : ℕ) → relar n → (w : W) → D w ^ n → Prop)
  (⟨rel⟩ : ∀{n i w w'}{ds : D w ^ n} → rel n i w ds → (f : w' ≥ w) → rel n i w' {!pontonkent ds [ f ]D!}) -- kb
  where
-}