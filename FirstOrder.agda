{-# OPTIONS --prop #-}

open import lib

module FirstOrder
  (Ter : Set)     -- set of terms
  (Rel : ℕ → Set) -- Rel n is the set of relations of arity n
  where

-- intuitionistic logic

record Algebra (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con  : Set i
    Pfs  : Con → Con → Prop j
    For  : Set i
    Pf   : Con → For → Prop j
    id   : ∀{Γ} → Pfs Γ Γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    ∙    : Con
    ε    : ∀{Γ} → Pfs Γ ∙
    _▷_  : Con → For → Con
    _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    q    : ∀{Γ A} → Pf  (Γ ▷ A) A

    rel  : ∀{n} → Rel n → Ter ^ n → For

    ⊤    : For
    tt   : ∀{Γ} → Pf Γ ⊤

    _⇒_  : For → For → For
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)     -- (Pf A → Pf B) → Pf (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

    _∧_  : For → For → For
    pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B

    -- ∀': For × For × For × ... → For
    --     \___________________/
    --        Ter mennyisegu
    ∀'   : (Ter → For) → For
    mk∀  : ∀{Γ A} → ((t : Ter) → Pf Γ (A t)) → Pf Γ (∀' A)
    un∀  : ∀{Γ A} → Pf Γ (∀' A) → (t : Ter) → Pf Γ (A t)

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixr 6 _⇒_
  infixr 8 _∧_

{-
module _ (funar : ℕ → ℕ) where
  data Ter : Set where
    fun : {n : ℕ} → Ter ^ (funar n) → Ter

-}

{-
module Peano where

  data Ter' : Set where
    zero : Ter'
    one  : Ter'
    _+_  : Ter' → Ter' → Ter'
    
  Rel' : ℕ → Set
  Rel' 0 = 𝟘
  Rel' 1 = 𝟘
  Rel' 2 = 𝟙
  Rel' _ = 𝟘

  -- ∀x.x+0 = x

  ex : For
  ex = ∀' (λ x → rel {2} * (x + zero) x)
-}

-- syntax

module I where

  data For  : Set where
       rel  : ∀{n} → Rel n → Ter ^ n → For
       ⊤    : For
       _⇒_  : For → For → For
       _∧_  : For → For → For
       ∀'   : (Ter → For) → For

  data Con  : Set where
       ∙    : Con
       _▷_  : Con → For → Con

  data Pfs  : Con → Con → Prop
  data Pf   : Con → For → Prop

  data Pfs    where
       id   : ∀{Γ} → Pfs Γ Γ
       _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
       p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
       ε    : ∀{Γ} → Pfs Γ ∙
       _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)

  data Pf     where
       _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
       q    : ∀{Γ A} → Pf  (Γ ▷ A) A
       tt   : ∀{Γ} → Pf Γ ⊤
       lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
       app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
       mk∀  : ∀{Γ A} → ((t : Ter) → Pf Γ (A t)) → Pf Γ (∀' A)
       un∀  : ∀{Γ A} → Pf Γ (∀' A) → (t : Ter) → Pf Γ (A t)
       pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
       fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
       snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B

  I : Algebra _ _
  I = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = id ;
    _∘_ = _∘_ ; _[_] = _[_] ; ∙ = ∙ ; ε = ε ; _▷_ = _▷_ ; _,_ = _,_ ;
    p = p ; q = q ; rel = rel ; ⊤ = ⊤ ; tt = tt ; _⇒_ = _⇒_ ; lam =
    lam ; app = app ; _∧_ = _∧_ ; pair = pair ; fst = fst ; snd = snd
    ; ∀' = ∀' ; mk∀ = mk∀ ; un∀ = un∀ }

  module I = Algebra I

module rec {i j}(M : Algebra i j) where

  module M = Algebra M

  ⟦_⟧F : I.For → M.For
  ⟦ I.rel r ts ⟧F = M.rel r ts
  ⟦ I.⊤ ⟧F = M.⊤
  ⟦ A I.⇒ B ⟧F = ⟦ A ⟧F M.⇒ ⟦ B ⟧F
  ⟦ A I.∧ B ⟧F = ⟦ A ⟧F M.∧ ⟦ B ⟧F
  ⟦ I.∀' A ⟧F = M.∀' λ t → ⟦ A t ⟧F

  ⟦_⟧C : I.Con → M.Con
  ⟦ I.∙ ⟧C = M.∙
  ⟦ Γ I.▷ A ⟧C = ⟦ Γ ⟧C M.▷ ⟦ A ⟧F

  ⟦_⟧s : ∀{Γ Δ} → I.Pfs Γ Δ  → M.Pfs ⟦ Γ ⟧C ⟦ Δ ⟧C
  ⟦_⟧P : ∀{Γ A} → I.Pf  Γ A  → M.Pf  ⟦ Γ ⟧C ⟦ A ⟧F

  ⟦ I.id ⟧s = M.id
  ⟦ σ I.∘ δ ⟧s = ⟦ σ ⟧s M.∘ ⟦ δ ⟧s
  ⟦ I.p ⟧s = M.p
  ⟦ I.ε ⟧s = M.ε
  ⟦ σ I., u ⟧s = ⟦ σ ⟧s M., ⟦ u ⟧P
  
  ⟦ u I.[ σ ] ⟧P = ⟦ u ⟧P M.[ ⟦ σ ⟧s ]
  ⟦ I.q ⟧P = M.q
  ⟦ I.tt ⟧P = M.tt
  ⟦ I.lam u ⟧P = M.lam ⟦ u ⟧P
  ⟦ I.app u ⟧P = M.app ⟦ u ⟧P
  ⟦ I.pair u v ⟧P = M.pair ⟦ u ⟧P ⟦ v ⟧P
  ⟦ I.fst u ⟧P = M.fst ⟦ u ⟧P
  ⟦ I.snd u ⟧P = M.snd ⟦ u ⟧P
  ⟦ I.mk∀ u ⟧P = M.mk∀ λ t → ⟦ u t ⟧P
  ⟦ I.un∀ u t ⟧P = M.un∀ ⟦ u ⟧P t

-- standard model

module _ (rel : ∀{n} → Rel n → Ter ^ n → Prop) where

  st : Algebra _ _
  st = record
    { Con = Prop
    ; Pfs = λ Γ Δ → Γ → Δ
    ; For = Prop
    ; Pf = λ Γ A → Γ → A
    ; id = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = 𝟙p
    ; ε = λ γ → *
    ; _▷_ = λ Γ A → Γ ×p A
    ; _,_ = λ σ u γ → (σ γ ,Σ u γ)
    ; p = proj₁
    ; q = proj₂
    ; rel = rel
    ; ⊤ = 𝟙p
    ; tt = λ γ → *
    ; _⇒_ = λ A B → A → B
    ; lam = λ u γ α → u (γ ,Σ α)
    ; app = λ u γ → u (proj₁ γ) (proj₂ γ)
    ; _∧_ = λ A B → A ×p B
    ; pair = λ u v γ → u γ ,Σ v γ
    ; fst = λ u γ → proj₁ (u γ)
    ; snd = λ u γ → proj₂ (u γ)
    ; ∀' = λ A → (t : Ter) → A t
    ; mk∀ = λ u γ t → u t γ
    ; un∀ = λ u t γ → u γ t
    }

module Kripke
  (W : Set)
  (_≤_ : W → W → Prop)
  (id≤ : {w : W} → w ≤ w)
  (_∘≤_ : {w w' w'' : W} → w' ≤ w → w'' ≤ w' → w'' ≤ w)
  (∣_$_∣rel : {n : ℕ} → Rel n → Ter ^ n → W → Prop)
  (_$_rel∶_⟨_⟩ : ∀{n}(r : Rel n)(ts : Ter ^ n) → ∀{w w'} → ∣ r $ ts ∣rel w → w' ≤ w → ∣ r $ ts ∣rel w')
  where

  record PSh : Set₁ where
    field
      ∣_∣    : W → Prop
      _∶_⟨_⟩ : ∀{w w'} → ∣_∣ w → w' ≤ w → ∣_∣ w'
  open PSh public

  K : Algebra _ _
  K = record
    { Con = PSh
    ; Pfs = λ Γ Δ → {w : W} → ∣ Γ ∣ w → ∣ Δ ∣ w
    ; For = PSh
    ; Pf  = λ Γ A → {w : W} → ∣ Γ ∣ w → ∣ A ∣ w
    ; id  = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = record { ∣_∣ = λ _ → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; ε = λ _ → *
    ; _▷_ = λ Γ A → record { ∣_∣ = λ w → ∣ Γ ∣ w ×p ∣ A ∣ w ; _∶_⟨_⟩ = λ { (γ ,Σ α) β → Γ ∶ γ ⟨ β ⟩ ,Σ A ∶ α ⟨ β ⟩ } }
    ; _,_ = λ σ u γ → σ γ ,Σ u γ
    ; p = proj₁
    ; q = proj₂
    ; rel = λ r ts → record { ∣_∣ = ∣ r $ ts ∣rel ; _∶_⟨_⟩ = r $ ts rel∶_⟨_⟩ }
    ; ⊤ = record { ∣_∣ = λ w → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; tt = λ _ → *
    ; _⇒_ = λ A B → record { ∣_∣ = λ w → ∀{w'} → w' ≤ w → ∣ A ∣ w' → ∣ B ∣ w' ; _∶_⟨_⟩ = λ {w w'} f β {w''} β' α → f (β ∘≤ β') α }
    ; lam = λ {Γ}{A} u γ β α → u (Γ ∶ γ ⟨ β ⟩ ,Σ α)
    ; app = λ { {Γ}{A}{B} u (γ ,Σ α) → u γ id≤ α }
    ; _∧_ = λ A B → record { ∣_∣ = λ w → ∣ A ∣ w ×p ∣ B ∣ w ; _∶_⟨_⟩ = λ { (α ,Σ α') β → A ∶ α ⟨ β ⟩ ,Σ B ∶ α' ⟨ β ⟩ } }
    ; pair = λ u v γ → u γ ,Σ v γ
    ; fst = λ u γ → proj₁ (u γ)
    ; snd = λ u γ → proj₂ (u γ)
    ; ∀' = λ A → record { ∣_∣ = λ w → (t : Ter) → ∣ A t ∣ w ; _∶_⟨_⟩ = λ f β t → A t ∶ f t ⟨ β ⟩ }
    ; mk∀ = λ u γ t → u t γ
    ; un∀ = λ u t γ → u γ t
    }

  module K = Algebra K

module Completeness where

  open Kripke
    I.Con
    I.Pfs
    I.id
    I._∘_
    (λ r ts Γ → I.Pf Γ (I.rel r ts))
    (λ r ts → I._[_])

  open rec K

  QF : (A : I.For){Γ : I.Con} → ∣ ⟦ A ⟧F ∣ Γ → I.Pf Γ A
  UF : (A : I.For){Γ : I.Con} → I.Pf Γ A → ∣ ⟦ A ⟧F ∣ Γ

  QF (I.rel r ts) u = u
  QF I.⊤          _ = I.tt
  QF (A I.⇒ B)    f = I.lam (QF B (f I.p (UF A I.q)))
  QF (A I.∧ B)    α = I.pair (QF A (proj₁ α)) (QF B (proj₂ α))
  QF (I.∀' A)     f = I.mk∀ λ t → QF (A t) (f t)

  UF (I.rel r ts) u = u
  UF I.⊤          _ = *
  UF (A I.⇒ B)    u = λ σ α → UF B (I.app u I.[ σ I., QF A α ])
  UF (A I.∧ B)    u = (UF A (I.fst u)) ,Σ (UF B (I.snd u))
  UF (I.∀' A)     u = λ t → UF (A t) (I.un∀ u t)

  UC : (Δ : I.Con){Γ : I.Con} → I.Pfs Γ Δ → ∣ ⟦ Δ ⟧C ∣ Γ
  UC I.∙       _ = *
  UC (Δ I.▷ A) σ = UC Δ (I.p I.∘ σ) ,Σ UF A (I.q I.[ σ ])

  compl : ∀{Γ A} → K.Pf ⟦ Γ ⟧C ⟦ A ⟧F → I.Pf Γ A
  compl {Γ}{A} f = QF A (f (UC Γ I.id))
