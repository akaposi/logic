{-# OPTIONS --prop #-}

module Sums where

open import lib

-- intuitionistic logic

record Model (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con  : Set i
    Pfs  : Con → Con → Prop j
    For  : Set i
    Pf   : Con → For → Prop j
    id   : ∀{Γ} → Pfs Γ Γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    ∙    : Con
    ε    : ∀{Γ} → Pfs Γ ∙
    _▷_  : Con → For → Con
    _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    q    : ∀{Γ A} → Pf  (Γ ▷ A) A

    ⊤    : For
    tt   : ∀{Γ} → Pf Γ ⊤

    _⇒_  : For → For → For
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

    _∨_  : For → For → For
    left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixr 6 _⇒_
  infixr 7 _∨_

-- syntax

module I where

  data For  : Set where
       ⊤    : For
       _⇒_  : For → For → For
       _∨_  : For → For → For

  data Con  : Set where
       ∙    : Con
       _▷_  : Con → For → Con

  data Pfs  : Con → Con → Prop
  data Pf   : Con → For → Prop

  data Pfs    where
       id   : ∀{Γ} → Pfs Γ Γ
       _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
       p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
       ε    : ∀{Γ} → Pfs Γ ∙
       _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)

  data Pf     where
       _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
       q    : ∀{Γ A} → Pf  (Γ ▷ A) A
       tt   : ∀{Γ} → Pf Γ ⊤
       lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
       app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
       left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
       right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
       case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

  I : Model _ _
  I = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = id ;
    _∘_ = _∘_ ; _[_] = _[_] ; ∙ = ∙ ; ε = ε ; _▷_ = _▷_ ; _,_ = _,_ ;
    p = p ; q = q ; ⊤ = ⊤ ; tt = tt ; _⇒_ = _⇒_ ; lam = lam ; app =
    app ; _∨_ = _∨_ ; left = left ; right = right ; case = case }

  module I = Model I

module rec {i j}(M : Model i j) where

  module M = Model M

  ⟦_⟧F : I.For → M.For
  ⟦ I.⊤ ⟧F = M.⊤
  ⟦ A I.⇒ B ⟧F = ⟦ A ⟧F M.⇒ ⟦ B ⟧F
  ⟦ A I.∨ B ⟧F = ⟦ A ⟧F M.∨ ⟦ B ⟧F

  ⟦_⟧C : I.Con → M.Con
  ⟦ I.∙ ⟧C = M.∙
  ⟦ Γ I.▷ A ⟧C = ⟦ Γ ⟧C M.▷ ⟦ A ⟧F

  ⟦_⟧s : ∀{Γ Δ} → I.Pfs Γ Δ  → M.Pfs ⟦ Γ ⟧C ⟦ Δ ⟧C
  ⟦_⟧P : ∀{Γ A} → I.Pf  Γ A  → M.Pf  ⟦ Γ ⟧C ⟦ A ⟧F

  ⟦ I.id ⟧s = M.id
  ⟦ σ I.∘ δ ⟧s = ⟦ σ ⟧s M.∘ ⟦ δ ⟧s
  ⟦ I.p ⟧s = M.p
  ⟦ I.ε ⟧s = M.ε
  ⟦ σ I., u ⟧s = ⟦ σ ⟧s M., ⟦ u ⟧P
  
  ⟦ u I.[ σ ] ⟧P = ⟦ u ⟧P M.[ ⟦ σ ⟧s ]
  ⟦ I.q ⟧P = M.q
  ⟦ I.tt ⟧P = M.tt
  ⟦ I.lam u ⟧P = M.lam ⟦ u ⟧P
  ⟦ I.app u ⟧P = M.app ⟦ u ⟧P
  ⟦ I.left u ⟧P = M.left ⟦ u ⟧P
  ⟦ I.right u ⟧P = M.right ⟦ u ⟧P
  ⟦ I.case u v w ⟧P = M.case ⟦ u ⟧P ⟦ v ⟧P ⟦ w ⟧P

-- standard model

st : Model _ _
st = record
  { Con = Prop
  ; Pfs = λ Γ Δ → Γ → Δ
  ; For = Prop
  ; Pf = λ Γ A → Γ → A
  ; id = λ γ → γ
  ; _∘_ = λ σ δ γ → σ (δ γ)
  ; _[_] = λ u σ γ → u (σ γ)
  ; ∙ = 𝟙p
  ; ε = λ γ → *
  ; _▷_ = λ Γ A → Γ ×p A
  ; _,_ = λ σ u γ → (σ γ ,Σ u γ)
  ; p = proj₁
  ; q = proj₂
  ; ⊤ = 𝟙p
  ; tt = λ γ → *
  ; _⇒_ = λ A B → A → B
  ; lam = λ u γ α → u (γ ,Σ α)
  ; app = λ u γ → u (proj₁ γ) (proj₂ γ)
  ; _∨_ = λ A B → A +p B
  ; left = λ u γ → inj₁ (u γ)
  ; right = λ u γ → inj₂ (u γ)
  ; case = λ u v w γ → ind+p _ (λ α → u (γ ,Σ α)) (λ α → v (γ ,Σ α)) (w γ)
  }

module _
  (W    : Set)
  (_≥_  : W → W → Prop)
  (id≥  : {w : W} → w ≥ w)
  (_∘≥_ : {w w' w'' : W} → w' ≥ w → w'' ≥ w' → w'' ≥ w)
  where

  record Sieve (w : W) : Set₁ where
    field
      ∣_∣    : ∀{w'} → w' ≥ w → Prop
      _∶_⟨_⟩ : ∀{w' w''}{e : w' ≥ w} → ∣_∣ e → (e' : w'' ≥ w') → ∣_∣ (e ∘≥ e')
  open Sieve public

  _∥_ : ∀{w w'} → Sieve w → w' ≥ w → Sieve w'
  ∣ P ∥ e ∣ e' = ∣ P ∣ (e ∘≥ e')
  (P ∥ e) ∶ p ⟨ e' ⟩ = P ∶ p ⟨ e' ⟩
  infix 30 _∥_

  module _
    (_◁_    : (w : W) → Sieve w → Prop)
    (max◁   : ∀{w P} → (∀{w'}{e : w' ≥ w} → ∣ P ∣ e) → w ◁ P)
    (stab◁  : ∀{w w' P} → w ◁ P → (e : w' ≥ w) → w' ◁ (P ∥ e))
    (trans◁ : ∀{w P Q} → w ◁ P → (∀{w'}{e : w' ≥ w} → ∣ P ∣ e → w' ◁ Q ∥ e) → w ◁ Q)
    where

    _⊆_ : ∀{w}(P Q : Sieve w) → Prop -- \sub=
    P ⊆ Q = ∀{w'}{e : w' ≥ _} → ∣ P ∣ e → ∣ Q ∣ e

    ⊆◁ : ∀{w P Q} → w ◁ P → P ⊆ Q → w ◁ Q
    ⊆◁ {w}{P}{Q} w◁P f = trans◁ w◁P λ {_}{e} e∈P → max◁ λ {_}{e''} → f (P ∶ e∈P ⟨ e'' ⟩)

    comp : ∀{w}(P : Sieve w)(Qs : ∀{w'}{e : w' ≥ w} → ∣ P ∣ e → Sieve w') → Sieve w
    ∣ comp {w} P Qs ∣ {w''} _ = ∃ W λ w' → Σp (w' ≥ w) λ e → Σp (w'' ≥ w') λ e' → Σp (∣ P ∣ e) λ e∈P → ∣ Qs e∈P ∣ e'
    comp {w} P Qs ∶ (w' ,∃ e ,Σ e' ,Σ e∈P ,Σ e'∈Q) ⟨ e'' ⟩ = w' ,∃ e ,Σ e' ∘≥ e'' ,Σ e∈P ,Σ Qs e∈P ∶ e'∈Q ⟨ e'' ⟩

    comp◁ : ∀{w}{P : Sieve w}{Qs : ∀{w'}{e : w' ≥ w} → ∣ P ∣ e → Sieve w'} →
      w ◁ P → (∀{w'}{e : w' ≥ w}(e∈P : ∣ P ∣ e) → w' ◁ Qs e∈P) → w ◁ comp P Qs
    comp◁ {w}{P}{Qs} w◁P ◁Qs =
      trans◁ w◁P λ {w'}{e} e∈P → ⊆◁ (◁Qs e∈P) λ {w''}{e'} e'∈Qs → (w' ,∃ e ,Σ e' ,Σ e∈P ,Σ e'∈Qs)

    comp' : ∀{w}(P : Sieve w)(Qs : ∀{w'}{e : w' ≥ w} → ∣ P ∣ e → ∃ (Sieve w') λ _ → 𝟙p) → ∃ (Sieve w) λ _ → 𝟙p
    comp' P Qs = {!comp P !}

    record Sh : Set₂ where
      field
        ∣_∣    : W → Prop₁
        _∶_⟨_⟩ : ∀{w w'} → ∣_∣ w → w' ≥ w → ∣_∣ w'
        paste  : ∀{w P} → w ◁ P → (∀{w'}{e : w' ≥ w} → Sieve.∣ P ∣ e → ∣_∣ w') → ∣_∣ w
    open Sh public

    Con = Sh
    Pfs : Con → Con → Prop₁
    Pfs Γ Δ = {w : W} → ∣ Γ ∣ w → ∣ Δ ∣ w
    For = Sh
    Pf : Con → For → Prop₁
    Pf Γ A = {w : W} → ∣ Γ ∣ w → ∣ A ∣ w
    id : ∀{Γ} → Pfs Γ Γ
    id γ = γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    (σ ∘ δ) γ = σ (δ γ)
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    (t [ σ ]) γ = t (σ γ)
    ∙ : Con
    ∣ ∙ ∣ w = ↑pl 𝟙p
    ∙ ∶ _ ⟨ e ⟩ = mk↑pl *
    paste ∙ _ _ = mk↑pl *
    ε : ∀{Γ} → Pfs Γ ∙
    ε γ = mk↑pl *
    _▷_  : Con → For → Con
    ∣ Γ ▷ A ∣ w = ∣ Γ ∣ w ×p ∣ A ∣ w
    (Γ ▷ A) ∶ (γ ,Σ α) ⟨ e ⟩ = Γ ∶ γ ⟨ e ⟩ ,Σ A ∶ α ⟨ e ⟩
    paste (Γ ▷ A) w◁P f = paste Γ w◁P (λ p → proj₁ (f p)) ,Σ paste A w◁P (λ p → proj₂ (f p))
    _,_ : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    (σ , t) γ = σ γ ,Σ t γ
    p : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    p γ = proj₁ γ
    q : ∀{Γ A} → Pf  (Γ ▷ A) A
    q γ = proj₂ γ
    ⊤ : For
    ⊤ = ∙
    tt : ∀{Γ} → Pf Γ ⊤
    tt {Γ} = ε {Γ}
    _⇒_ : For → For → For
    ∣ A ⇒ B ∣ w = ∀{w'} → w' ≥ w → ∣ A ∣ w' → ∣ B ∣ w'
    ((A ⇒ B) ∶ f ⟨ e ⟩) e' α = f (e ∘≥ e') α
    paste (A ⇒ B) w◁P f {w'} e α = paste B (stab◁ w◁P e) λ {w''}{e'} e'∈P → f e'∈P id≥ (A ∶ α ⟨ e' ⟩)
    lam : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
    lam {Γ}{A} t γ e α = t (Γ ∶ γ ⟨ e ⟩ ,Σ α)
    app : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
    app t (γ ,Σ α) = t γ id≥ α

    a : {A B : Set}{C : B → Prop}{D : Prop} → (A → ∃ B C) → ((A → B) → D) → D
    a f g = g λ a → {!!}

    _∨_ : For → For → For
    ∣ A ∨ B ∣ w = ∃ (Sieve w) λ P → w ◁ P ×p (∀{w'}{e : w' ≥ w} → ∣ P ∣ e → ∣ A ∣ w' +p ∣ B ∣ w')
    (A ∨ B) ∶ P ,∃ (w◁P ,Σ f) ⟨ e ⟩ = P ∥ e ,∃ (stab◁ w◁P e ,Σ f)
    paste (A ∨ B) {w}{P} w◁P f = {!!} -- comp P {!!} ,∃ {!!}
    
    {-
    left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C
    -}

{-
  B : Model _ _
  B = record
    { Con = Sh
    ; Pfs = λ Γ Δ → {w : W} → ∣ Γ ∣ w → ∣ Δ ∣ w
    ; For = Sh
    ; Pf  = λ Γ A → {w : W} → ∣ Γ ∣ w → ∣ A ∣ w
    ; id  = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = record { ∣_∣ = λ _ → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; ε = λ _ → *
    ; _▷_ = λ Γ A → record
      { ∣_∣ = λ w → ∣ Γ ∣ w ×p ∣ A ∣ w
      ; _∶_⟨_⟩ = λ { (γ ,Σ α) β → Γ ∶ γ ⟨ β ⟩ ,Σ A ∶ α ⟨ β ⟩ }
      ; paste = λ w◁C α → (paste Γ w◁C λ Cw' → proj₁ (α Cw')) ,Σ paste A w◁C λ Cw' → proj₂ (α Cw') }
    ; _,_ = λ σ u γ → σ γ ,Σ u γ
    ; p = proj₁
    ; q = proj₂
    ; ⊤ = record { ∣_∣ = λ w → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; tt = λ _ → *
    ; _⇒_ = λ A B → record
      { ∣_∣ = λ w → ∀{w'} → w' ≤ w → ∣ A ∣ w' → ∣ B ∣ w'
      ; _∶_⟨_⟩ = λ {w w'} f β {w''} β' α → f (β ∘≤ β') α
      ; paste = λ w◁C f β α → {!!} }
    ; lam = λ {Γ}{A} u γ β α → u (Γ ∶ γ ⟨ β ⟩ ,Σ α)
    ; app = λ { {Γ}{A}{B} u (γ ,Σ α) → u γ id≤ α }
    ; _∨_ = λ A B → record
      { ∣_∣ = λ w → ∣ A ∣ w +p ∣ B ∣ w ;
      _∶_⟨_⟩ = λ α β → ind+p _ (λ x → inj₁ (A ∶ x ⟨ β ⟩)) (λ y → inj₂ (B ∶ y ⟨ β ⟩)) α
      ; paste = {!!} }
    ; left = λ u γ → inj₁ (u γ)
    ; right = λ u γ → inj₂ (u γ)
    ; case = λ u u' v γ → ind+p _ (λ α → u (γ ,Σ α)) (λ α → u' (γ ,Σ α)) (v γ)
    }

  module K = Model K

module Completeness where

  open Kripke I.Con I.Pfs I.id I._∘_

  open rec K

  QF : (A : I.For){Γ : I.Con} → ∣ ⟦ A ⟧F ∣ Γ → I.Pf Γ A
  UF : (A : I.For){Γ : I.Con} → I.Pf Γ A → ∣ ⟦ A ⟧F ∣ Γ

  QF I.⊤       _ = I.tt
  QF (A I.⇒ B) f = I.lam (QF B (f I.p (UF A I.q)))
  QF (A I.∨ B) f = ind+p _ (λ α → I.left (QF A α)) (λ α → I.right (QF B α)) f

  UF I.⊤       _ = *
  UF (A I.⇒ B) u = λ σ α → UF B (I.app u I.[ σ I., QF A α ])
  UF (A I.∨ B) u = {!I.case I.q ? u!}

  UC : (Δ : I.Con){Γ : I.Con} → I.Pfs Γ Δ → ∣ ⟦ Δ ⟧C ∣ Γ
  UC I.∙       _ = *
  UC (Δ I.▷ A) σ = UC Δ (I.p I.∘ σ) ,Σ UF A (I.q I.[ σ ])

  compl : ∀{Γ A} → K.Pf ⟦ Γ ⟧C ⟦ A ⟧F → I.Pf Γ A
  compl {Γ}{A} f = QF A (f (UC Γ I.id))
-}
