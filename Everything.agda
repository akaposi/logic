{-# OPTIONS --prop #-}

open import lib

module Everything
  (Ter : Set)     -- set of terms
  (Rel : ℕ → Set) -- Rel n is the set of relations of arity n
  where

-- intuitionistic logic

record Algebra (i j : Level) : Set (lsuc (i ⊔ j)) where
  field
    Con  : Set i
    Pfs  : Con → Con → Prop j
    For  : Set i
    Pf   : Con → For → Prop j
    id   : ∀{Γ} → Pfs Γ Γ
    _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
    _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
    ∙    : Con
    ε    : ∀{Γ} → Pfs Γ ∙
    _▷_  : Con → For → Con
    _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)
    p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
    q    : ∀{Γ A} → Pf  (Γ ▷ A) A

    rel  : ∀{n} → Rel n → Ter ^ n → For

    _⇒_  : For → For → For
    lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
    app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B

    ⊤    : For
    tt   : ∀{Γ} → Pf Γ ⊤

    _∧_  : For → For → For
    pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
    fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
    snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B

    -- ∀': For × For × For × ... → For
    --     \___________________/
    --           Ter many
    ∀'   : (Ter → For) → For
    mk∀  : ∀{Γ A} → ((t : Ter) → Pf Γ (A t)) → Pf Γ (∀' A)
    un∀  : ∀{Γ A} → Pf Γ (∀' A) → (t : Ter) → Pf Γ (A t)

    ⊥    : For
    exfalso : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A
      
    _∨_  : For → For → For
    left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
    right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
    case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C

    ∃'   : (Ter → For) → For
    mk∃  : ∀{Γ A} → (∃ Ter λ t → Pf Γ (A t)) → Pf Γ (∃' A)
    un∃  : ∀{Γ A C} → Pf Γ (∃' A) → ((t : Ter) → Pf (Γ ▷ A t) C) → Pf Γ C

  infixl 5 _▷_
  infixl 5 _,_
  infixr 6 _∘_
  infixl 8 _[_]
  infixr 6 _⇒_
  infixr 8 _∧_
  infixr 7 _∨_

-- syntax

module I where

  data For  : Set where
       rel  : ∀{n} → Rel n → Ter ^ n → For
       _⇒_  : For → For → For
       ⊤    : For
       _∧_  : For → For → For
       ∀'   : (Ter → For) → For
       ⊥    : For
       _∨_  : For → For → For
       ∃'   : (Ter → For) → For

  data Con  : Set where
       ∙    : Con
       _▷_  : Con → For → Con

  data Pfs  : Con → Con → Prop
  data Pf   : Con → For → Prop

  data Pfs    where
       id   : ∀{Γ} → Pfs Γ Γ
       _∘_  : ∀{Γ Θ Δ} → Pfs Θ Δ → Pfs Γ Θ → Pfs Γ Δ
       p    : ∀{Γ A} → Pfs (Γ ▷ A) Γ
       ε    : ∀{Γ} → Pfs Γ ∙
       _,_  : ∀{Γ Δ A} → Pfs Γ Δ → Pf Γ A → Pfs Γ (Δ ▷ A)

  data Pf     where
       _[_] : ∀{Γ Δ A} → Pf Δ A → Pfs Γ Δ → Pf Γ A
       q    : ∀{Γ A} → Pf  (Γ ▷ A) A
       lam  : ∀{Γ A B} → Pf (Γ ▷ A) B → Pf Γ (A ⇒ B)
       app  : ∀{Γ A B} → Pf Γ (A ⇒ B) → Pf (Γ ▷ A) B
       tt   : ∀{Γ} → Pf Γ ⊤
       pair : ∀{Γ A B} → Pf Γ A → Pf Γ B → Pf Γ (A ∧ B)
       fst  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ A
       snd  : ∀{Γ A B} → Pf Γ (A ∧ B) → Pf Γ B
       mk∀  : ∀{Γ A} → ((t : Ter) → Pf Γ (A t)) → Pf Γ (∀' A)
       un∀  : ∀{Γ A} → Pf Γ (∀' A) → (t : Ter) → Pf Γ (A t)
       exfalso : ∀{Γ A} → Pf Γ ⊥ → Pf Γ A
       left : ∀{Γ A B} → Pf Γ A → Pf Γ (A ∨ B)
       right : ∀{Γ A B} → Pf Γ B → Pf Γ (A ∨ B)
       case : ∀{Γ A B C} → Pf (Γ ▷ A) C → Pf (Γ ▷ B) C → Pf Γ (A ∨ B) → Pf Γ C
       mk∃  : ∀{Γ A} → (∃ Ter λ t → Pf Γ (A t)) → Pf Γ (∃' A)
       un∃  : ∀{Γ A C} → Pf Γ (∃' A) → ((t : Ter) → Pf (Γ ▷ A t) C) → Pf Γ C

  I : Algebra _ _
  I = record { Con = Con ; Pfs = Pfs ; For = For ; Pf = Pf ; id = id ;
    _∘_ = _∘_ ; _[_] = _[_] ; ∙ = ∙ ; ε = ε ; _▷_ = _▷_ ; _,_ = _,_ ;
    p = p ; q = q ; rel = rel ; ⊤ = ⊤ ; tt = tt ; _⇒_ = _⇒_ ; lam =
    lam ; app = app ; _∧_ = _∧_ ; pair = pair ; fst = fst ; snd = snd
    ; _∨_ = _∨_ ; left = left ; right = right ; case = case ; ∀' = ∀'
    ; mk∀ = mk∀ ; un∀ = un∀ ; ∃' = ∃' ; mk∃ = mk∃ ; un∃ = un∃ ; ⊥ = ⊥
    ; exfalso = exfalso }

  module I = Algebra I

module rec {i j}(M : Algebra i j) where

  module M = Algebra M

  ⟦_⟧F : I.For → M.For
  ⟦ I.rel r ts ⟧F = M.rel r ts
  ⟦ A I.⇒ B ⟧F = ⟦ A ⟧F M.⇒ ⟦ B ⟧F
  ⟦ I.⊤ ⟧F = M.⊤
  ⟦ A I.∧ B ⟧F = ⟦ A ⟧F M.∧ ⟦ B ⟧F
  ⟦ I.∀' A ⟧F = M.∀' λ t → ⟦ A t ⟧F
  ⟦ I.⊥ ⟧F = M.⊥
  ⟦ A I.∨ B ⟧F = ⟦ A ⟧F M.∨ ⟦ B ⟧F
  ⟦ I.∃' A ⟧F = M.∃' λ t → ⟦ A t ⟧F

  ⟦_⟧C : I.Con → M.Con
  ⟦ I.∙ ⟧C = M.∙
  ⟦ Γ I.▷ A ⟧C = ⟦ Γ ⟧C M.▷ ⟦ A ⟧F

  ⟦_⟧s : ∀{Γ Δ} → I.Pfs Γ Δ  → M.Pfs ⟦ Γ ⟧C ⟦ Δ ⟧C
  ⟦_⟧P : ∀{Γ A} → I.Pf  Γ A  → M.Pf  ⟦ Γ ⟧C ⟦ A ⟧F

  ⟦ I.id ⟧s = M.id
  ⟦ σ I.∘ δ ⟧s = ⟦ σ ⟧s M.∘ ⟦ δ ⟧s
  ⟦ I.p ⟧s = M.p
  ⟦ I.ε ⟧s = M.ε
  ⟦ σ I., u ⟧s = ⟦ σ ⟧s M., ⟦ u ⟧P
  
  ⟦ u I.[ σ ] ⟧P = ⟦ u ⟧P M.[ ⟦ σ ⟧s ]
  ⟦ I.q ⟧P = M.q
  ⟦ I.lam u ⟧P = M.lam ⟦ u ⟧P
  ⟦ I.app u ⟧P = M.app ⟦ u ⟧P
  ⟦ I.tt ⟧P = M.tt
  ⟦ I.pair u v ⟧P = M.pair ⟦ u ⟧P ⟦ v ⟧P
  ⟦ I.fst u ⟧P = M.fst ⟦ u ⟧P
  ⟦ I.snd u ⟧P = M.snd ⟦ u ⟧P
  ⟦ I.mk∀ u ⟧P = M.mk∀ λ t → ⟦ u t ⟧P
  ⟦ I.un∀ u t ⟧P = M.un∀ ⟦ u ⟧P t
  ⟦ I.exfalso u ⟧P = M.exfalso ⟦ u ⟧P
  ⟦ I.left u ⟧P = M.left ⟦ u ⟧P
  ⟦ I.right u ⟧P = M.right ⟦ u ⟧P
  ⟦ I.case u v w ⟧P = M.case ⟦ u ⟧P ⟦ v ⟧P ⟦ w ⟧P
  ⟦ I.mk∃ (t ,∃ u) ⟧P = M.mk∃ (t ,∃ ⟦ u ⟧P)
  ⟦ I.un∃ u v ⟧P = M.un∃ ⟦ u ⟧P λ t → ⟦ v t ⟧P

-- standard model

module _ (rel : ∀{n} → Rel n → Ter ^ n → Prop) where

  st : Algebra _ _
  st = record
    { Con = Prop
    ; Pfs = λ Γ Δ → Γ → Δ
    ; For = Prop
    ; Pf = λ Γ A → Γ → A
    ; id = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = 𝟙p
    ; ε = λ γ → *
    ; _▷_ = λ Γ A → Γ ×p A
    ; _,_ = λ σ u γ → (σ γ ,Σ u γ)
    ; p = proj₁
    ; q = proj₂
    ; rel = rel
    ; _⇒_ = λ A B → A → B
    ; lam = λ u γ α → u (γ ,Σ α)
    ; app = λ u γ → u (proj₁ γ) (proj₂ γ)
    ; ⊤ = 𝟙p
    ; tt = λ γ → *
    ; _∧_ = λ A B → A ×p B
    ; pair = λ u v γ → u γ ,Σ v γ
    ; fst = λ u γ → proj₁ (u γ)
    ; snd = λ u γ → proj₂ (u γ)
    ; _∨_ = λ A B → A +p B
    ; ∀' = λ A → (t : Ter) → A t
    ; mk∀ = λ u γ t → u t γ
    ; un∀ = λ u t γ → u γ t
    ; ⊥ = 𝟘p
    ; exfalso = λ u γ → ind𝟘p (u γ)
    ; left = λ t γ → inj₁ (t γ)
    ; right = λ t γ → inj₂ (t γ)
    ; case = λ u v w γ → ind+p _ (λ α → u (γ ,Σ α)) (λ α → v (γ ,Σ α)) (w γ)
    ; ∃' = λ A → ∃ Ter A
    ; mk∃ = λ { (t ,∃ u) γ → t ,∃ u γ }
    ; un∃ = λ u v γ → with∃ (u γ) λ t α → v t (γ ,Σ α)
    }

module Kripke
  (W : Set)
  (_≤_ : W → W → Prop)
  (id≤ : {w : W} → w ≤ w)
  (_∘≤_ : {w w' w'' : W} → w' ≤ w → w'' ≤ w' → w'' ≤ w)
  (∣_$_∣rel : {n : ℕ} → Rel n → Ter ^ n → W → Prop)
  (_$_rel∶_⟨_⟩ : ∀{n}(r : Rel n)(ts : Ter ^ n) → ∀{w w'} → ∣ r $ ts ∣rel w → w' ≤ w → ∣ r $ ts ∣rel w')
  where

  record PSh : Set₁ where
    field
      ∣_∣    : W → Prop
      _∶_⟨_⟩ : ∀{w w'} → ∣_∣ w → w' ≤ w → ∣_∣ w'
  open PSh public

  K : Algebra _ _
  K = record
    { Con = PSh
    ; Pfs = λ Γ Δ → {w : W} → ∣ Γ ∣ w → ∣ Δ ∣ w
    ; For = PSh
    ; Pf  = λ Γ A → {w : W} → ∣ Γ ∣ w → ∣ A ∣ w
    ; id  = λ γ → γ
    ; _∘_ = λ σ δ γ → σ (δ γ)
    ; _[_] = λ u σ γ → u (σ γ)
    ; ∙ = record { ∣_∣ = λ _ → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; ε = λ _ → *
    ; _▷_ = λ Γ A → record { ∣_∣ = λ w → ∣ Γ ∣ w ×p ∣ A ∣ w ; _∶_⟨_⟩ = λ { (γ ,Σ α) β → Γ ∶ γ ⟨ β ⟩ ,Σ A ∶ α ⟨ β ⟩ } }
    ; _,_ = λ σ u γ → σ γ ,Σ u γ
    ; p = proj₁
    ; q = proj₂
    ; rel = λ r ts → record { ∣_∣ = ∣ r $ ts ∣rel ; _∶_⟨_⟩ = r $ ts rel∶_⟨_⟩ }
    ; _⇒_ = λ A B → record { ∣_∣ = λ w → ∀{w'} → w' ≤ w → ∣ A ∣ w' → ∣ B ∣ w' ; _∶_⟨_⟩ = λ {w w'} f β {w''} β' α → f (β ∘≤ β') α }
    ; lam = λ {Γ}{A} u γ β α → u (Γ ∶ γ ⟨ β ⟩ ,Σ α)
    ; app = λ { {Γ}{A}{B} u (γ ,Σ α) → u γ id≤ α }
    ; ⊤ = record { ∣_∣ = λ w → 𝟙p ; _∶_⟨_⟩ = λ _ _ → * }
    ; tt = λ _ → *
    ; _∧_ = λ A B → record { ∣_∣ = λ w → ∣ A ∣ w ×p ∣ B ∣ w ; _∶_⟨_⟩ = λ { (α ,Σ α') β → A ∶ α ⟨ β ⟩ ,Σ B ∶ α' ⟨ β ⟩ } }
    ; pair = λ u v γ → u γ ,Σ v γ
    ; fst = λ u γ → proj₁ (u γ)
    ; snd = λ u γ → proj₂ (u γ)
    ; ∀' = λ A → record { ∣_∣ = λ w → (t : Ter) → ∣ A t ∣ w ; _∶_⟨_⟩ = λ f β t → A t ∶ f t ⟨ β ⟩ }
    ; mk∀ = λ u γ t → u t γ
    ; un∀ = λ u t γ → u γ t
    ; ⊥ = record { ∣_∣ = λ _ → 𝟘p ; _∶_⟨_⟩ = λ () }
    ; exfalso = λ t γ → ind𝟘p (t γ)
    ; _∨_ = λ A B → record { ∣_∣ = λ w → ∣ A ∣ w +p ∣ B ∣ w ; _∶_⟨_⟩ = λ α β → ind+p _ (λ x → inj₁ (A ∶ x ⟨ β ⟩)) (λ y → inj₂ (B ∶ y ⟨ β ⟩)) α }
    ; left = λ u γ → inj₁ (u γ)
    ; right = λ u γ → inj₂ (u γ)
    ; case = λ u u' v γ → ind+p _ (λ α → u (γ ,Σ α)) (λ α → u' (γ ,Σ α)) (v γ)
    ; ∃' = λ A → record { ∣_∣ = λ w → ∃ Ter λ t → ∣ A t ∣ w ; _∶_⟨_⟩ = λ { (t ,∃ α) f → t ,∃ (A t ∶ α ⟨ f ⟩) } }
    ; mk∃ = λ { (t ,∃ β) γ → t ,∃ β γ }
    ; un∃ = λ β β' γ → with∃ (β γ) λ t α → β' t (γ ,Σ α)
    }

  module K = Algebra K
