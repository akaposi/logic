
open import Agda.Primitive
open import Agda.Builtin.Equality
open import Agda.Builtin.Bool
open import Agda.Builtin.Unit renaming (⊤ to Unit)
open import Agda.Builtin.Sigma
open import Agda.Builtin.Nat renaming (Nat to ℕ) hiding (_==_)

data Empty : Set where

exfalso : {i : Level} {A : Set i} → Empty → A
exfalso ()

data _⊎_ {i j}(A : Set i)(B : Set j) : Set (i ⊔ j) where
  inl : A → A ⊎ B
  inr : B → A ⊎ B

case : {i j k : Level}{A : Set i}{B : Set j}{C : Set k} → A ⊎ B → (A → C) → (B → C) → C
case (inl a) f _ = f a
case (inr b) _ g = g b

_×_ : ∀{i j} → Set i → Set j → Set (i ⊔ j)
A × B = Σ A λ _ → B

record Lift {i j}(A : Set i) : Set (i ⊔ j) where
  constructor mk
  field un : A
open Lift

module infinitary where

  data Tm : Set where
    const  : ℕ → Tm
  _+t_   : Tm → Tm → Tm
  const x +t const x' = const (x + x')
  _*t_   : Tm → Tm → Tm
  const x *t const x' = const (x * x')

  data For : Set where
    _∧_ _∨_ _⊃_ : For → For → For
    ⊤ ⊥         : For
    _>_ _==_    : Tm → Tm → For
    ∀'          : (Tm → For) → For

  _≟_ : ℕ → ℕ → Bool
  zero ≟ zero = true
  zero ≟ suc n = false
  suc m ≟ zero = false
  suc m ≟ suc n = m ≟ n

  _≟F_ : For → For → Bool
  (A ∧ A₁) ≟F (B ∧ B₁) = {!!}
  (A ∧ A₁) ≟F (B ∨ B₁) = {!!}
  (A ∧ A₁) ≟F (B ⊃ B₁) = {!!}
  (A ∧ A₁) ≟F ⊤ = {!!}
  (A ∧ A₁) ≟F ⊥ = {!!}
  (A ∧ A₁) ≟F (x > x₁) = {!!}
  (A ∧ A₁) ≟F (x == x₁) = {!!}
  (A ∧ A₁) ≟F ∀' x = {!!}
  (A ∨ A₁) ≟F (B ∧ B₁) = {!!}
  (A ∨ A₁) ≟F (B ∨ B₁) = {!A ≟ B && A₁ ≟ B₁!}
  (A ∨ A₁) ≟F (B ⊃ B₁) = {!!}
  (A ∨ A₁) ≟F ⊤ = {!!}
  (A ∨ A₁) ≟F ⊥ = {!!}
  (A ∨ A₁) ≟F (x > x₁) = {!!}
  (A ∨ A₁) ≟F (x == x₁) = {!!}
  (A ∨ A₁) ≟F ∀' x = {!!}
  (A ⊃ A₁) ≟F (B ∧ B₁) = {!!}
  (A ⊃ A₁) ≟F (B ∨ B₁) = {!!}
  (A ⊃ A₁) ≟F (B ⊃ B₁) = {!!}
  (A ⊃ A₁) ≟F ⊤ = {!!}
  (A ⊃ A₁) ≟F ⊥ = {!!}
  (A ⊃ A₁) ≟F (x > x₁) = false
  (A ⊃ A₁) ≟F (x == x₁) = false
  (A ⊃ A₁) ≟F ∀' x = false
  ⊤ ≟F (B ∧ B₁) = false
  ⊤ ≟F (B ∨ B₁) = false
  ⊤ ≟F (B ⊃ B₁) = false
  ⊤ ≟F ⊤ = true
  ⊤ ≟F ⊥ = false
  ⊤ ≟F (x > x₁) = {!!}
  ⊤ ≟F (x == x₁) = {!!}
  ⊤ ≟F ∀' x = {!!}
  ⊥ ≟F (B ∧ B₁) = {!!}
  ⊥ ≟F (B ∨ B₁) = {!!}
  ⊥ ≟F (B ⊃ B₁) = {!!}
  ⊥ ≟F ⊤ = {!!}
  ⊥ ≟F ⊥ = {!!}
  ⊥ ≟F (x > x₁) = {!!}
  ⊥ ≟F (x == x₁) = {!!}
  ⊥ ≟F ∀' x = {!!}
  (x > x₁) ≟F (B ∧ B₁) = {!!}
  (x > x₁) ≟F (B ∨ B₁) = {!!}
  (x > x₁) ≟F (B ⊃ B₁) = {!!}
  (x > x₁) ≟F ⊤ = {!!}
  (x > x₁) ≟F ⊥ = {!!}
  (x > x₁) ≟F (x₂ > x₃) = {!!}
  (x > x₁) ≟F (x₂ == x₃) = {!!}
  (x > x₁) ≟F ∀' x₂ = {!!}
  (x == x₁) ≟F (B ∧ B₁) = {!!}
  (x == x₁) ≟F (B ∨ B₁) = {!!}
  (x == x₁) ≟F (B ⊃ B₁) = {!!}
  (x == x₁) ≟F ⊤ = {!!}
  (x == x₁) ≟F ⊥ = {!!}
  (x == x₁) ≟F (x₂ > x₃) = {!!}
  (x == x₁) ≟F (x₂ == x₃) = {!!}
  (x == x₁) ≟F ∀' x₂ = {!!}
  ∀' x ≟F (B ∧ B₁) = false
  ∀' x ≟F (B ∨ B₁) = false
  ∀' x ≟F (B ⊃ B₁) = false
  ∀' x ≟F ⊤ = false
  ∀' x ≟F ⊥ = false
  ∀' x ≟F (x₁ > x₂) = false
  ∀' x ≟F (x₁ == x₂) = false
  ∀' F ≟F ∀' G = {!F (const 0) ≟F G (const 0) && F (const 1) ≟F G (const 1) && ... !}

  AF AF' : For
  AF = ∀' λ x → (x +t const 1) == x
  AF' = ∀' λ x → (const 1 +t x) == x

  ∀xx+1>x : For
  ∀xx+1>x = ∀' (λ x → (x +t const 1) > x)

  F : Tm → For
  F (const zero) = ⊤
  F (const (suc x)) = F (const x) ⊃ F (const x)

  B B' B'' : For
  B = ∀' (λ x → const 1 == const 1)
  B' = ∀' (λ x → x == x)
  B'' = ∀' F

-- ∀x.F(x)
-- F(0) = ⊤
-- F(1) = ⊤⊃⊤
-- F(2) = (⊤⊃⊤)⊃(⊤⊃⊤)
-- ....
{-
    ∀'
   / | \ \ \ ...
  ⊤  ⊃ ...
    / \
    ⊤ ⊤
    
1==2 ∨ 3 == 3

       ∨
      / \
     ==  ==
    / \  / \
   1  2  3 3

-}


{-
For   : Set
Tm    : Set
_∧_   : For → For → For
_>_   : Tm → Tm → For
Con   : Set
_+_   : Tm → Tm → Tm
const : ℕ → Tm
∀'    : (Tm → For) → For       -- (ℕ → 𝟚)-nek tobb eleme van, mint ℕ-nek
                               -- ∀' P  ?=  ∀' Q,       P, Q : Tm → For

--------------------------
    1
lim(-)
x↦∞ x

2
∫ 1+x² dx
0

(λ x → x + 2)

Con := bizonyitas leirasahoz kellenek bizonyitasvaltozok, formulak leirasahoz pedig kellenek termvaltozok

category with families

-- category
Con  : Set
Sub  : Con → Con → Set
_∘_  : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
ass  : (γ∘δ)∘θ = γ∘(δ∘θ)
id   : Sub Γ Γ
idl  : id∘γ = γ
idr  : γ∘id = γ
-- category has a terminal object
◇    : Con          -- zero
ε    : Sub Γ ◇
◇η   : (σ : Sub Γ ◇) → σ = ε

-- Tm is a locally representable functor from the category to SET
Tm   : Con → Set
_[_] : Tm Γ → Sub Δ Γ → Tm Δ
[∘]  : t[γ∘δ] = t[γ][δ]
[id] : t[id] = t
_▹   : Con → Con    -- suc
_,_  : Sub Δ Γ → Tm Δ → Sub Δ (Γ ▹)
p    : Sub (Γ ▹) Γ
q    : Tm (Γ ▹)
▹β₁  : p∘(γ,t)=γ
▹β₂  : q[γ,t]=t
▹η   : γt = (p∘γt,q[γt])

For  : Con → Set
_[_] : For Γ → Sub Δ Γ → For Δ
[∘]  : A[γ∘δ] = A[γ][δ]
[id] : A[id] = A

∀'   : For (Γ ▹) → For Γ
∀[]  : (∀' A)[γ] = ∀' (A[γ∘p,q])
∃'   : For (Γ ▹) → For Γ
∃[]  : (∃' A)[γ] = ∃' (A[γ∘p,q])
_>_  : Tm Γ → Tm Γ → For Γ
>[]  : (t>t')[γ] = (t[γ]) > (t'[γ])
_+_  : Tm Γ → Tm Γ → Tm Γ
+[]  : (t+t')[γ] = t[γ] + t'[γ]
_-_  : Tm Γ → Tm Γ → Tm Γ
-[]  : (t-t')[γ] = t[γ] - t'[γ]
abs  : Tm Γ → Tm Γ
abs[] : (abs t)[γ] = abs (t[γ])
const : ℕ → Tm Γ
const[] : (const n)[γ] = const n
_⊃_  : For Γ → For Γ → For Γ
⊃[]  : (A⊃B)[γ] = A[γ] ⊃ B[γ]
_∧_  : For Γ → For Γ → For Γ
∧[]  : (A∧B)[γ] = A[γ] ∧ B[γ]
Eq   : Tm Γ → Tm Γ → For Γ
Eq[] : (Eq t t')[γ] = Eq (t[γ]) (t[γ'])

∀x.x+1>x : For ◇   := ∀' (q + const 1) > q
∀x.x>2 ⊃ ∀y.x>y+1  := ∀' (q > const 2 ⊃ ∀' q[p] > q + const 1) : For ◇
                          ^:For (◇▹)     ^:For(◇▹▹)

v0 = q          : Tm (Γ ▹)      
v1 = q[p]       : Tm (Γ ▹ ▹)
v2 = q[p][p]    : Tm (Γ ▹ ▹ ▹)
v3 = q[p][p][p] : Tm (Γ ▹ ▹ ▹ ▹)
...
zero = q
suc  = _[p]

1 + 1 = 2

----------------------------------------------------------------------------

Con  : Set                 = ℕ
◇    : Con
_▹   : Con → Con
Var  : Con → Set           = Fin
vz   : Var (Γ ▹)
vs   : Var Γ → Var (Γ ▹)
Tm   : Con → Set
var  : Var Γ → Tm Γ
zero : Tm Γ
suc  : Tm Γ → Tm Γ
_+_  : Tm Γ → Tm Γ → Tm Γ
_*_  : Tm Γ → Tm Γ → Tm Γ
_^_  : Tm Γ → Tm Γ → Tm Γ
+β₁  : zero  + n = n
+β₂  : suc m + n = suc (m + n)
*β₁  : zero  * n = zero
*β₂  : suc m * n = n + (m * n)
^β₁  : m ^ zero = suc zero
^β₂  : m * (suc n) = m * (m ^ n)
For  : Con → Set
_∧_ _∨_ _⊃_ : For Γ → For Γ → For Γ
⊥ ⊤ : For Γ
∀' ∃ : For (Γ ▹) → For Γ
Eq : Tm Γ → Tm Γ → For Γ
Conp : Con → Set
◇ : Conp Γ
_▹_ : Conp Γ → For Γ → Conp Γ
Pf : (Γ : Con)(Δ : Conp Γ) → For Γ → Set -- (Prop)
vz : Pf Γ (Δ ▹ A) A
vs : Pf Γ Δ A → Pf Γ (Δ ▹ B) A
elim⊃ : Pf Γ Δ (A ⊃ B) → Pf Γ Δ A → Pf Γ Δ B
intro⊃ : Pf Γ (Δ ▹ A) B → Pf Γ Δ (A ⊃ B)
elim∀ : Pf Γ Δ (∀' A) → (t : Tm Γ) → Pf Γ Δ (A[t]) --- NEM MUKODIK, MERT KELL _[_] HELYETTESITES

refl : Eq t t
reflect : Eq t t' → t = t'

_>_ : Tm Γ → Tm Γ → For Γ
t > t' := ∃ (Eq t (suc zero + vz + t'))             -- t>t' := ∃x.t=1+x+t'

-}

-- nulladrendu kombinator logika (Hilbert-fele logika leiras, Moses Schonfinkel)
{-
For : Set
Pf  : For → Set
_⊃_ : For → For → For
_$_ : Pf (A ⊃ B) → Pf A → Pf B
K   : Pf (A ⊃ B ⊃ A)
S   : Pf ((A ⊃ B ⊃ C) ⊃ (A ⊃ B) ⊃ (A ⊃ C))
_∨_ : For → For → For
inl : Pf A → Pf (A ∨ B)
inr : Pf B → Pf (A ∨ B)
case : Pf (A ∨ B) → Pf (A ⊃ C) → Pf (B ⊃ C) → Pf C
-}

-- nezzuk meg, hogy elsorendu formulakat hogyan tudunk kombinatorokkal leirni


data Ty : Set where
  For : Ty
  _⇒_ : Ty → Ty → Ty
  Nat : Ty
infixr 2 _⇒_
infixl 5 _$_

data Exp : Ty → Set where
  ⊤ ⊥   : Exp For
  ⊃ ∧ ∨ : Exp (For ⇒ For ⇒ For)
  >     : Exp (Nat ⇒ Nat ⇒ For)
  const : ℕ → Exp Nat
  +t   : Exp (Nat ⇒ Nat ⇒ Nat)
  K     : ∀{A B} → Exp (A ⇒ B ⇒ A)
  S     : ∀{A B C} → Exp ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ (A ⇒ C))
  ∀'    : Exp (Nat ⇒ For) → Exp For
  _$_   : ∀{A B} → Exp (A ⇒ B) → Exp A → Exp B
  -- K $ a $ b = a
  -- S $ f $ ab $ a = f $ a $ (ab $ a)

C : ∀{A B C} → Exp ((B ⇒ C) ⇒ (A ⇒ B) ⇒ (A ⇒ C))
C = S $ (K $ S) $ K
-- λf.λg.λa.f(g a) = λf.λg.S(λa.f)(λa.g a) = λf.λg.S(Kf)g = λf.S(Kf) = S(λf.S)(λf.Kf) = S(KS)K

∀x1>x ∀xx+1>0 : Exp For
∀x1>x   = ∀' (> $ (const 1))
∀xx>1   = ∀' (S $ > $ (K $ const 1)) -- > $ x > const 1
       -- ∀' (λ x → _>_ x (const 1))
       -- S $ > $ (K $ const 1) $ x = > $ x $ (K $ const 1 $ x) = > $ x $ const 1
∀xx+1>0 = ∀' (C $ (S $ > $ (K $ const 0)) $ (S $ +t $ (K $ const 1)))
-- S $ > $ (K $ const 0)   = (λx.x>0)
-- S $ +t $ (K $ const 1)  = (λx.x+1)
--                           (λx.x>0)∘(λx.x+1) = (λx.x+1>0)

⟦_⟧T : Ty → Set₁
⟦ For ⟧T = Set
⟦ A ⇒ B ⟧T = ⟦ A ⟧T → ⟦ B ⟧T
⟦ Nat ⟧T = Lift ℕ

_>?_ : ℕ → ℕ → Set
zero >? zero = Empty
zero >? suc n = Empty
suc m >? zero = Unit
suc m >? suc n = m >? n

⟦_⟧E : ∀{A : Ty} → Exp A → ⟦ A ⟧T
⟦ ⊤ ⟧E = Unit
⟦ ⊥ ⟧E = Empty
⟦ ⊃ ⟧E = λ X Y → X → Y
⟦ ∧ ⟧E = λ X Y → X × Y 
⟦ ∨ ⟧E = λ X Y → X ⊎ Y 
⟦ > ⟧E = λ m n → un m >? un n
⟦ const x ⟧E = mk x
⟦ +t ⟧E = λ m n → mk (un m + un n)
⟦ K ⟧E = λ a b → a
⟦ S ⟧E = λ f ab a → f a (ab a)
⟦ ∀' E ⟧E = (n : ℕ) → ⟦ E ⟧E (mk n)
⟦ E $ E' ⟧E = ⟦ E ⟧E ⟦ E' ⟧E
{-
∀x.1>x   = ∀' (> $ (const 1))         : Exp For
         = ∀' (const 1 > q)           : For ◇
∀x.x>1   = ∀' (S $ > $ (K $ const 1)) : Exp For
         = ∀' (q > const 1)           : For ◇ 
∀x.x+1>0 = ∀' (C $ (S $ > $ (K $ const 0)) $ (S $ +t $ (K $ const 1))) : Exp Fo
         = ∀' (x + const 1 > const 0)
-}

data Con  : Set
data Sub  : Con → Con → Set

variable Δ Γ Θ : Con

data Con where
  ◇    : Con
  _▹   : Con → Con

data Tm : Con → Set

data Sub where
  _∘_  : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  id   : Sub Γ Γ
  ε    : Sub Γ ◇
  _,_  : Sub Δ Γ → Tm Δ → Sub Δ (Γ ▹)
  p    : Sub (Γ ▹) Γ

data Tm where
  _[_] : Tm Γ → Sub Δ Γ → Tm Δ
  q    : Tm (Γ ▹)
  _+'_ : Tm Γ → Tm Γ → Tm Γ
  _-'_ : Tm Γ → Tm Γ → Tm Γ
  abs  : Tm Γ → Tm Γ
  const : ℕ → Tm Γ

data For' : Con → Set where
  _[_] : For' Γ → Sub Δ Γ → For' Δ
  ∀'   : For' (Γ ▹) → For' Γ
  ∃'   : For' (Γ ▹) → For' Γ
  _>_  : Tm Γ → Tm Γ → For' Γ
  _⊃_  : For' Γ → For' Γ → For' Γ
  _∧_  : For' Γ → For' Γ → For' Γ
  Eq   : Tm Γ → Tm Γ → For' Γ

infix 5 _>_
infixr 4 _⊃_

postulate
  f : For' (◇ ▹ ▹)
  u : Tm ◇
{-
f-nek a hatarerteke u
  u : Tm ◇
  f : For (◇ ▹ ▹) -- relacio
  ∀n.∃m.f(n,m)     ∀' (∃' f[ε,q[p],q]) : For ◇
  ∀x.∃y.f(x,y)        ^ : For (◇ ▹)
                          ^ : For (◇ ▹ ▹)
  ∀n∃m,m'.f(n,m)∧f(n,m')⊃Eq(m,m')
                   ∀' (∃' (∃' (f[p]∧f[ε,v2,v0]⊃Eq v1 v0)))
                              ^ For (◇▹▹▹)
  ∀x.∃n.∀m.m>n⊃abs(f(m)-u)<x
  ∀x.∃n.∀m.m>n⊃∃k.f(m,k)∧abs(k-u)<x ===
            ∀'(∃'(∀'(q>q[p] ⊃ ∃(f[ε,q[p],q] ∧ abs (q - u[ε]) < q[p][p][p])

  m = q
  f(m) = ?

  ∀m.f(m)=0 === ∀m.f(m,0)
  ∀m.Eq(f(m)-1,0) === ∀m.∃k.f(m,k)∧Eq(k-1,0) ===
    ∀' (∃' f ∧ Eq (q - const 1) (const 0))

  Eq(f(0),0) ==== f(0,0)
  f(0)

  t : Tm ◇
  t[ε] : Tm Γ
  _,_ : Sub Δ Γ → Tm Δ → Sub Δ (Γ ▹)
  f : For (◇▹▹)
  f[?] : For (◇▹▹▹)
  ?    : Sub (◇▹▹▹) (◇▹▹)
  (ε, q[p][p] + q[p], abs q)    : Sub (◇▹▹▹) (◇▹▹)
  (a↦x+y,b↦abs(z)) : Sub (x,y,z) (a,b)
  (ε,v1+v0,const 1) : Sub (Γ▹▹) (◇▹▹)
  q[(ε,v1+v0,const 1)] = const 1
-}

FFFF : For' ◇
FFFF = ∀' (∃' (∀' ((q For'.> (q [ p ])) For'.⊃ (∃' (((f [ (ε , (q [ p ])) , q ]) For'.∧ ((q [ p ] [ p ] [ p ]) For'.> abs (q -' (u [ ε ])) )))))))

-- TODO: write an interpreter for For' ⟦_⟧
