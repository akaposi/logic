Az elsőrendű logika, mint algebrai struktúra
--------------------------------------------

A természetes levezetés kalkulusát írjuk le algebrai módon, categories
with families (CwF)-el. A természetes levezetésnek az az előnye (pl. a
Hilbert-féle kalkulusokhoz képest), hogy a változókat úgy használjuk,
mint papíron. Az informális leíráshoz képest az alábbiakban két fő
különbség lesz: a helyettesítések explicitek, valamint változónevek
helyett természetes számokat (De Bruijn indexeket) használunk.

Először is, mi a nulladrendű logika egy algebrája (modellje)? A
szintaxis az üres halmaz fölötti szabad algebra. Utána kiegészítjük
majd elsőrendű logikává.

Van egy kategóriánk (gráfunk), az objektumokat Con-nal (környezet), a
morfizmusokat Sub-bal (helyettesítés) jelöljük. Az algebra egyes
komponenseit |-vel kezdődő sorokkal jelöljük.

| Con  : Set
| Sub  : Con → Con → Set
| _∘_  : Sub Δ Γ → Sub Ξ Δ → Sub Ξ Γ
| ass  : (γ∘δ)∘ξ = γ∘(δ∘ξ)
| id   : Sub Γ Γ
| idl  : id∘γ = γ
| idr  : γ∘id = γ

Ha majd bevezettünk további műveleteket és egyenlőségeket, világos
lesz, hogy a szintaxisban az objektumok környezetek (formula-listák,
Γ,Δ-val jelöljük őket), a morfizmusok helyettesítések
(bizonyítás-listák). Például az (A,B,C)-t tartalmazó három hosszú
környezetből van egy helyettesítés a (A∧B, C∨A)-t tartalmazó kettő
hosszú környezetbe, hiszen (A,B,C)-t feltételezve be tudjuk majd
bizonyítani A∧B-t, és (A,B,C)-t feltételezve be tudjuk majd
bizonyítani C∨A-t is. A másik irányban, (A∧B, C∨A)-ből (A,B,C)-be nem
lesz helyettesítés a szintaxisban.

A kategóriában van egy terminális objektum (üres környezet ◆), üres
helyettesítés (ε).

| ◆ : Con
| ε : Sub Γ ◆
| ◆η : (σ:Sub Γ ◆) → σ = ε

(Az eddig bevezetett fajtákhoz, műveletekhez, egyenlőségekhez tartozó
szintaxis a csak egy objektumú (◆) kategória, amiben egy darab
morfizmus (id=ε) van ◆-ból ◆-ba.)

Minden objektumhoz tartozik formulák egy halmaza, melyhez tartozik egy
_[_] helyettesítés alkalmazása művelet (röviden: For és _[_] egy
funktor a kategóriánkból a halmazok kategóriájába).

| For  : Con → Set
| _[_] : For Γ → Sub Δ Γ → For Δ
| [∘]  : A[γ∘δ] = A[γ][δ]
| [id] : A[id] = A

(A formulákat nulladrendű logikához elég lett volna egy Con-tól nem
függő halmazként megadni. Azért engedjük meg a környezettől való
függést, hogy majd működjünk elsőrendű logikára is.)

A bizonyítások egy reláció Con és For között, ezeket is lehet
helyettesíteni, és bármely két bizonyítás, ami ugyanabban a
környezetben ugyanazt bizonyítja, egyenlő (irrelevancia).

| Pf   : (Γ:Con) → For Γ → Set
| _[_] : Pf Γ A → (γ:Sub Δ Γ) → Pf Δ (A[γ])
| irr  : (a a' : Pf Γ A) → a = a'

A környezeteket ki tudjuk egészíteni bizonyítás-változókkal.

| _▹_ : (Γ:Con) → For Γ → Con

A helyettesítéseket ki tudjuk egészíteni bizonyításokkal, és így a
helyettesítések a szintaxisban tényleg bizonyítás-listává válnak.

| _,_ : (γ:Sub Δ Γ) → Pf Δ (A[γ]) → Sub Δ (Γ▹A)
| p   : Sub (Γ▹A) Γ
| q   : Pf  (Γ▹A) (A[p])
| ▹β  : p∘(γ,a) = γ
| ▹η  : (p∘γa,q[γa]) = γa

A p és q műveletekkel kapunk bizonyítás-változókat. q-t a nulla De
Bruijn indexnek nevezzük, a p-vel való helyettesítés (_[p] : Pf Γ A →
Pf (Γ▹B) (A[p])) eggyel megnöveli az indexet. Például a 3 De Bruin
index:

  Pf (Γ▹A▹B▹C▹D) (A[p][p][p][p]) : q[p][p][p]

Ez egy bizonyítás, ami azt mondja, hogy abban a környezetben, ami
Γ-val kezdődik, utána benne van (ebben a sorrendben) A, B, C, D, abban
A[p][p][p][p] bebizonyítható. A-t azért kell négyszer helyettesíteni
p-vel (négyszer gyengíteni), mert A önmagában értelmes a Γ
környezetben, itt viszont még a Γ után négy dolog szerepel, és ezeket
el kell felejteni, hogy megkapjuk A környezetét.

Az eddigi 4 fajta (Con, Sub, For, Pf), 10 művelet (_∘_, id, ◆, ε,
_[_], _[_], _▹_, _,_, p, q) és 9 egyenlőség (ass, ..., ▹η) összességét
helyettesítési kalulusnak nevezzük. Egy ilyen algebra nem más, mint
egy olyan CwF, melyben bármely két Pf Γ A egyenlő.

Most hozzáadjuk a különböző logikai összekötőket. Igaz:

| ⊤   : For Γ
| ⊤[] : ⊤[γ] = ⊤
| tt  : Pf Γ ⊤

Hamis:

| ⊥       : For Γ
| ⊥[]     : ⊥[γ] = ⊥
| exfalso : Pf Γ ⊥ → Pf Γ A

Konjunkció:

| _∧_  : For Γ → For Γ → For Γ
| ∧[]  : (A∧B)[γ] = (A[γ])∧(B[γ])
| _,_  : Pf Γ A → Pf Γ B → Pf Γ (A∧B)
| fst  : Pf Γ (A∧B) → Pf Γ A
| snd  : Pf Γ (A∧B) → Pf Γ B

Diszjunkció:

| _∨_  : For Γ → For Γ → For Γ
| ∨[]  : (A∨B)[γ] = (A[γ])∨(B[γ])
| inl  : Pf Γ A → Pf Γ (A∨B)
| inr  : Pf Γ B → Pf Γ (A∨B)
| case : Pf (Γ▹A) C → Pf (Γ▹B) C → Pf Γ (A∨B) → Pf Γ C

Implikáció:

| _⇒_  : For Γ → For Γ → For Γ
| ⇒[]  : (A⇒B)[γ] = (A[γ])⇒(B[γ])
| lam  : Pf (Γ▹A) B → Pf Γ (A⇒B)
| _$_  : Pf Γ (A⇒B) → Pf Γ A → Pf Γ B

_$_-t modus ponens-nek is szokás nevezni.

Ezzel felsoroltuk a nulladrendű logika algebrájának összes komponensét
(4 fajta, 25 művelet, 14 egyenlőség). Ha egy algebrában teljesülnek az
alábbi egyenlőségek, akkor az egy Heyting-algebra (és minden Heyting
algebra automatikusan egy nulladrendű logika algebra):

  Con = For Γ
  Sub Δ Γ = Pf Δ A
  ◆ = ⊤
  Γ▹A = Γ∧A
  Sub Γ Δ → Sub Δ Γ → Γ = Δ

Ha ezen túl még teljesül az alábbi egyenlőség, akkor az egy Boolean-algebra.

  (A : Ty Γ) → Pf Γ (A∨¬A)

Most jön az elsőrendű logika. Hozzáveszünk egy új fajtát az algebrai
struktúránkhoz, a termek fajtáját. A termeket is lehet helyettesíteni,
és lehetnek term-változóik a környezetben.

| Tm   : Con → Set
| _[_] : Tm Γ → Sub Δ Γ → Tm Δ
| [∘]  : t[γ∘δ] = t[γ][δ]
| [id] : t[id] = t
| _▸   : Con → Con

A helyettesítések tartalmazhatnak termeket is (a _,_, p, q műveleteket
túlterheljük).

| _,_  : Sub Δ Γ → Tm Δ → Sub Δ (Γ▸)
| p    : Sub (Γ▸) Γ
| q    : Tm (Γ▸)
| ▸β₁  : p∘(γ,t) = p
| ▸β₂  : q[γ,t] = t
| ▸η   : (p∘γt,q[γt]) = γt

Most már egy környezetben nem csak bizonyítás-változók vannak, hanem
term-változóink is. Például a (◆▸▹A▸) környezetben van két
term-változó (0 és 2 De Bruijn indexekkel) és egy bizonyítás-változó
(mely A-t bizonyítja, 1-es De Bruijn index-szel).

  q          : Tm (◆▸▹A▸)
  q[p]       : Tm (◆▸▹A▸)
  q[p][p]    : Pf (◆▸▹A▸) (A[p][p])

Bevezetjük a minden és létezik kvantorokat.

| ∀   : For (Γ▸) → For Γ
| ∀[] : (∀A)[γ] = ∀(A[γ∘p,q])
| mk∀ : Pf (Γ▸) A → Pf Γ (∀A)
| un∀ : Pf Γ (∀A) → (t:Tm Γ) → Pf (A[id,t])

| ∃   : For (Γ▸) → For Γ
| ∃[] : (∃A)[γ] = ∃(A[γ∘p,q])
| mk∃ : (t:Tm Γ) → Pf Γ (A[id,t]) → Pf Γ (∃A)
| un∃ : (a:Pf Γ (∃A)) → Pf (Γ▸▹A) (C[p]) → Pf Γ C

Egy elsőrendű szignatúra megadja, hogy az adott aritású
függvényszimbólumok illetve relációszimbólumok halmazát. Például funar
2 a 2-paraméteres függvényszimbólumok halmaza.

funar : ℕ → Set
relar : ℕ → Set

A szignatúrához tartozó függvények és relációk:

| fun : (n : ℕ) → funar n → Tm (◆▸^n)
| rel : (n : ℕ) → relar n → For (◆▸^n)

Itt ◆▸^n a _▸ művelet n-szeres iterációját jelenti, például ◆▸^0 = ◆,
◆▸^1 = ◆▸, ◆▸^4 = ◆▸▸▸▸.

Összefoglalás: az elsőrendű logika funar-al és relar-ral van
paraméterezve, és ha ezek meg vannak adva, akkor meg tudjuk mondani,
hogy mi egy algebra. Egy algebra 5 halmazból/halmazcsaládból áll (Con,
Sub, For, Pf, Tm), ezeken 38 függvény van megadva (_∘_, id, ◆, ε,
_[_], _[_], _▹_, _,_, p, q, ⊤, tt, ⊥, exfalso, ∧, _,_, fst, snd, _∨_,
inl, inr, case, _⇒_, lam, _$_, _[_], _▸, _,_, p, q, ∀, mk∀, un∀, ∃,
mk∃, un∃, fun, rel), és ezekre a függvényekre 21 egyenlőség teljesül
(ass, idl, idr, ◆η, [∘], [id], irr, ▹β, ▹η, ⊤[], ⊥[], ∧[], ∨[], ⇒[],
[∘], [id], ▸β₁, ▸β₂, ▸η, ∀[], ∃[]).

Az elsőrendű logika szintaxisa a szabad algebra (iniciális algebra),
melyet úgy is megkonstruálhatunk, hogy induktívan felépítjük az 5
halmazcsaládot a 38 művelet segítségével teljesen szabadon, és ezt a
21 egyenlőséggel kvóciensezzük (nincs szükség ezt kézzel megcsinálni,
mert ismert, hogy tetszőleges általánosított algebrai struktúrának van
szintaxisa ebben az értelemben, lásd pl. Kaposi, Kovács,
Altenkirch. Constructing Quotient Inductive-Inductive Types. POPL
2019).

A szintaxisban Γ : Con egy környezet, mely bizonyításváltozók típusait
és termváltozókat sorol fel. Minden változót egy természetes számmal
(De Bruijn index-szel) azonosítunk. Ty Γ azon formulák halmaza, mely a
Γ-ban levő term-változókra hivatkozhat (bár Γ-ban bizonyításváltozók
is vannak, ezekre nem hivatkoznak a formulák). Pf Γ A-nak elemei
levezetési fák, melyek A-t vezetik le Γ környezetben. Bármely két
ilyen levezetési fát egyenlőnek tekintünk (irr). Egy szintaktikus A :
For ◆ zárt formulát tételnek nevezünk, ha létezik bizonyítása, tehát
egy a : Pf ◆ A levezetési fa a szintaxisban.

Tetszőleges algebrába pontosan egy homomorfizmus megy a szintaxisból,
ez a kiértékelés (értelmezés, interpretáció), ⟦_⟧-el jelöljük. Ez a
szintaxis helyessége. Egy algebra-család teljes a szintaxisra nézve,
ha bármely szintaktikus Γ : Con környezete és A : For Γ formulára, ha
minden algebrában Pf ⟦Γ⟧ ⟦A⟧-nak van eleme, akkor a szintaxisban Pf Γ
A-nak is van eleme.
