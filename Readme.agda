{-# OPTIONS --prop #-}

module Readme where

-- the negative fragment

import Minimal    -- ⊤, _⇒_
import Negative   -- propvars, _⇒_, ⊤, _∧_
import FirstOrder -- relations, _⇒_, ⊤, _∧_, ∀ (one sort)
import Everything -- relations, _⇒_, ⊤, _∧_, _∨_, ∀ (one sort)

import Classical

import SumsKripke -- ⊤, _⇒_, _∨_ (Kripke, noLEM)
import Sums       -- ⊤, _⇒_, _∨_ (Beth model)
import SumsCheat  -- ⊤, _⇒_, _∨_ (gluing)

{-
references:
Thorsten talk: https://www.cs.nott.ac.uk/~psztxa/talks/nbe09.pdf
Palmgren: Constructive sheaf semantics: https://www.princeton.edu/~hhalvors/teaching/phi323_f2009/palmgren.pdf
Coquand-Mannaa-Ruch: Stack semantics of type theory : https://arxiv.org/pdf/1701.02571.pdf
Schwichtenberg: Mathematical logic: http://www.mathematik.uni-muenchen.de/~schwicht/lectures/logic/ss10/ml.pdf
Gilbert-Hermant: Normalisation by completeness with Heyting algebras: http://www.cri.mines-paristech.fr/classement/doc/A-614.pdf https://github.com/SkySkimmer/NormalisationByCompleteness/blob/master/nj_strong_complete.v
  "strong completeness" produces normal derivations, this is what they prove
  they only have weak sums in their normal forms
  so the usual normalisation proof for STT with Kripke logical predicates (where sums are interpreted noncanonically) works -- but I guess this is something different
  they say that Heyting is enough for completeness of the "cut free system"
Altenkirch-Dybjer-Hofmann-Scott: NBE for typed lambda calculus with coproducts: http://www.cs.nott.ac.uk/~psztxa/publ/lics01.pdf
  here they have proper normal forms, a complicated definition
Scherer: Deciding equivalence with sums and the empty type https://arxiv.org/pdf/1610.01213.pdf
  this also has proper sums
  doesn't talk about Grothendieck topologies but saturation
Awodey cat theory book proves completeness for Heyting algebras using the Lindenbaum-Tarski algebra (cheating)
Pierre-Marie: https://www.xn--pdrot-bsa.fr/drafts/sheaftt.pdf
-}

{-
TODO:


-}
