{-# OPTIONS --prop #-}

module ListAlg where

open import Lib

record ListAlg {i k} (T : Set k) : Set (lsuc (i ⊔ k))  where
  field
    Lₐ : Set i
    nₐ : Lₐ
    cₐ : T -> Lₐ -> Lₐ
open ListAlg public


record ListMor {i j k} {T : Set k}
       (A : ListAlg {i} T)(B : ListAlg {j} T) : Set (i ⊔ j ⊔ k) where
  field
    Lₘ : Lₐ A -> Lₐ B
    nₘ : Lₘ (nₐ A) ≡ nₐ B
    cₘ : {x : T}{xs : Lₐ A} -> Lₘ (cₐ A x xs) ≡ cₐ B x (Lₘ xs)
open ListMor public


record ListDis {i j k} {T : Set k}
       (A : ListAlg {i} T) : Set (lsuc (i ⊔ j ⊔ k)) where
  field
    Lₚ : Lₐ A -> Set j
    nₚ : Lₚ (nₐ A)
    cₚ : {x : T}{xs : Lₐ A} -> Lₚ xs -> Lₚ (cₐ A x xs)
open ListDis public


record ListSec {i j k} {T : Set k}
       {A : ListAlg {i} T}(D : ListDis {i} {j} A) : Set (i ⊔ j ⊔ k) where
  field
    Lₛ : (xs : Lₐ A) -> Lₚ D xs
    nₛ : Lₛ (nₐ A) ≡ nₚ D
    cₛ : {x : T}{xs : Lₐ A} -> Lₛ (cₐ A x xs) ≡ cₚ D (Lₛ xs)
open ListSec public

postulate
  I : ∀ {i k} {T : Set k} -> ListAlg {i} T
  rec : ∀ {i j k} {T : Set k} (A : ListAlg {j} T) -> ListMor {i} I A
  ind : ∀ {i j k} {T : Set k} (D : ListDis {i} {j} I) -> ListSec {T = T} D


List : ∀ {i k} (T : Set k) -> Set i
List T = Lₐ {T = T} I

[] : ∀ {i k} {T : Set k} -> List {i} T
[] = nₐ I

infixr 20 _::_
_::_ : ∀ {i k} {T : Set k} -> T -> List {i} T -> List {i} T
_::_  = cₐ I

-- concatenation of two lists

Concat : ∀ {k} {T : Set k} -> ListAlg {lzero} T
Concat {T = T} = record
  { Lₐ = List T -> List T
  ; nₐ = id
  ; cₐ = λ x -> _∘_ (_::_ x)
  }

concat : ∀ {k} {T : Set k} -> ListMor {lzero} {T = T} I Concat
concat = rec Concat

infixl 10 _++_
_++_ : ∀ {k} {T : Set k} -> List T -> List T -> List T
_++_ = Lₘ concat

-- properties of concatenation

-- zero is left identity

IdlConcat : ∀ {k} {T : Set k} -> ListDis {T = T} I
IdlConcat = record
  { Lₚ = λ xs -> ↑p ([] ++ xs ≡ xs)
  ; nₚ = ↑[ eta (nₘ concat) ]↑
  ; cₚ = const $ ↑[ eta (nₘ concat) ]↑
  }

idl-concat : ∀ {k} {T : Set k}{xs : List T} -> [] ++ xs ≡ xs
idl-concat {xs = xs} = ↓[ Lₛ (ind IdlConcat) xs ]↓

-- zero is right identity

IdrConcat : ∀ {k} {T : Set k} -> ListDis {T = T} I
IdrConcat = record
  { Lₚ = λ xs -> ↑p (xs ++ [] ≡ xs)
  ; nₚ = ↑[ eta (nₘ concat) ]↑
  ; cₚ = λ h -> ↑[ eta (cₘ concat)
                 ◾ ap (_::_ _) ↓[ h ]↓ ]↑
  }

idr-concat : ∀ {k} {T : Set k}{xs : List T} -> xs ++ [] ≡ xs
idr-concat {xs = xs} = ↓[ Lₛ (ind IdrConcat) xs ]↓

-- addition is associative

AssConcat : ∀ {k} {T : Set k} -> ListDis {T = T} I
AssConcat {T = T} = record
  { Lₚ = λ xs → (ys zs : List T) -> ↑p ((xs ++ ys) ++ zs ≡ xs ++ (ys ++ zs))
  ; nₚ = λ ys zs → ↑[ ap (flip _++_ zs) idl-concat ◾ idl-concat ⁻¹ ]↑
  ; cₚ = λ h ys zs → ↑[ ap (flip _++_ zs) (eta (cₘ concat))
                      ◾ eta (cₘ concat)
                      ◾ ap (_::_ _) ↓[ h _ _ ]↓
                      ◾ eta (cₘ concat) ⁻¹ ]↑
  }

ass-concat : ∀ {k} {T : Set k} -> (xs ys zs : List T) ->
             (xs ++ ys) ++ zs ≡ xs ++ (ys ++ zs)
ass-concat xs ys zs = ↓[ Lₛ (ind AssConcat) xs ys zs ]↓

