module NatAlg0 where

data Nat : Set where
  zero : Nat
  suc : Nat -> Nat
{-# BUILTIN NATURAL Nat #-}

infix 10 _≡_
data _≡_ {i}{A : Set i}(x : A) : A -> Set i where
  refl : x ≡ x

transport : ∀ {i j}{A : Set i}{P : A -> Set j}{x y : A} -> x ≡ y -> P x -> P y
transport refl p = p

sym : ∀ {i}{A : Set i}{x y : A} -> x ≡ y -> y ≡ x
sym refl = refl

trans : ∀ {i}{A : Set i}{x y z : A} -> x ≡ y -> y ≡ z -> x ≡ z
trans refl refl = refl

cong : ∀ {i j}{A : Set i}{B : Set j}(f : A -> B){x y : A} -> x ≡ y -> f x ≡ f y
cong f refl = refl

eta : ∀ {i j}{A : Set i}{B : Set j}{f g : A -> B} -> f ≡ g -> {x : A} -> f x ≡ g x
eta refl = refl

id : ∀ {i}{A : Set i} -> A -> A
id x = x

flip : ∀ {i j k}{A : Set i}{B : Set j}{C : Set k} -> (A -> B -> C) -> B -> A -> C
flip f x y = f y x

_∘_ : ∀ {i j k}{A : Set i}{B : A -> Set j}{C : (x : A) -> B x -> Set k}
         (f : {x : A}(y : B x) -> C x y)(g : (x : A) -> B x) -> (x : A) -> C x (g x)
(f ∘ g) x = f (g x)

record NatAlg : Set₁ where
  field
    Nₐ : Set
    zₐ : Nₐ
    sₐ : Nₐ -> Nₐ
open NatAlg

record NatMor (A B : NatAlg) : Set where
  field
    Nₘ : Nₐ A -> Nₐ B
    zₘ : Nₘ (zₐ A) ≡ zₐ B
    sₘ : {n : Nₐ A} -> Nₘ (sₐ A n) ≡ sₐ B (Nₘ n)
open NatMor

N : NatAlg
N = record
  {
    Nₐ = Nat ;
    zₐ = zero ;
    sₐ = suc
  }

postulate
  rec : (A : NatAlg) -> NatMor N A

idm : NatMor N N
idm = rec N

lem-idm : (n : Nat) -> Nₘ idm n ≡ n
lem-idm zero = zₘ idm
lem-idm (suc n) = trans (sₘ idm) (cong suc (lem-idm n))

S : NatAlg
S = record
  {
    Nₐ = Nat ;
    zₐ = suc zero ;
    sₐ = suc
  }

succ : NatMor N S
succ = rec S

lem-succ : (n : Nat) -> Nₘ succ n ≡ suc n
lem-succ zero = zₘ succ
lem-succ (suc n) = trans (sₘ succ) (cong suc (lem-succ n))

D : NatAlg
D = record
  {
    Nₐ = Nat ;
    zₐ = zero ;
    sₐ = suc ∘ suc
  }

double : NatMor N D
double = rec D

infix 40 2*_
2*_ : Nat -> Nat
2* zero = zero
2* (suc n) = suc (suc (2* n))
 
lem-double : (n : Nat) -> Nₘ double n ≡ 2* n
lem-double zero = zₘ double
lem-double (suc n) = trans (sₘ double) (cong (suc ∘ suc) (lem-double n))

Sum : NatAlg
Sum = record
  {
    Nₐ = Nat -> Nat ;
    zₐ = id ;
    sₐ = _∘_ suc
  }

plus : NatMor N Sum
plus = rec Sum

infixl 40 _+_
_+_ : Nat -> Nat -> Nat
_+_ = Nₘ plus

idl-plus : {n : Nat} -> zero + n ≡ n
idl-plus {n} = eta (zₘ plus)

idr-plus : {n : Nat} -> n + zero ≡ n
idr-plus {zero} = eta (zₘ plus)
idr-plus {suc n} = trans (eta (sₘ plus)) (cong suc idr-plus)

ass-plus : (x y z : Nat) -> (x + y) + z ≡ x + (y + z)
ass-plus zero y z = trans (cong (flip (_+_) z) idl-plus) (sym idl-plus)
ass-plus (suc x) y z = trans (cong (flip (_+_) z) (eta (sₘ plus))) (trans (eta (sₘ plus)) (trans (cong suc (ass-plus x y z)) (sym (eta (sₘ plus)))))
