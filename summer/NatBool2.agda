{-# OPTIONS --prop --rewriting #-}

module NatBool2 where

open import Lib
{-# BUILTIN REWRITE _≡ₚ_ #-}

record NatBoolAlg {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty : Set i
    Tm : Ty -> Set j

    Bool : Ty
    Nat : Ty

    true : Tm Bool
    false : Tm Bool
    zero : Tm Nat
    suc : Tm Nat -> Tm Nat
    isZero : Tm Nat -> Tm Bool
    ite : {A : Ty} -> Tm Bool -> Tm A -> Tm A -> Tm A
    
    ite-t : {A : Ty}{u v : Tm A} -> ite true u v ≡ₚ u
    ite-f : {A : Ty}{u v : Tm A} -> ite false u v ≡ₚ v
    isZero-z : isZero zero ≡ₚ true
    isZero-s : {n : Tm Nat} -> isZero (suc n) ≡ₚ false

module I where
  data Ty : Set where
    Bool Nat : Ty

  postulate
    Tm : Ty -> Set

    true : Tm Bool
    false : Tm Bool
    zero : Tm Nat
    suc : Tm Nat -> Tm Nat
    isZero : Tm Nat -> Tm Bool
    ite : {A : Ty} -> Tm Bool -> Tm A -> Tm A -> Tm A
    
    ite-t : {A : Ty}{u v : Tm A} -> ite true u v ≡ₚ u
    ite-f : {A : Ty}{u v : Tm A} -> ite false u v ≡ₚ v
    isZero-z : isZero zero ≡ₚ true
    isZero-s : {n : Tm Nat} -> isZero (suc n) ≡ₚ false

  I : NatBoolAlg {lzero}{lzero}
  
  I = record { Ty = Ty ; Tm = Tm ; Bool = Bool ; Nat = Nat ; true =
    true ; false = false ; zero = zero ; suc = suc ; isZero = isZero ;
    ite = ite ; ite-t = ite-t ; ite-f = ite-f ; isZero-z = isZero-z ;
    isZero-s = isZero-s }

--  {-# REWRITE ite-t ite-f isZero-z isZero-s #-}

module NatBoolRec {i j} (A : NatBoolAlg {i}{j}) where
  private
    module A = NatBoolAlg A

  postulate
    ⟦_⟧T : I.Ty -> A.Ty
    ⟦_⟧t : ∀ {T} -> I.Tm T -> A.Tm ⟦ T ⟧T

    ⟦Bool⟧ : ⟦ I.Bool ⟧T ≡ₚ A.Bool
    ⟦Nat⟧ : ⟦ I.Nat ⟧T ≡ₚ A.Nat
    {-# REWRITE ⟦Bool⟧ ⟦Nat⟧ #-}
  
    ⟦true⟧ : ⟦ I.true ⟧t ≡ₚ A.true
    ⟦false⟧ : ⟦ I.false ⟧t ≡ₚ A.false
    ⟦zero⟧ :  ⟦ I.zero ⟧t ≡ₚ A.zero
    ⟦suc⟧ : ∀ {n} -> ⟦ I.suc n ⟧t ≡ₚ A.suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {n} -> ⟦ I.isZero n ⟧t ≡ₚ A.isZero ⟦ n ⟧t
    ⟦ite⟧ : ∀ {T} {b : I.Tm I.Bool}{u v : I.Tm T} ->
              ⟦ I.ite b u v ⟧t ≡ₚ A.ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦ite⟧ #-}

St : NatBoolAlg
St = record
       { Ty = Set
       ; Tm = id
       ; Bool = ⇅
       ; Nat = ℕ
       ; true = ↑
       ; false = ↓
       ; zero = zeroℕ
       ; suc = sucℕ
       ; isZero = isZero
       ; ite = if_then_else_
       ; ite-t = refl
       ; ite-f = refl
       ; isZero-z = refl
       ; isZero-s = refl
       }
       where
         isZero : ℕ -> ⇅
         isZero zeroℕ = ↑
         isZero (sucℕ n) = ↓

⌜_⌝B : ⇅ -> I.Tm I.Bool
⌜ ↑ ⌝B = I.true
⌜ ↓ ⌝B = I.false

⌜_⌝N : ℕ -> I.Tm I.Nat
⌜ zeroℕ ⌝N = I.zero
⌜ sucℕ n ⌝N = I.suc ⌜ n ⌝N

open NatBoolRec St

⌜_⌝ : ∀ {A} -> ⟦ A ⟧T -> I.Tm A
⌜_⌝ {I.Bool} = ⌜_⌝B
⌜_⌝ {I.Nat} = ⌜_⌝N

stability : ∀ {A} {x : ⟦ A ⟧T} -> _≡ₚ_ {_}{⟦ A ⟧T} ⟦ ⌜_⌝ {A} x ⟧t x
stability {I.Bool} {↑} = refl
stability {I.Bool} {↓} = refl
stability {I.Nat} {zeroℕ} = refl
stability {I.Nat} {sucℕ n} = apₚ sucℕ (stability {I.Nat}{n})

a : ⊤
a = {!isZero zero!}
  where
    open NatBoolAlg I.I
