{-# OPTIONS --prop #-}

module NBF where

open import Lib

record NBFAlg {i j k l}{Con : Set i}{STy Ty : Set j}
       {∙ : Con}{_▹_ : Con -> Ty -> Con}
       {Bool Nat : STy}{sty : STy -> Ty}{_⇒_ : STy -> Ty -> Ty}
       : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixr 4 _∘ₛ_
  infixl 3 _,ₛ_
  infixl 6 _[_]
  infixl 5 _$'_
  
  field
    Sub : Con -> Con -> Set k
    Tm : Con -> Ty -> Set l

    _∘ₛ_ : ∀ {Γ Δ Θ} -> Sub Δ Θ -> Sub Γ Δ -> Sub Γ Θ
    idₛ : ∀ {Γ} -> Sub Γ Γ
    ε : ∀ {Γ} -> Sub Γ ∙
    p : ∀ {Γ A} -> Sub (Γ ▹ A) Γ
    _,ₛ_ : ∀ {Γ Δ A} -> Sub Γ Δ -> Tm Γ A -> Sub Γ (Δ ▹ A)

    q : ∀ {Γ A} -> Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} -> Tm Δ A -> Sub Γ Δ -> Tm Γ A
    lam : ∀ {Γ A B} -> Tm (Γ ▹ sty A) B -> Tm Γ (A ⇒ B)
    _$'_ : ∀ {Γ A B} -> Tm Γ (A ⇒ B) -> Tm Γ (sty A) -> Tm Γ B
    
    true : ∀ {Γ} -> Tm Γ (sty Bool)
    false : ∀ {Γ} -> Tm Γ (sty Bool)
    zero : ∀ {Γ} -> Tm Γ (sty Nat)
    suc : ∀ {Γ} -> Tm Γ (sty Nat) -> Tm Γ (sty Nat)
    isZero : ∀ {Γ} -> Tm Γ (sty Nat) -> Tm Γ (sty Bool)
    ite : ∀ {Γ A} -> Tm Γ (sty Bool) -> Tm Γ A -> Tm Γ A -> Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} ->
          (σ ∘ₛ δ) ∘ₛ ν ≡ σ ∘ₛ (δ ∘ₛ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} -> idₛ ∘ₛ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} -> σ ∘ₛ idₛ ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} -> σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} ->
          p ∘ₛ (σ ,ₛ t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} ->
          q [ σ ,ₛ t ] ≡ t
    ▹η : ∀ {Γ A} -> (p {Γ}{A} ,ₛ q) ≡ idₛ
    ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{δ : Sub Γ Δ}{t : Tm Δ A} ->
         (σ ,ₛ t) ∘ₛ δ ≡ σ ∘ₛ δ ,ₛ t [ δ ]
    [id] : ∀ {Γ A} {t : Tm Γ A} -> t [ idₛ ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{δ : Sub Γ Δ}{t : Tm Θ A} ->
          t [ σ ∘ₛ δ ]  ≡ t [ σ ] [ δ ]

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ sty A) B}{u : Tm Γ (sty A)} ->
         lam t $' u ≡ t [ idₛ ,ₛ u ]
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)}{u : Tm Γ (sty A)} ->
         lam $ (t $' u) [ p ] ≡ t
    lam[] : ∀ {Γ Δ A B} {σ : Sub Γ Δ}{t : Tm (Δ ▹ sty A) B} ->
            (lam t) [ σ ] ≡ lam (t [ σ ∘ₛ p ,ₛ q ])

record NBFMor {i j k₁ l₁ k₂ l₂}{Con : Set i}{STy Ty : Set j}
       {∙ : Con}{_▹_ : Con -> Ty -> Con}
       {Bool Nat : STy}{sty : STy -> Ty}{_⇒_ : STy -> Ty -> Ty}
       (A : NBFAlg {i}{j}{k₁}{l₁}{Con}{STy}{Ty}{∙}{_▹_}{Bool}{Nat}{sty}{_⇒_})
       (B : NBFAlg {i}{j}{k₂}{l₂}{Con}{STy}{Ty}{∙}{_▹_}{Bool}{Nat}{sty}{_⇒_})
       : Set (i ⊔ j ⊔ k₁ ⊔ l₁ ⊔ k₂ ⊔ l₂) where
  private
    module A = NBFAlg A
    module B = NBFAlg B

  field
    Subᴹ : ∀ {Γ Δ} -> A.Sub Γ Δ -> B.Sub Γ Δ
    Tmᴹ : ∀ {Γ T} -> A.Tm Γ T -> B.Tm Γ T

    ∘̂ₛᴹ : ∀ {Γ Δ Θ} {σ : A.Sub Δ Θ}{δ : A.Sub Γ Δ} ->
            Subᴹ (σ A.∘ₛ δ) ≡ Subᴹ σ B.∘ₛ Subᴹ δ
    idₛᴹ : ∀ {Γ} -> Subᴹ (A.idₛ {Γ}) ≡ B.idₛ
    εᴹ : ∀ {Γ} -> Subᴹ (A.ε {Γ}) ≡ B.ε
    pᴹ : ∀ {Γ A} -> Subᴹ (A.p {Γ}{A}) ≡ B.p
    ,ₛᴹ : ∀ {Γ Δ A} {σ : A.Sub Γ Δ}{t : A.Tm Γ A} ->
          Subᴹ (σ A.,ₛ t) ≡ Subᴹ σ B.,ₛ Tmᴹ t

    qᴹ : ∀ {Γ A} -> Tmᴹ (A.q {Γ}{A}) ≡ B.q
    []ᴹ : ∀ {Γ Δ A} {t : A.Tm Δ A}{σ : A.Sub Γ Δ} ->
          Tmᴹ (t A.[ σ ]) ≡ (Tmᴹ t) B.[ Subᴹ σ ]
    lamᴹ : ∀ {Γ A B} {t : A.Tm (Γ ▹ sty A) B} ->
           Tmᴹ (A.lam t) ≡ B.lam (Tmᴹ t)
    $̂'ᴹ : ∀ {Γ A B} {t : A.Tm Γ (A ⇒ B)}{u : A.Tm Γ (sty A)} ->
          Tmᴹ (t A.$' u) ≡ Tmᴹ t B.$' Tmᴹ u

    trueᴹ : ∀ {Γ} -> Tmᴹ (A.true {Γ}) ≡ B.true
    falseᴹ : ∀ {Γ} -> Tmᴹ (A.false {Γ}) ≡ B.false
    zeroᴹ : ∀ {Γ} -> Tmᴹ (A.zero {Γ}) ≡ B.zero
    sucᴹ : ∀ {Γ} {n : A.Tm Γ (sty Nat)} ->
           Tmᴹ (A.suc n) ≡ B.suc (Tmᴹ n)
    isZeroᴹ : ∀ {Γ} {n : A.Tm Γ (sty Nat)} ->
              Tmᴹ (A.isZero n) ≡ B.isZero (Tmᴹ n)
    iteᴹ : ∀ {Γ T} {b : A.Tm Γ (sty Bool)}{u v : A.Tm Γ T} ->
           Tmᴹ (A.ite b u v) ≡ B.ite (Tmᴹ b) (Tmᴹ u) (Tmᴹ v)

data STy : Set where
  Bool : STy
  Nat : STy

data Ty : Set where
  sty : STy -> Ty
  _⇒_ : STy -> Ty -> Ty

data Con : Set where
  ∙ : Con
  _▹_ : Con -> Ty -> Con

infixl 1 _▹_
infixr 2 _⇒_

postulate
  I : ∀ {k l} ->
      NBFAlg {k = k}{l}{Con}{STy}{Ty}{∙}{_▹_}{Bool}{Nat}{sty}{_⇒_}
  rec : ∀ {k₁ l₁ k₂ l₂} (A : NBFAlg) ->
        NBFMor {k₁ = k₁}{l₁}{k₂}{l₂} I A

open NBFAlg (I {lzero}{lzero})

id' : ∀ {Γ A} -> Tm Γ (A ⇒ sty A)
id' = lam q

ff : ∀ {Γ A} -> Tm Γ (A ⇒ sty Bool)
ff = lam false

const' : ∀ {Γ A B} -> Tm Γ (A ⇒ B ⇒ sty A)
const' = lam $ lam $ q [ p ]

not : ∀ {Γ} -> Tm Γ (Bool ⇒ sty Bool)
not = lam (ite q false true)

and : ∀ {Γ} -> Tm Γ (Bool ⇒ Bool ⇒ sty Bool)
and = lam $ lam $ ite (q [ p ]) q false

xor : ∀ {Γ} -> Tm Γ (Bool ⇒ Bool ⇒ sty Bool)
xor = lam $ lam $ ite (q [ p ]) (not $' q) q

flip' : ∀ {Γ A B C} -> Tm Γ (A ⇒ B ⇒ C) -> Tm Γ (B ⇒ A ⇒ C)
flip' f = lam $ lam $ f [ p ] [ p ] $' q $' q [ p ]
