{-# OPTIONS --prop --rewriting #-}

module NatBool where

open import Lib
{-# BUILTIN REWRITE _≡_ #-}

record NatBoolAlg {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty : Set i
    Tm : Ty -> Set j

    Bool : Ty
    Nat : Ty

    true : Tm Bool
    false : Tm Bool
    zero : Tm Nat
    suc : Tm Nat -> Tm Nat
    isZero : Tm Nat -> Tm Bool
    ite : ∀ {A} -> Tm Bool -> Tm A -> Tm A -> Tm A
    
    ite-t : ∀ {A} {u v : Tm A} -> ite true u v ≡ u
    ite-f : ∀ {A} {u v : Tm A} -> ite false u v ≡ v
    isZero-z : isZero zero ≡ true
    isZero-s : ∀ {n} -> isZero (suc n) ≡ false

module I where
  data Ty : Set where
    Bool : Ty
    Nat : Ty
  
  postulate
    Tm : Ty -> Set
    
    true : Tm Bool
    false : Tm Bool
    zero : Tm Nat
    suc : Tm Nat -> Tm Nat
    isZero : Tm Nat -> Tm Bool
    ite : ∀ {A} -> Tm Bool -> Tm A -> Tm A -> Tm A
    
    ite-t : ∀ {A} {u v : Tm A} -> ite true u v ≡ u
    ite-f : ∀ {A} {u v : Tm A} -> ite false u v ≡ v
    isZero-z : isZero zero ≡ true
    isZero-s : ∀ {n} -> isZero (suc n) ≡ false
  
  I : NatBoolAlg
  I = record { Ty = Ty ; Tm = Tm ; Bool = Bool ; Nat = Nat ; true =
    true ; false = false ; zero = zero ; suc = suc ; isZero = isZero ;
    ite = ite ; ite-t = ite-t ; ite-f = ite-f ; isZero-z = isZero-z ;
    isZero-s = isZero-s }
  {-# REWRITE ite-t ite-f isZero-z isZero-s #-}

module NatBoolRec {i j} (A : NatBoolAlg {i}{j}) where
  private
    module A = NatBoolAlg A

  postulate
    ⟦_⟧T : I.Ty -> A.Ty
    ⟦_⟧t : ∀ {T} -> I.Tm T -> A.Tm ⟦ T ⟧T

    ⟦Bool⟧ : ⟦ I.Bool ⟧T ≡ A.Bool
    ⟦Nat⟧ : ⟦ I.Nat ⟧T ≡ A.Nat
    {-# REWRITE ⟦Bool⟧ ⟦Nat⟧ #-}
  
    ⟦true⟧ : ⟦ I.true ⟧t ≡ A.true
    ⟦false⟧ : ⟦ I.false ⟧t ≡ A.false
    ⟦zero⟧ :  ⟦ I.zero ⟧t ≡ A.zero
    ⟦suc⟧ : ∀ {n} -> ⟦ I.suc n ⟧t ≡ A.suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {n} -> ⟦ I.isZero n ⟧t ≡ A.isZero ⟦ n ⟧t
    ⟦ite⟧ : ∀ {T} {b : I.Tm I.Bool}{u v : I.Tm T} ->
              ⟦ I.ite b u v ⟧t ≡ A.ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦ite⟧ #-}

record NatBoolDis {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty : I.Ty -> Set i
    Tm : ∀ {T} -> Ty T -> I.Tm T -> Set j
    
    Bool : Ty I.Bool
    Nat : Ty I.Nat

    true : Tm Bool I.true
    false : Tm Bool I.false
    zero : Tm Nat I.zero
    suc : ∀ {n} -> Tm Nat n -> Tm Nat (I.suc n)
    isZero : ∀ {n} -> Tm Nat n -> Tm Bool (I.isZero n)
    ite : ∀ {T} {Tᴰ : Ty T}{b : I.Tm I.Bool}{u v : I.Tm T} ->
          Tm Bool b -> Tm Tᴰ u -> Tm Tᴰ v -> Tm Tᴰ (I.ite b u v)

    ite-t : ∀ {T} {u v : I.Tm T}
            {Tᴰ : Ty T}{uᴰ : Tm Tᴰ u}{vᴰ : Tm Tᴰ v} ->
            ite true uᴰ vᴰ ≡ uᴰ
    ite-f : ∀ {T} {u v : I.Tm T}
            {Tᴰ : Ty T}{uᴰ : Tm Tᴰ u}{vᴰ : Tm Tᴰ v} ->
            ite false uᴰ vᴰ ≡ vᴰ
    isZero-z : isZero zero ≡ true
    isZero-s : ∀ {n} {nᴰ : Tm Nat n} -> isZero (suc nᴰ) ≡ false

module NatBoolInd {i j} (D : NatBoolDis {i}{j}) where
  private
    module D = NatBoolDis D

  postulate
    ⟦_⟧T : (T : I.Ty) -> D.Ty T
    ⟦_⟧t : ∀ {T} (t : I.Tm T) -> D.Tm ⟦ T ⟧T t

    ⟦Bool⟧ : ⟦ I.Bool ⟧T ≡ D.Bool
    ⟦Nat⟧ : ⟦ I.Nat ⟧T ≡ D.Nat
    {-# REWRITE ⟦Bool⟧ ⟦Nat⟧ #-}

    ⟦true⟧ : ⟦ I.true ⟧t ≡ D.true
    ⟦false⟧ : ⟦ I.false ⟧t ≡ D.false
    ⟦zero⟧ :  ⟦ I.zero ⟧t ≡ D.zero
    ⟦suc⟧ : ∀ {n} -> ⟦ I.suc n ⟧t ≡ D.suc ⟦ n ⟧t
    ⟦isZero⟧ : ∀ {n} -> ⟦ I.isZero n ⟧t ≡ D.isZero ⟦ n ⟧t
    ⟦ite⟧ : ∀ {T} {b : I.Tm I.Bool}{u v : I.Tm T} ->
              ⟦ I.ite b u v ⟧t ≡ D.ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦zero⟧ ⟦suc⟧ ⟦isZero⟧ ⟦ite⟧ #-}

record NatBoolDisP {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty : I.Ty -> Prop i
    Tm : ∀ {T} -> Ty T -> I.Tm T -> Prop j
    
    Bool : Ty I.Bool
    Nat : Ty I.Nat

    true : Tm Bool I.true
    false : Tm Bool I.false
    zero : Tm Nat I.zero
    suc : ∀ {n} -> Tm Nat n -> Tm Nat (I.suc n)
    isZero : ∀ {n} -> Tm Nat n -> Tm Bool (I.isZero n)
    ite : ∀ {T} {Tᴰ : Ty T}{b : I.Tm I.Bool}{u v : I.Tm T} ->
          Tm Bool b -> Tm Tᴰ u -> Tm Tᴰ v -> Tm Tᴰ (I.ite b u v)

module NatBoolIndP {i j} (D : NatBoolDisP {i}{j}) where
  private
    module D = NatBoolDisP D

  postulate
    ⟦_⟧T : (T : I.Ty) -> D.Ty T
    ⟦_⟧t : ∀ {T} (t : I.Tm T) -> D.Tm ⟦ T ⟧T t
    
--------------------------------------------
-- standard model and properties
--------------------------------------------

module Standard where
  private
    isZero : ℕ -> ⇅
    isZero = primrec ↑ λ _ _ -> ↓
  
  St : NatBoolAlg
  St = record
         { Ty = Set
         ; Tm = id
         ; Bool = ⇅
         ; Nat = ℕ
         ; true = ↑
         ; false = ↓
         ; zero = zeroℕ
         ; suc = sucℕ
         ; isZero = isZero
         ; ite = if_then_else_
         ; ite-t = refl
         ; ite-f = refl
         ; isZero-z = refl
         ; isZero-s = refl
         }

  open NatBoolRec St

  -- term embedding
  
  ⌜_⌝B : ⇅ -> I.Tm I.Bool
  ⌜ ↑ ⌝B = I.true
  ⌜ ↓ ⌝B = I.false

  ⌜_⌝N : ℕ -> I.Tm I.Nat
  ⌜ zeroℕ ⌝N = I.zero
  ⌜ sucℕ n ⌝N = I.suc ⌜ n ⌝N

  ⌜_⌝ : ∀ {A} -> ⟦ A ⟧T -> I.Tm A
  ⌜_⌝ {I.Bool} = ⌜_⌝B
  ⌜_⌝ {I.Nat} = ⌜_⌝N

  -- stability
  
  stabilityB : {b : ⇅} -> ⟦ ⌜ b ⌝B ⟧t ≡ b
  stabilityB {↑} = refl
  stabilityB {↓} = refl

  stabilityN : {n : ℕ} -> ⟦ ⌜ n ⌝N ⟧t ≡ n
  stabilityN {zeroℕ} = refl
  stabilityN {sucℕ n} = ap sucℕ stabilityN
  
  stability : ∀ {A} {t : ⟦ A ⟧T} -> ⟦ ⌜_⌝ {A} t ⟧t ≡ t
  stability {I.Bool} = stabilityB
  stability {I.Nat} = stabilityN

  -- completeness

  isZero-⌜⌝ : ∀ {n} -> ⌜ isZero n ⌝ ≡ I.isZero ⌜ n ⌝
  isZero-⌜⌝ {zeroℕ} = refl
  isZero-⌜⌝ {sucℕ n} = refl

  ite-⌜⌝ : ∀ {A} {b : ⇅}{u v : ⟦ A ⟧T} ->
           ⌜_⌝ {A} $ if b then u else v ≡ I.ite ⌜ b ⌝ ⌜ u ⌝ ⌜ v ⌝
  ite-⌜⌝ {b = ↑} = refl
  ite-⌜⌝ {b = ↓} = refl

  Comp : NatBoolDisP
  Comp = record
            { Ty = λ _ -> ⊤ₚ
            ; Tm = λ _ t -> ⌜ ⟦ t ⟧t ⌝ ≡ t
            ; Bool = _
            ; Nat = _
            ; true = refl
            ; false = refl
            ; zero = refl
            ; suc = ap I.suc
            ; isZero = λ {n} nᴰ -> isZero-⌜⌝ {⟦ n ⟧t}
                                 ◾ ap I.isZero nᴰ
            ; ite = λ {_ _ b u v} bᴰ uᴰ vᴰ ->
                                   ite-⌜⌝ {b = ⟦ b ⟧t}
                                 ◾ ap (I.ite ⌜ ⟦ b ⟧t ⌝ _) vᴰ
                                 ◾ ap (λ t -> I.ite ⌜ ⟦ b ⟧t ⌝ t v) uᴰ
                                 ◾ ap (λ t -> I.ite t u v) bᴰ
            }

  module Comp = NatBoolIndP Comp

  completeness : ∀ {A} {t : I.Tm A} -> ⌜ ⟦ t ⟧t ⌝ ≡ t
  completeness {t = t} = Comp.⟦ t ⟧t

  -- completeness using setty displayed algebra
  
  CompS : NatBoolDis
  CompS = record
            { Ty = const ⊤
            ; Tm = const $ λ t -> ↑p (⌜ ⟦ t ⟧t ⌝ ≡ t)
            ; Bool = _
            ; Nat = _
            ; true = refl↑
            ; false = refl↑
            ; zero = refl↑
            ; suc = λ h -> ↑[ ap I.suc ↓[ h ]↓ ]↑
            ; isZero = λ {n} nᴰ -> ↑[ isZero-⌜⌝ {⟦ n ⟧t}
                                   ◾ ap I.isZero ↓[ nᴰ ]↓ ]↑
            ; ite = λ {_ _ b u v} bᴰ uᴰ vᴰ ->
                          ↑[ ite-⌜⌝ {b = ⟦ b ⟧t}
                          ◾ ap (I.ite ⌜ ⟦ b ⟧t ⌝ _) ↓[ vᴰ ]↓
                          ◾ ap (λ t -> I.ite ⌜ ⟦ b ⟧t ⌝ t v) ↓[ uᴰ ]↓
                          ◾ ap (λ t -> I.ite t u v) ↓[ bᴰ ]↓ ]↑
            ; ite-t = refl
            ; ite-f = refl
            ; isZero-z = refl
            ; isZero-s = refl
            }
  
  module CompS = NatBoolInd CompS
  
  completeness' : ∀ {A} {t : I.Tm A} -> ⌜ ⟦ t ⟧t ⌝ ≡ t
  completeness' {t = t} = ↓[ CompS.⟦ t ⟧t ]↓
