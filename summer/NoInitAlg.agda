{-# OPTIONS --prop #-}

module NoInitAlg where

open import Lib

record Alg {i} : Set (lsuc i) where
  field
    Cₐ : Set i
    conₐ : (Cₐ -> ⊥) -> Cₐ
open Alg public

record Mor {i j} (A : Alg {i})(B : Alg {j}) : Set (i ⊔ j) where
  field
    Cₘ : Cₐ A -> Cₐ B
    conₘ : {f : Cₐ A -> ⊥} -> Cₘ (conₐ A f) ≡ conₐ B {!!}
open Mor public

record Dis {i j} (A : Alg {i}) : Set (lsuc (i ⊔ j)) where
  field
    Cₚ : Cₐ A -> Set j
    conₚ : {f : Cₐ A -> ⊥} -> {!!} -> Cₚ (conₐ A f)
open Dis public

record Sec {i j} {A : Alg {i}}(D : Dis {i} {j} A) : Set (i ⊔ j) where
  field
    Cₛ : (c : Cₐ A) -> Cₚ D c
    conₛ : {f : Cₐ A -> ⊥} -> Cₛ (conₐ A f) ≡ conₚ D {!!}
open Sec public

postulate
  I : ∀ {i} -> Alg {i}
  rec : ∀ {i j} (A : Alg {j}) -> Mor {i} I A
  ind : ∀ {i j} (D : Dis {i} {j} I) -> Sec D

elim : ∀ {i j} (D : Dis {i} {j} I) -> (c : Cₐ I) -> Cₚ D c
elim = Cₛ ∘ ind

D : Dis {lzero} I
D = record { Cₚ = λ _ -> Cₐ I -> ⊥ ; conₚ = λ {f} _ -> f }

y : Cₐ I
y = conₐ I (λ c -> elim D c c)

rip : ⊥
rip = elim D y y
