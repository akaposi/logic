{-# OPTIONS --prop #-}

module IndRecEquiv where

open import Lib
open import NatAlg

Ind : Setω
Ind = ∀ {i j} (D : NatDis {i} {j} I) -> NatSec D

Rec : Setω
Rec = ∀ {i j} -> ((A : NatAlg {j}) -> NatMor {i} I A) ×
                 ({A : NatAlg {j}}(M₁ M₂ : NatMor {i} I A) ->
                  (n : Nₐ I) -> ↑p (Nₘ M₁ n ≡ Nₘ M₂ n))

ind→rec : Ind -> Rec
ind→rec = λ ind ->
            ((λ A ->
               let D : NatDis I
                   D = record
                       { Nₚ = const $ Nₐ A
                       ; zₚ = zₐ A
                       ; sₚ = sₐ A
                       }
                   
                   sec : NatSec D
                   sec = ind D
               in record
                  { Nₘ = Nₛ sec
                  ; zₘ = zₛ sec
                  ; sₘ = sₛ sec
                  })
             ,
             λ {A} M₁ M₂ ->
               let D : NatDis I
                   D = record
                       { Nₚ = λ n -> ↑p (Nₘ M₁ n ≡ Nₘ M₂ n)
                       ; zₚ = ↑[ zₘ M₁ ◾ (zₘ M₂) ⁻¹ ]↑
                       ; sₚ = λ h -> ↑[ sₘ M₁
                                      ◾ ap (sₐ A) ↓[ h ]↓
                                      ◾ sₘ M₂ ⁻¹ ]↑
                       }
               in Nₛ (ind D))

rec→ind : Rec -> Ind
rec→ind = λ rec+u D ->
            let rec = λ {i j} -> proj₁ $ rec+u {i}{j}
                ∃! = λ {i j A} -> proj₂ (rec+u {i}{j}) {A}
                
                A : NatAlg
                A = record
                    { Nₐ = Σ (Nₐ I) (Nₚ D)
                    ; zₐ = zₐ I , zₚ D
                    ; sₐ = λ t -> sₐ I (proj₁ t) , sₚ D (proj₂ t)
                    }

                f : Nₐ I -> Nₐ A
                f = Nₘ $ rec A
                
                proj : NatMor A I
                proj = record
                       { Nₘ = proj₁
                       ; zₘ = refl
                       ; sₘ = refl
                       }
                
                proj∘recA=id : {n : Nₐ I} -> Nₘ (proj ∘ₘ rec A) n ≡ n
                proj∘recA=id {n} = ↓[ ∃! (proj ∘ₘ rec A) Id n ]↓
            in record
               { Nₛ = λ n → ? --coe (ap (Nₚ D) proj∘recA=id) (proj₂ $ f n)
               ; zₛ = ?
               ; sₛ = {!!}
               }
