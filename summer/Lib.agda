{-# OPTIONS --prop #-}

module Lib where

open import Agda.Primitive public

--------------------------------------------------
-- function combinators
--------------------------------------------------

id : ∀ {i} {A : Set i} -> A -> A
id x = x

const : ∀ {i j} {A : Set i}{B : Set j} -> A -> B -> A
const x y = x

infixr 1 _$_
_$_ : ∀ {i j} {A : Set i}{B : A -> Set j} ->
        ((x : A) -> B x) -> (x : A) -> B x
f $ x = f x

flip : ∀ {i j k} {A : Set i}{B : Set j}{C : A -> B -> Set k} ->
         ((x : A) -> (y : B) -> C x y) -> (x : B) -> (y : A) -> C y x
flip f x y = f y x

infixr 6 _∘_
_∘_ : ∀ {i j k} {A : Set i}{B : A -> Set j}{C : (x : A) -> B x -> Set k}
        (f : {x : A}(y : B x) -> C x y)(g : (x : A) -> B x) ->
        (x : A) -> C x (g x)
(f ∘ g) x = f (g x)

infixr 6 _∘'_
_∘'_ : ∀ {i j k} {A : Set i}{B : Set j}{C : Set k} ->
       (B -> C) -> (A -> B) -> A -> C
f ∘' g = f ∘ g

--------------------------------------------------
-- basic data types
--------------------------------------------------

data ⊥ : Set where

exfalso : ∀ {i} {A : Set i} -> ⊥ -> A
exfalso ()

exfalso-p : ∀ {i} {A : Prop i} -> ⊥ -> A
exfalso-p ()

data ⊥ₚ : Prop where

exfalsoₚ : ∀ {i} {A : Prop i} -> ⊥ₚ -> A
exfalsoₚ ()

record ⊤ : Set where
  constructor tt
open ⊤ public

record ⊤ₚ : Prop where
  constructor ttₚ
open ⊤ₚ public

data ⇅ : Set where
  ↑ : ⇅
  ↓ : ⇅

⇅-elim : ∀ {i} (P : ⇅ -> Set i) ->
            P ↑ -> P ↓ -> (b : ⇅) -> P b
⇅-elim P u v ↑ = u
⇅-elim P u v ↓ = v

if_then_else_ : ∀ {i} {A : Set i} ->
                ⇅ -> A -> A -> A
if_then_else_ {A = A} b u v = ⇅-elim (const A) u v b

data ℕ : Set where
  zeroℕ : ℕ
  sucℕ : ℕ -> ℕ
{-# BUILTIN NATURAL ℕ #-}

ℕ-elim : ∀ {i} (P : ℕ -> Set i) ->
               P zeroℕ -> ({n : ℕ} -> P n -> P (sucℕ n)) ->
               (n : ℕ) -> P n
ℕ-elim P u v zeroℕ = u
ℕ-elim P u v (sucℕ n) = v (ℕ-elim P u v n)

primrec : ∀ {i} {A : Set i} -> A -> (ℕ -> A -> A) -> ℕ -> A
primrec {A = A} u v = ℕ-elim (const A) u (λ {n} -> v n)

--------------------------------------------------
-- type constructors
--------------------------------------------------

record Σ {i j} (A : Set i)(B : A -> Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁
open Σ public

infixl 11 _×_
_×_ : ∀ {i j} (A : Set i)(B : Set j) -> Set (i ⊔ j)
A × B = Σ A (const B)

infixl 10 _⊎_
data _⊎_ {i j} (A : Set i)(B : Set j) : Set (i ⊔ j) where
  inj₁ : A -> A ⊎ B
  inj₂ : B -> A ⊎ B

⊎-elim : ∀ {i j k} {A : Set i}{B : Set j}(P : A ⊎ B -> Set k) ->
         ((a : A) -> P (inj₁ a)) -> ((b : B) -> P (inj₂ b)) ->
         (t : A ⊎ B) -> P t
⊎-elim P u v (inj₁ t) = u t
⊎-elim P u v (inj₂ t) = v t

record ↑p {i} (A : Prop i) : Set i where
  constructor ↑[_]↑
  field
    ↓[_]↓ : A
open ↑p public

--------------------------------------------------
-- equality
--------------------------------------------------

infix 0 _≡_
data _≡_ {i} {A : Set i}(x : A) : A -> Prop i where
  refl : x ≡ x

infix 3 _■
_■ : ∀ {i} {A : Set i}(x : A) -> x ≡ x
x ■ = refl

infix 5 _⁻¹
_⁻¹ : ∀ {i} {A : Set i}{x y : A} -> x ≡ y -> y ≡ x
refl ⁻¹ = refl

infixr 4 _◾_
_◾_ : ∀ {i} {A : Set i}{x y z : A} -> x ≡ y -> y ≡ z -> x ≡ z
refl ◾ p = p

ap : ∀ {i j} {A : Set i}{B : Set j}(f : A -> B){x y : A} ->
         x ≡ y -> f x ≡ f y
ap f refl = refl

eta : ∀ {i j} {A : Set i}{B : A -> Set j}{f g : (x : A) -> B x} ->
        f ≡ g -> {x : A} -> f x ≡ g x
eta refl = refl

--------------------------------------------------
-- prop-set interoperation for equality
--------------------------------------------------

refl↑ : ∀ {i} {A : Set i}{x : A} -> ↑p (x ≡ x)
refl↑ = ↑[ refl ]↑

--------------------------------------------------
-- equality in set
--------------------------------------------------

infix 0 _≡ₛ_
data _≡ₛ_ {i} {A : Set i}(x : A) : A -> Set i where
  refl : x ≡ₛ x

J : ∀ {i j} {A : Set i}{x : A}(P : {y : A} -> x ≡ₛ y -> Set j) -> P refl ->
    {y : A}(p : x ≡ₛ y) -> P p
J P pr refl = pr

transport : ∀ {i j} {A : Set i}(P : A -> Set j){x y : A} ->
              x ≡ₛ y -> P x -> P y
transport P refl p = p

infix 3 _■ₛ
_■ₛ : ∀ {i} {A : Set i}(x : A) -> x ≡ₛ x
x ■ₛ = refl

infix 5 _⁻¹ₛ
_⁻¹ₛ : ∀ {i} {A : Set i}{x y : A} -> x ≡ₛ y -> y ≡ₛ x
refl ⁻¹ₛ = refl

infixr 4 _◾ₛ_
_◾ₛ_ : ∀ {i} {A : Set i}{x y z : A} -> x ≡ₛ y -> y ≡ₛ z -> x ≡ₛ z
refl ◾ₛ p = p

apₛ : ∀ {i j} {A : Set i}{B : Set j}(f : A -> B){x y : A} ->
         x ≡ₛ y -> f x ≡ₛ f y
apₛ f refl = refl

etaₛ : ∀ {i j} {A : Set i}{B : A -> Set j}{f g : (x : A) -> B x} ->
        f ≡ₛ g -> {x : A} -> f x ≡ₛ g x
etaₛ refl = refl

coe : ∀ {i} {A B : Set i} -> A ≡ₛ B -> A -> B
coe refl = id

_=[_]=_ : ∀ {i} {A B : Set i} -> A -> A ≡ₛ B -> B -> Set i
x =[ p ]= y = coe p x ≡ₛ y

apd : ∀ {i j} {A : Set i}{B : A -> Set j}(f : (x : A) -> B x)
      {x y : A}(p : x ≡ₛ y) -> f x =[ apₛ B p  ]= f y
apd f refl = refl
