{-# OPTIONS --prop #-}

module NBF0 where

open import Lib

record NBFAlg {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 1 _▹_
  infixr 2 _⇒_
  infixr 4 _∘ₛ_
  infixl 3 _,ₛ_
  infixl 6 _[_]
  infixl 5 _$'_
  
  field
    Con : Set i
    STy : Set j
    Ty : Set j
    Sub : Con -> Con -> Set k
    Tm : Con -> Ty -> Set l

    ∙ : Con
    _▹_ : Con -> Ty -> Con

    Bool : STy
    Nat : STy
    sty : STy -> Ty
    _⇒_ : STy -> Ty -> Ty

    _∘ₛ_ : ∀ {Γ Δ Θ} -> Sub Δ Θ -> Sub Γ Δ -> Sub Γ Θ
    idₛ : ∀ {Γ} -> Sub Γ Γ
    ε : ∀ {Γ} -> Sub Γ ∙
    p : ∀ {Γ A} -> Sub (Γ ▹ A) Γ
    _,ₛ_ : ∀ {Γ Δ A} -> Sub Γ Δ -> Tm Γ A -> Sub Γ (Δ ▹ A)

    q : ∀ {Γ A} -> Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} -> Tm Δ A -> Sub Γ Δ -> Tm Γ A
    lam : ∀ {Γ A B} -> Tm (Γ ▹ sty A) B -> Tm Γ (A ⇒ B)
    _$'_ : ∀ {Γ A B} -> Tm Γ (A ⇒ B) -> Tm Γ (sty A) -> Tm Γ B
    
    true : ∀ {Γ} -> Tm Γ (sty Bool)
    false : ∀ {Γ} -> Tm Γ (sty Bool)
    zero : ∀ {Γ} -> Tm Γ (sty Nat)
    suc : ∀ {Γ} -> Tm Γ (sty Nat) -> Tm Γ (sty Nat)
    isZero : ∀ {Γ} -> Tm Γ (sty Nat) -> Tm Γ (sty Bool)
    ite : ∀ {Γ A} -> Tm Γ (sty Bool) -> Tm Γ A -> Tm Γ A -> Tm Γ A

    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} ->
          (σ ∘ₛ δ) ∘ₛ ν ≡ σ ∘ₛ (δ ∘ₛ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} -> idₛ ∘ₛ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} -> σ ∘ₛ idₛ ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} -> σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} ->
          p ∘ₛ (σ ,ₛ t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} ->
          q [ σ ,ₛ t ] ≡ t
    ▹η : ∀ {Γ A} -> (p {Γ}{A} ,ₛ q) ≡ idₛ
    ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{δ : Sub Γ Δ}{t : Tm Δ A} ->
         (σ ,ₛ t) ∘ₛ δ ≡ σ ∘ₛ δ ,ₛ t [ δ ]
    [id] : ∀ {Γ A} {t : Tm Γ A} -> t [ idₛ ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{δ : Sub Γ Δ}{t : Tm Θ A} ->
          t [ σ ∘ₛ δ ]  ≡ t [ σ ] [ δ ]

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ sty A) B}{u : Tm Γ (sty A)} ->
         lam t $' u ≡ t [ idₛ ,ₛ u ]
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)}{u : Tm Γ (sty A)} ->
         lam $ (t $' u) [ p ] ≡ t
    lam[] : ∀ {Γ Δ A B} {σ : Sub Γ Δ}{t : Tm (Δ ▹ sty A) B} ->
            (lam t) [ σ ] ≡ lam (t [ σ ∘ₛ p ,ₛ q ])

postulate
  I : ∀ {i j k l} -> NBFAlg {i}{j}{k}{l}

St : NBFAlg
St = record
       { Con = Set
       ; STy = Set
       ; Ty = Set
       ; Sub = λ Γ Δ -> Γ -> Δ
       ; Tm = λ Γ A -> Γ -> A
       ; ∙ = ⊤
       ; _▹_ = _×_
       ; Bool = ⇅
       ; Nat = ℕ
       ; sty = id
       ; _⇒_ = λ A B -> A -> B
       ; _∘ₛ_ = _∘'_
       ; idₛ = id
       ; ε = const tt
       ; p = proj₁
       ; _,ₛ_ = λ σ t γ -> σ γ , t γ
       ; q = proj₂
       ; _[_] = _∘'_
       ; lam = {!!}
       ; _$'_ = {!!}
       ; true = {!!}
       ; false = {!!}
       ; zero = {!!}
       ; suc = {!!}
       ; isZero = {!!}
       ; ite = {!!}
       ; ass = {!!}
       ; idl = {!!}
       ; idr = {!!}
       ; ∙η = {!!}
       ; ▹β₁ = {!!}
       ; ▹β₂ = {!!}
       ; ▹η = {!!}
       ; ,∘ = {!!}
       ; [id] = {!!}
       ; [∘] = {!!}
       ; ⇒β = {!!}
       ; ⇒η = {!!}
       ; lam[] = {!!}
       }

open NBFAlg (I {lzero}{lzero}{lzero}{lzero})

id' : ∀ {Γ A} -> Tm Γ (A ⇒ sty A)
id' = lam q

ff : ∀ {Γ A} -> Tm Γ (A ⇒ sty Bool)
ff = lam false

const' : ∀ {Γ A B} -> Tm Γ (A ⇒ B ⇒ sty A)
const' = lam $ lam $ q [ p ]

not : ∀ {Γ} -> Tm Γ (Bool ⇒ sty Bool)
not = lam (ite q false true)

and : ∀ {Γ} -> Tm Γ (Bool ⇒ Bool ⇒ sty Bool)
and = lam $ lam $ ite (q [ p ]) q false

xor : ∀ {Γ} -> Tm Γ (Bool ⇒ Bool ⇒ sty Bool)
xor = lam $ lam $ ite (q [ p ]) (not $' q) q

flip' : ∀ {Γ A B C} -> Tm Γ (A ⇒ B ⇒ C) -> Tm Γ (B ⇒ A ⇒ C)
flip' f = lam $ lam $ f [ p ] [ p ] $' q $' q [ p ]
