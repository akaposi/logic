{-# OPTIONS --prop #-}

module NatBool0 where

open import Lib

record NatBoolAlg {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty : Set i
    Tm : Ty -> Set j

    Bool : Ty
    Nat : Ty

    true : Tm Bool
    false : Tm Bool
    zero : Tm Nat
    suc : Tm Nat -> Tm Nat
    isZero : Tm Nat -> Tm Bool
    ite : {A : Ty} -> Tm Bool -> Tm A -> Tm A -> Tm A
    
    ite-t : {A : Ty}{u v : Tm A} -> ite true u v ≡ u
    ite-f : {A : Ty}{u v : Tm A} -> ite false u v ≡ v
    isZero-z : isZero zero ≡ true
    isZero-s : {n : Tm Nat} -> isZero (suc n) ≡ false
open NatBoolAlg public

record NatBoolMor {i₁ j₁ i₂ j₂}
       (A : NatBoolAlg {i₁}{j₁})(B : NatBoolAlg {i₂}{j₂}) : Set (i₁ ⊔ j₁ ⊔ i₂ ⊔ j₂) where
  field
    Tyₘ : (Ty A) -> (Ty B)
    Tmₘ : {T : (Ty A)} -> (Tm A) T -> (Tm B) (Tyₘ T)

    Boolₘ : Tyₘ (Bool A) ≡ (Bool B)
    Natₘ : Tyₘ (Nat A) ≡ (Nat B)
    
    trueₘ : Tmₘ (true A) =[ ap (Tm B) Boolₘ ]= (true B)
    falseₘ : Tmₘ (false A) =[ ap (Tm B) Boolₘ ]= (false B)
    zeroₘ : Tmₘ (zero A) =[ ap (Tm B) Natₘ ]= (zero B)
    sucₘ : {n : (Tm A) (Nat A)} ->
           Tmₘ ((suc A) n) =[ ap (Tm B) Natₘ ]= ((suc B) $ coe (ap (Tm B) Natₘ) (Tmₘ n))
    isZeroₘ : {n : (Tm A) (Nat A)} ->
              Tmₘ ((isZero A) n) =[ ap (Tm B) Boolₘ ]= ((isZero B) $ coe (ap (Tm B) Natₘ) (Tmₘ n))
    iteₘ : {T : (Ty A)}{b : (Tm A) (Bool A)}{u v : (Tm A) T} ->
           (Tmₘ $ (ite A) b u v) ≡ (ite B) (coe (ap (Tm B) Boolₘ) $ Tmₘ b) (Tmₘ u) (Tmₘ v)
open NatBoolMor public

{-
module I where
  data Ty {i} : Set i where
    Bool : Ty
    Nat : Ty
  
  data Tm {i j} : Ty {i} -> Set j where
    true : Tm Bool
    false : Tm Bool
    zero : Tm Nat
    suc : Tm {i}{j} Nat -> Tm {i}{j} Nat

  ite : ∀ {i j} {A : Ty {i}} ->
          Tm {i}{j} Bool -> Tm {i}{j} A -> Tm {i}{j} A -> Tm {i}{j} A
  ite = {!!}
  
  I : ∀ {i j} -> NatBoolAlg {i}{j}
  I = record
        { Ty = Ty
        ; Tm = Tm
        ; Bool = Bool
        ; Nat = Nat
        ; true = true
        ; false = false
        ; zero = zero
        ; suc = suc
        ; isZero = {!!}
        ; ite = {!!}
        ; ite-t = {!!}
        ; ite-f = {!!}
        ; isZero-z = {!!}
        ; isZero-s = {!!}
        }

module Mor {i₁ j₁ i₂ j₂}
       (A : NatBoolAlg {i₁}{j₁})(B : NatBoolAlg {i₂}{j₂}) where
  module A = NatBoolAlg A
  module B = NatBoolAlg B
-}


