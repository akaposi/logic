{-# OPTIONS --prop #-}

module NatAlg where

open import Lib

record NatAlg {i} : Set (lsuc i)  where
  field
    Nₐ : Set i
    zₐ : Nₐ
    sₐ : Nₐ -> Nₐ
open NatAlg public

record NatMor {i j} (A : NatAlg {i})(B : NatAlg {j}) : Set (i ⊔ j) where
  field
    Nₘ : Nₐ A -> Nₐ B
    zₘ : Nₘ (zₐ A) ≡ zₐ B
    sₘ : {n : Nₐ A} -> Nₘ (sₐ A n) ≡ sₐ B (Nₘ n)
open NatMor public

record NatDis {i j} (A : NatAlg {i}) : Set (lsuc (i ⊔ j)) where
   field
     Nₚ : Nₐ A -> Set j
     zₚ : Nₚ (zₐ A)
     sₚ : {n : Nₐ A} -> Nₚ n -> Nₚ (sₐ A n)
open NatDis public

record NatSec {i j} {A : NatAlg {i}}(D : NatDis {i}{j} A) : Set (i ⊔ j) where
  field
    Nₛ : (n : Nₐ A) -> Nₚ D n
    zₛ : Nₛ (zₐ A) ≡ zₚ D
    sₛ : {n : Nₐ A} -> Nₛ (sₐ A n) ≡ sₚ D (Nₛ n)
open NatSec public

postulate
  I : ∀ {i} -> NatAlg {i}
  rec : ∀ {i j} (A : NatAlg {j}) -> NatMor {i} I A
  ind : ∀ {i j} (D : NatDis {i}{j} I) -> NatSec D

------------------------------------------------------
-- category of N-algebras
------------------------------------------------------

Id : ∀ {i} {A : NatAlg {i}} -> NatMor A A
Id = record
     { Nₘ = id
     ; zₘ = refl
     ; sₘ = refl
     }

_∘ₘ_ : ∀ {i j k} {A : NatAlg {i}}{B : NatAlg {j}}{C : NatAlg {k}} ->
       NatMor B C -> NatMor A B -> NatMor A C
M₂ ∘ₘ M₁ = record
           { Nₘ = Nₘ M₂ ∘ Nₘ M₁
           ; zₘ = ap (Nₘ M₂) (zₘ M₁) ◾ zₘ M₂
           ; sₘ = ap (Nₘ M₂) (sₘ M₁) ◾ sₘ M₂
           }

------------------------------------------------------
--
------------------------------------------------------

Nat : ∀ {i} -> Set i
Nat = Nₐ I

zero : ∀ {i} -> Nat {i}
zero = zₐ I

suc : ∀ {i} -> Nat {i} -> Nat {i}
suc = sₐ I

-- sum of two natural numbers

Sum : NatAlg {lzero}
Sum = record
  { Nₐ = Nat -> Nat
  ; zₐ = id
  ; sₐ = _∘_ suc
  }

plus : NatMor {lzero} I Sum
plus = rec Sum

infixl 10 _+_
_+_ : Nat -> Nat -> Nat
_+_ = Nₘ plus

-- properties of this sum

-- zero is left identity

IdlPlus : NatDis I
IdlPlus = record
  { Nₚ = λ n -> ↑p (zero + n ≡ n)
  ; zₚ = ↑[ eta (zₘ plus) ]↑
  ; sₚ = const $ ↑[ eta (zₘ plus) ]↑
  }

idl-plus : {n : Nat} -> zero + n ≡ n
idl-plus {n} = ↓[ Nₛ (ind IdlPlus) n ]↓

-- zero is right identity

IdrPlus : NatDis I
IdrPlus = record
  { Nₚ = λ n -> ↑p (n + zero ≡ n)
  ; zₚ = ↑[ eta (zₘ plus) ]↑
  ; sₚ = λ h → ↑[ eta (sₘ plus)
                ◾ ap suc ↓[ h ]↓ ]↑
  }

idr-plus : {n : Nat} -> n + zero ≡ n
idr-plus {n} = ↓[ Nₛ (ind IdrPlus) n ]↓

-- addition is associative

AssPlus : NatDis I
AssPlus = record
  { Nₚ = λ x → (y z : Nat) -> ↑p ((x + y) + z ≡ x + (y + z))
  ; zₚ = λ y z → ↑[ ap (flip _+_ z) idl-plus ◾ idl-plus ⁻¹ ]↑
  ; sₚ = λ h y z → ↑[ ap (flip _+_ z) (eta (sₘ plus))
                   ◾ eta (sₘ plus)
                   ◾ ap suc ↓[ h  _ _ ]↓
                   ◾ eta (sₘ plus) ⁻¹ ]↑
  }

ass-plus : (x y z : Nat) -> (x + y) + z ≡ x + (y + z)
ass-plus x y z = ↓[ Nₛ (ind AssPlus) x y z ]↓

-- addition is commutative

LemCommPlus : NatDis I
LemCommPlus = record
  { Nₚ = λ x → (y : Nat) -> ↑p (suc x + y ≡ x + suc y)
  ; zₚ = λ y → ↑[ eta (sₘ plus)
                ◾ ap suc idl-plus
                ◾ idl-plus ⁻¹ ]↑
  ; sₚ = λ h y -> ↑[ eta (sₘ plus)
                   ◾ ap suc ↓[ h _ ]↓
                   ◾ eta (sₘ plus) ⁻¹ ]↑
  }

lem-comm-plus : (x y : Nat) -> suc x + y ≡ x + suc y
lem-comm-plus x y = ↓[ Nₛ (ind LemCommPlus) x y ]↓

CommPlus : NatDis I
CommPlus = record
  { Nₚ = λ x -> (y : Nat) -> ↑p (x + y ≡ y + x)
  ; zₚ = λ y → ↑[ idl-plus ◾ idr-plus ⁻¹ ]↑
  ; sₚ = λ h y → ↑[ eta (sₘ plus)
                  ◾ ap suc ↓[ h _ ]↓
                  ◾ eta (sₘ plus) ⁻¹
                  ◾ lem-comm-plus _ _ ]↑
                  
  }

comm-plus : (x y : Nat) -> x + y ≡ y + x
comm-plus x y = ↓[ Nₛ (ind CommPlus) x y ]↓
